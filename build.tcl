# project creation
set proj_name itks_hccstar_hwemu
set proj_dir project/
set part xc7vx690tffg1761-2
create_project -force -dir $proj_dir -name $proj_name -part $part
set_property target_language VHDL [current_project]


# VHDL sources
set hdl_dir hdl
set pktgen_dir $hdl_dir/packetgen
set lcbdec_dir $hdl_dir/lcbdecoder
set tb_dir tb
set synth_top hccstaremu
set sim_top tb_top

add_files -fileset sources_1 [glob -directory $hdl_dir {*.vhd}]
set_property library hccemu [get_files [glob -directory $hdl_dir {*.vhd}]]

add_files -fileset sources_1 [glob -directory $lcbdec_dir {*.vhd}]
set_property library hccemu_lcbdec [get_files [glob -directory $lcbdec_dir {*.vhd}]]

add_files -fileset sources_1 [glob -directory $pktgen_dir {*.vhd}]
set_property library hccemu_pktgen [get_files [glob -directory $pktgen_dir {*.vhd}]]

add_files -fileset sim_1 [glob -directory $tb_dir {*.vhd}]


# IP sources
set ip_dir ip
foreach dir [glob -directory $ip_dir {*}] {add_files [glob -directory $dir {*.xci}]}
update_ip_catalog
foreach ip [get_ips *] {
    generate_target all [get_files $ip.xci]
}

# finishing touches
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

set_property top $synth_top [get_filesets sources_1]
set_property top $sim_top [get_filesets sim_1]
