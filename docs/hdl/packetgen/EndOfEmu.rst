End of Emulator
==============================================

When no data is coming from the packer, the end-of-emulator module assembles an
IDLE frame and sends it to the 8b10b encoder. Additionally, the end-of-emulator
module packs on the 2-bit code indicating whether the current word is a Start
of Frame (SoF) word, data word, End of Frame (EoF) word, or idle word. This information is
added to every word coming through (IDLE or actual packer output) and is used by
the 8b10b encoder.

Signals
-------

Inputs
  ``clk``
    240 MHz clock.
  ``rst``
    Reset.
  ``din[16:0]``
    16-bit word plus 1 bit "last flag" from the packer. Of type
    ``packed_plus_last``.
  ``empty_packer``
    Signals when the packer is empty.
  ``valid_packer``
    Signals when ``din`` is valid.
  ``full_packer``
    *Unused*. Signals when the packer is full.
  ``almost_full_packer``
    *Unused*. Signals when the packer is almost full.

Outputs
  ``rd_en_packer``
    Read enable for the packer, we want data!
  ``k_in``
    *Unused*. Signals when the output word is special -- i.e. a start or end of
    frame.
  ``elinkdata_o[17:0]``
    16-bit output word plus 2 bit code for the 8b10b encoder. Of type
    ``to_elink``.
  ``last_word``
    Signals when the current word is the last word in a packet. Also goes out
    of the emulator into FELIG, but not sure where or why...
  ``data_rdy``
    Not entirely sure what the purpose of this is. It seems like it may end up
    being connected to the 8b10b encoder?

Internal
  ``rd_en_packer_sig``
    Internal signal for ``rd_en_packer`` output.
  ``k_in_sig``
    Internal signal for ``k_in`` output.
  ``data_last``
    ``din(16)`` -- signals when the current input is the last word in a packet.
  ``k_start``
    Kind of a default value for ``k_in``.
  ``datacode``
    *Unused*. Constants ``sof_code``, ``eof_code``, etc... are used instead.
  ``last_word_s``
    Internal signal for ``last_word`` outut.


Description
-----------

The end-of-emulator module serves two purposes:
 - Emit two 8-bit IDLE words when the packer has nothing to give us
 - Append the 2-bit code expected by the 8b10b encoder to every 16-bit word that
   passes through

The four 2-bit codes for the 8b10b encoder are:

+------+-----+-----+-------+
| Data | EoF | SoF | Comma |
+------+-----+-----+-------+
| 00   | 01  | 10  | 11    | 
+------+-----+-----+-------+

IDLE words (``0xbc``) are appended with the Comma code, creating a final output
of ``0x3bcbc``. When the packer first starts outputting data, the SoF code is
added to the 16-bit word. All subsequent words are appended with the Data code,
until the last word which gets the EoF code.
