Event Counter
=============


The event counter module generates the 11-bit trigger information used in the
header of a physics packet. Internal or external L0a tags can be used.

Signals
-------

Inputs
  ``clk_40_i``
    40 MHz clock.
  ``trig_sel_i``
    Internal/external L0a Tag select.
  ``l0a_i``
    L0a input.
  ``l0a_tag_i[6:0]``
    L0a tag input.
  ``bcr_i``
    Bunch crossing count reset.
  ``full``
    Signals when the event builder FIFO is full.
  ``almost_full``
    Signals when the event builder FIFO is almost full.
  ``start_event``
    Input signaling when to start an event.

Outputs
  ``event_info_o[10:0]``
    Output 11-bit event info used in physics packet header. Connected to the
    event builder FIFO. Of type ``event_info``.
  ``wr_en``
    Write enable for the event builder FIFO.

Internal
  ``bcid_count[7:0]``
    An ``unsigned`` counting the number of bunch crossings since the last BCR.
  ``tag_count[6:0]``
    An ``unsigned`` counting the number of L0a.
  ``bcid[7:0]``
    A ``std_logic_vector`` representation of ``bcid_count``.
  ``bcid_less1[7:0]``
    A ``std_logic_vector`` representation of ``bcid_count`` minus 1.
  ``bcid_part[3:0]``
    The three LSBs of ``bcid``.
  ``wr_en_s``
    Internal signal for ``wr_en``.
  ``tag_int[6:0]``
    A ``std_logic_vector`` representation of ``tag_count``.
  ``tag_ext[6:0]``
    Just ``l0a_tag_i``.
  ``event_info_s[10:0]``
    Internal signal for ``event_info_o``. Of type ``event_info``. Of type
    ``event_info``.

Description
-----------

The 11-bit trigger information word is arranged as follows:

+--------+--------------+
| L0a    | Partial BCID |
+--------+--------------+
| 7 bits | 4 bits       |
+--------+--------------+

The partial BCID is always calculated internally. The full 8-bit BCID is
incremented at each 40 MHz clock cycle. The partial BCID is calculated as the
four least significant bits of one less than the BCID.

The L0a tag can be computed internally or supplied externally -- which is used
is determined by ``trig_sel_i``. When low, the internal tag is used, and when
high the value on ``L0a_tag_i`` is used. The internal tag is incremented every
time there is an L0a and ``start_event`` is activated -- regardless of the
value of ``trig_sel_i``.
