Hit Generator Control Module
==============================================

The hit generator control module generates the physics packet when a trigger
command has been received. The generated packet, containing both trigger and
hit information, is loaded into a FIFO and eventually read by the data
selector and sent through the packer and out along the uplink.

**TODO:** Implement handling of triggers when the BRAM is empty.

Signals
-------

Inputs
  ``clk``
    240 MHz clock input.
  ``rst``
    Synchronous reset.
  ``trig_preamble[10:0]``
    11-bit trigger information.
  ``empty_trig``
    Signals when the trigger FIFO is empty.
  ``valid_trig``
    Signals when the data on ``trigger_preamble`` is valid.
  ``itks_emu_hits[16:0]``
    17-bit bus bringing in hit data from the BRAM.
  ``itks_emu_hits_valid``
    Signals when the data on ``itks_emu_hits`` is valid.
  ``itks_emu_hits_empty``
    Signals when the BRAM is empty.
    
Outputs
  ``itks_emu_rd_en``
    Read enable for the BRAM -- signals we want hit data.
  ``rd_en_trig``
    Read enable for the trigger FIFO.
  ``data[21:0]``
    22-bit bus to be written to the HitGen FIFO.
  ``wr_en_hitgen``
    Indicates ``data`` is a valid word and should be written to the HitGen FIFO
    as part of a packet.

Internal
  ``state_reg``
    Stores the current state of the state machine. Of type
    ``hccstar_hitgen_ctl_type``.
  ``state_next``
    Stores the next state for the state machine, which gets updated at every
    clock cycle. Of type ``hccstar_hitgen_ctl_type``.
  ``packet_type[3:0]``
    Stores the TYP code for a physics packet (``0b0010``).
  ``error_flag``
    Signals an error in the physics packet generation
    process. Currently unimplemented, and always set to zero.
  ``last_hit``
    Single bit signaling the last hit within the current trigger.
  ``hits_to_write[16:0]``
    17-bit word used to store the hit data. Of type ``st_hits``.

Description
-----------

Physics Packet Format
^^^^^^^^^^^^^^^^^^^^^
A single physics packet contains a number of 16-bit words, arranged as follows:

+-----------------+--------+------+-------+----------------+---------------+---------+---------------+
| Start-of-Packet | Header | Hits | [...] | [Error Header] | [Error Block] | Trailer | End-of-Packet |
+-----------------+--------+------+-------+----------------+---------------+---------+---------------+

where quantities in square brackets may not always be present.

The Start-of-Packet word consists of the K28.5 idle symbol (``0xbc``), followed
by the K28.1 start-of-packet symbol (``0x3c``):

+-----------------+
| Start-of-Packet |
+========+========+
| K28.5  | K28.1  |
+--------+--------+
| 8 Bits | 8 Bits |
+--------+--------+

The Header word contains the TYP code, an error flag, the L0tag and the BCID:

+---------------------------------------+
| Header                                |
+========+============+========+========+
| TYP    | Error Flag | L0ID   | BCID   |
+--------+------------+--------+--------+
| 4 Bits | 1 Bit      | 7 Bits | 4 Bits |
+--------+------------+--------+--------+

where the BCID is calculated as the 3 LSBs of the bunch crossing counter,
followed by a single parity bit which is calculated over the entire 8-bit
counter.

The hit words contain the ABCStar clusters, and the number of these words can
vary between events.

Currently, the emulator will not produce any error blocks, so the Error Header
and Error Block words will never be inserted into the packet.

After the ABCStar clusters (and the error block, if there is one) comes a
trailer word, consisting of the invalid cluster ``0x6fed``.

The final word in the packet consists of the K28.6 end-of-packet symbol
(``0xdc``) and the K28.5 idle symbol:

+-----------------+
| End-of-Packet   |
+========+========+
| K28.6  | K28.5  |
+--------+--------+
| 8 Bits | 8 Bits |
+--------+--------+

State Machine
^^^^^^^^^^^^^

The physics packets are generated with a state machine, which controls the
``itks_emu_rd_en``, ``data``, ``rd_en_trig``, and ``wr_en_hitgen`` signals to
interact with the BRAM, the trigger FIFO, and the FIFO immediately following the
hit generator.
All words written to the HitGen FIFO are in the format expected by the packer,
consisting of a single bit "last word" flag, the 16-bit word, and the 5-bit word
length. It circles through the states ``IDLE``, ``START``, ``START_CTL_K28_1``,
``HEADER``, ``READ_HITS``, ``TRAILER``, and ``END_CTL_K28_6``, as seen in the
diagram below.

.. tikz:: State machine diagram for the hit generator control module.
   :libs: automata, positioning, arrows

   [->,
     >=stealth, node distance=3cm,
     every state/.style={
         rectangle,
         rounded corners,
         draw=black,
         very thick,
         minimum height=2em,
         inner sep=2pt,
         text centered,
         fill=gray!10
     },
     initial text=$ $
    ]
    \node[state, initial] (s0) {IDLE};
    \node[state, above right of=s0] (s1) {START};
    \node[state, below right of=s0] (s6) {END\_CTL\_K28\_6};
    \node[state, right of=s1] (s2) {START\_CTL\_K28\_1};
    \node[state, right of=s2, xshift=3cm] (s3) {HEADER};
    \node[state, right of=s6] (s5) {TRAILER};
    \node[state, right of=s5, xshift=3cm] (s4) {READ\_HITS};
    \draw   (s0) edge[loop above] (s0)
            (s0) edge[below right] node{Trig FIFO} (s1)
            (s1) edge (s2)
            (s2) edge[loop above] (s2)
            (s2) edge[above] node{Valid Trigger} (s3)
            (s3) edge (s4)
            (s4) edge[loop below] (s4)
            (s4) edge[above] node{Last Hit} (s5)
            (s5) edge (s6)
            (s6) edge (s0);

In the ``IDLE`` state, all outputs are set to zero. If the trigger FIFO is not
empty, then at the next clock cycle the state machine will move into the
``START`` state.

In the ``START`` state, ``rd_en_trig`` is activated so that at the next clock
cycle there will be data ready from the trigger FIFO. All outputs are still
held low. The state machine will automatically move into the ``START_CTL_K28_1``
state at the next clock cycle.

In the ``START_CTL_K28_1`` state, ``data`` contains the 16-bit "Start-of-Packet"
word. If the information coming from the trigger FIFO is valid, then the write
enable signal for the HitGen FIFO is activated to start writing the packet. If
``valid_trig`` is not activated, the state machine will remain in the same
state. Otherwise, after writing the first word into the FIFO, the state machine
will transition to the ``HEADER`` state.

In the ``HEADER`` state, the "Header" word is assembled and sent out along
``data`` and written into the HitGen FIFO. The state machine will automatically
transition into the ``READ_HITS`` state at the next clock cycle.

In the ``READ_HITS`` state, ``itks_emu_rd_en`` is used to read from the BRAM,
which outputs its data into ``itks_emu_hits`` and then into ``hits_to_write``.
Note that the BRAM outputs its data one clock cycle after ``itks_emu_rd_en`` is
activated, so when the state machine first enters the ``READ_HITS`` state,
``itks_emu_rd_en`` will be activated and then at the next clock cycle the data
will be read out. The state machine will stay in this state until it sees a
cluster with the "last hit" bit set, at which point it will transition to the
``TRAILER`` state at the next clock cycle (along with deactivating
``itks_emu_rd_en`` so as not to keep reading the BRAM!).

In the ``TRAILER`` state, the invalid "Trailer" pattern (``0x6fed``) is written
into the HitGen FIFO. The state machine will automatically transition to the
``END_CTL_K28_6`` state at the next clock cycle.

In the ``END_CTL_K28_6`` state, ``last_hit`` is deactivated, since it gets
latched only when the BRAM data is valid and thus would otherwise end up staying
activated. The "End-of-Packet" word is written into the HitGen FIFO, with the
"last word" bit (the MSB of ``data``) set so the packer knows the current packet
has ended.

**NOTE:** The handling of ``last_hit`` could be changed here. There's not
really a reason to have to reset ``last_hit`` when entering this state --
instead, you could just check for
``(last_hit = '1') and (itks_emu_hits_valid = '1')`` in the ``READ_HITS`` state
transition logic.
