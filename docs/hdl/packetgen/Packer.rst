Packer
==============================================

The packer takes in variable length data words and densely packs them into
16-bit words.

Signals
-------

Inputs
  ``clk``
    240 MHz clock.
  ``reset``
    Synchronous reset.
  ``packerin[15:0]``
    Input word.
  ``packerinlength[4:0]``
    Length of the input word.
  ``packerinvalid``
    Signals when ``packerin`` is valid.
  ``packerinlast``
    Signals when ``packerin`` is the last word in the packet.
  ``evbready``
    Unknown.

Outputs
  ``packerready``
    Signals when the packer is ready for more data.
  ``packerout[15:0]``
    Output 16-bit word.
  ``packeroutvalid``
    Signals when ``packerout`` is valid.
  ``packeroutlast``
    Signals when ``packerout`` is the last word in the packet.
  ``packercnt[31:0]``
    Unknown.
