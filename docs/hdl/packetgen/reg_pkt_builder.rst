Register Read Packet Builder
============================

The register read packet builder generates the 48-bit output packet when a
a register read command has been received. This generated packet, containing
the register address, value, and TYP code, is loaded into a FIFO and eventually
read by the data selector and sent through the packer and out along the uplink.

**TODO:** Currently, the FIFO is inside the ``reg_pkt_builder`` object. This
should be moved out into the top module of the HCCStar emulator at some point.
The ``reg_pkt_builder``'s output signals would then be the current internal
``fifo_din`` and ``fifo_wr_en`` FIFO control signals.


Signals
-------

Inputs
  ``clk``
    240 MHz clock
  ``rst``
    Reset signal
  ``reg_read_i``
    Signals valid register data
  ``reg_addr_i[7:0]``
    Register address from LCB Wrapper
  ``reg_data_i[31:0]``
    Register value from LCB Wrapper
  ``fifo_rd_en_i``
    Read enable, passed directly to FIFO ``rd_en``

Outputs
  ``fifo_data_o[21:0]``
    FIFO word, passed directly from FIFO ``dout``
  ``fifo_valid_o``
    Signals that the FIFO output is valid data, passed directly from FIFO ``valid``
  ``fifo_empty_o``
    Signals empty FIFO, passed directly from FIFO ``empty``

Internal
    ``reg_addr[7:0]``
      Used to store register address across multiple clock cycles
    ``reg_data[31:0]``
      Used to store register value across multiple clock cycles
    ``fifo_word[15:0]``
      Stores a single word of the full packet as it is being constructed
    ``fifo_last``
      Single bit to represent the last word in a register read packet
    ``fifo_din[21:0]``
      The input to the internal FIFO, consists of the FIFO word, last bit, and
      data length
    ``fifo_wr_en``
      Write enable for the internal FIFO
    ``state``
      Of type ``t_states``, stores the state of the SM


Description
-----------

The layout of the 48-bit register read packet is as follows:

+--------------------------------------------------------+
| 48-Bit Register Read Packet                            |
+==========+==================+================+=========+
| 4 Bits   | 8 Bits           | 32 Bits        | 4 Bits  |
+----------+------------------+----------------+---------+
| TYP Code | Register Address | Register Value | Padding |
+----------+------------------+----------------+---------+

This 48-bit packet is split into three 16-bit chunks in order to maximize the
amount of data read by the packer per clock cycle. Appended to the ends of the
packet are the start-of-packet and end-of-packet frames, defined as

+---------------------+
| Start-of-Packet     |
+==========+==========+
| K28.5    | K28.1    |
+----------+----------+
| ``0xbc`` | ``0x3c`` |
+----------+----------+

+---------------------+
| End-of-Packet       |
+==========+==========+
| K28.6    | K28.5    |
+----------+----------+
| ``0xdc`` | ``0xbc`` |
+----------+----------+

where K28.5, K28.1 and K28.6 are the 8b/10b IDLE, Start-of-Packet, and
End-of-Packet codes, respectively.

A six-state synchronous FSM with an idle state (Idle) and five write states
(Header, Write1, Write2, Write3, Trailer) generates the five 16-bit words.

The state machine controls the inputs to the FIFO through ``fifo_word``,
``fifo_last``, and ``fifo_wr_en``. The actual input to the fifo, ``fifo_din``,
is a combination of the last bit, the data length, and the fifo word:

+-----------------------------------+
| 22-Bit FIFO Data Input            |
+===========+=============+=========+
| 1 Bit     | 5 Bits      | 16 Bits |
+-----------+-------------+---------+
| Last Flag | Data Length | Data    |
+-----------+-------------+---------+

Since the data length is always 16, it is stored as a 5-bit constant
``0b10000``. The "last flag", ``fifo_last``, signals the end of a
register read packet, so the third word written to the FIFO should have
``fifo_last`` set.

The state machine diagram can be seen below.

.. tikz:: State machine diagram for the register read packet builder.
   :libs: automata, positioning, arrows, external

    [->,
     >=stealth, node distance=3cm,
     every state/.style={
        rectangle,
        rounded corners,
        draw=black,
        very thick,
        minimum height=2em,
        inner sep=2pt,
        text centered,
        fill=gray!10
     },
     initial text=$ $
    ]
    \node[state, initial] (s0) {Idle};
    \node[state, right of=s0, xshift=1.25cm] (s1) {Header};
    \node[state, right of=s1] (s2) {Write2};
    \node[state, below of=s2] (s3) {Write2};
    \node[state, left of=s3] (s4) {Write3};
    \node[state, left of=s4] (s5) {Trailer};
    \draw   (s0) edge[loop below] (s0)
            (s0) edge[above] node{\tt reg\_read\_i=1} (s1)
            (s1) edge (s2)
            (s2) edge (s3)
            (s3) edge (s4)
            (s4) edge (s5)
            (s5) edge[bend right] (s0);

The default Idle state continually looks for new register data by checking
``reg_read_i``. As soon as this signal is high, the values from ``reg_addr_i``
and ``reg_data_i`` are latched into ``reg_addr`` and ``reg_data``, and at the
next clock cycle the FSM will enter the Header state. When there is no new
register data, the FSM remains in the Idle state.

In the Header state, the first 16-bit chunk is loaded into ``fifo_data``,
consisting of the K28.5 and K28.1 codes. ``fifo_last`` is held low, as there
are still more words in the packet. At the next clock cycle, the FSM will enter
the Write1 state.

In the Write1 state, the second 16-bit chunk is loaded into ``fifo_data``,
consisting of the 4-bit TYP code, the 8-bit register address, and the 4 MSBs
of the register's value. ``fifo_last`` is set to 0, as there are still three
more words in the packet. ``fifo_wr_en`` is activated to write the resulting
``fifo_din`` into the FIFO. At the next clock cycle, the FSM will enter the
Write2 state.

In the Write2 state, the third 16-bit chunk is loaded into ``fifo_data``,
consisting of the next 16 bits of the register's value (i.e. bits 27 through
12). Again, ``fifo_last`` is held low, and ``fifo_wr_en`` is held high. At the
next clock cycle, the FSM will enter the Write3 state.

In the Write3 state, the fourth 16-bit chunk is loaded into ``fifo_data``,
consisting of the next final 12 bits of the register's value, and four bits of
padding. This padding is always four zero bits, so is stored in the ``PADDING``
constant.At the next clock cycle, the FSM will
enter the Trailer state.

In the Trailer state, the last chunk is loaded into ``fifo_data``, consisting
of the K28.6 and K28.5 codes. Now, ``fifo_last`` is activated to indicate the
last word in the packet. ``fifo_wr_en`` is still set high.  The FSM returns to
the Idle state at the next clock cycle.

Upon returning to the Idle state, all FIFO inputs are zeroed out.

The register address, data, and read signals are all originally produced in the
40 MHz domain by the LCB wrapper. These are chopped into the 240 MHz domain
with the ``gbt_rx_flag``. There are six 240 MHz cycles per 40 MHz cycle, so
there is no need for a FIFO on the register address/data inputs as the packet
production only takes four clock cycles.
