----------------------------------------------------------------------------------
-- Company: UBC
-- Engineer: Dominique Trischuk
--
-- Create Date: 01/17/2018 02:51:55 PM
-- Design Name:
-- Module Name: hccstaremu - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;

library hccemu_pktgen;
  use hccemu_pktgen.HCCStar_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hccstaremu is
  port (
    clk                       : in    std_logic;
    l0a_i                     : in    std_logic;
    l0a_tag_i                 : in    std_logic_vector(6 downto 0);
    bcr_i                     : in    std_logic;
    trig_sel                  : in    std_logic;
    rst                       : in    std_logic;
    chunk_length_i            : in    std_logic_vector(11 downto 0);
    itks_emu_hits             : in    std_logic_vector(16 downto 0);
    itks_emu_hits_valid       : in    std_logic;
    itks_emu_hits_full        : in    std_logic;
    itks_emu_hits_empty       : in    std_logic;
    itks_emu_hits_almost_full : in    std_logic;
    itks_emu_rd_ctl           : in    std_logic;
    itks_emu_rd_en            : out   std_logic;
    elinkdata_o               : out   to_elink;
    elinkdata_rdy_o           : out   std_logic;
    last_word                 : out   std_logic;
    reg_read_i                : in    std_logic;
    reg_addr_i                : in    std_logic_vector( 7 downto 0);
    reg_data_i                : in    std_logic_vector(31 downto 0)
  );
end entity hccstaremu;

architecture hccstar_body_str of hccstaremu is

  signal data_prepacked : prepacked;
  signal data_packed    : packed_plus_last; --packed +1 for last hit
  --Trig Emu Signals
  signal rd_en_event  : std_logic;
  signal empty_event  : std_logic;
  signal valid_event  : std_logic;
  signal event_info_s : event_info;

  --Hit Gen Signal
  signal empty_hitgen : std_logic;
  signal rd_en_hitgen : std_logic;
  signal valid_hitgen : std_logic;

  -- Packer signals
  signal evbready       : std_logic;
  signal packerlast     : std_logic;
  signal packerready    : std_logic;
  signal data_last      : std_logic;
  signal packerout      : packed;
  signal packeroutvalid : std_logic;
  signal packedcount    : packerlong;
  signal pckrst         : std_logic := '1';
  signal pckrst1        : std_logic := '0';

  signal rd_en_packer       : std_logic;
  signal full_packer        : std_logic;
  signal almost_full_packer : std_logic;
  signal empty_packer       : std_logic; -- := '1';
  signal valid_packer       : std_logic;
  signal k_in               : std_logic;
  signal last_word_o        : std_logic;
  --signal to_elink          : encoded;

  component event_builder is
    port (
      clk_40_i     : in    std_logic;
      trig_sel_i   : in    std_logic;
      l0a_i        : in    std_logic;
      l0a_tag_i    : in    std_logic_vector(6 downto 0);
      rst          : in    std_logic;
      bcr_i        : in    std_logic;
      rd_en_event  : in    std_logic;
      start_event  : in    std_logic;
      event_info_o : out   event_info;
      empty_event  : out   std_logic;
      valid_event  : out   std_logic
    );
  end component event_builder;

  component hccstar_hitgen is
    port (
      clk                       : in    std_logic;
      rst                       : in    std_logic;
      trig_preamble             : in    trig_info;
      empty_trig                : in    std_logic;
      valid_trig                : in    std_logic;
      rd_en_hitgen              : in    std_logic;
      chunk_length              : in    std_logic_vector(11 downto 0);
      itks_emu_hits             : in    std_logic_vector(16 downto 0);
      itks_emu_hits_valid       : in    std_logic;
      itks_emu_hits_empty       : in    std_logic;
      itks_emu_hits_full        : in    std_logic;
      itks_emu_hits_almost_full : in    std_logic;
      itks_emu_rd_ctl           : in    std_logic;
      data                      : out   prepacked;
      rd_en_trig                : out   std_logic;
      empty_hitgen_out          : out   std_logic;
      valid_hitgen_out          : out   std_logic;
      itks_emu_rd_en            : out   std_logic
    );
  end component hccstar_hitgen;

  component packer is
    port (
      clk            : in    std_logic;
      reset          : in    std_logic;
      packerin       : in    packed;
      packerinlength : in    std_logic_vector(4 downto 0);
      packerinvalid  : in    std_logic;
      packerinlast   : in    std_logic;
      packerready    : out   std_logic;
      evbready       : in    std_logic;
      packerout      : out   packed;
      packeroutvalid : out   std_logic;
      packeroutlast  : out   std_logic;
      packercnt      : out   packerlong
    );
  end component packer;

  component hccstar_packer_fifo is
    port (
      clk         : in    std_logic;
      srst        : in    std_logic;
      din         : in    packed_plus_last;
      wr_en       : in    std_logic;
      rd_en       : in    std_logic;
      dout        : out   packed_plus_last;
      full        : out   std_logic;
      almost_full : out   std_logic;
      empty       : out   std_logic;
      valid       : out   std_logic
    );
  end component hccstar_packer_fifo;

  component endofemu is
    port (
      clk                : in    std_logic;
      rst                : in    std_logic;
      din                : in    packed_plus_last;
      empty_packer       : in    std_logic;
      valid_packer       : in    std_logic;
      full_packer        : in    std_logic;
      almost_full_packer : in    std_logic;
      rd_en_packer       : out   std_logic;
      k_in               : out   std_logic;
      elinkdata_o        : out   to_elink;
      last_word          : out   std_logic;
      data_rdy           : out   std_logic
      --to_elink           : out encoded  --for 8b option
    );
  end component endofemu;

  component reg_pkt_builder is
    port (
      clk          : in    std_logic;
      rst          : in    std_logic;
      reg_read_i   : in    std_logic;
      reg_addr_i   : in    std_logic_vector( 7 downto 0);
      reg_data_i   : in    std_logic_vector(31 downto 0);
      fifo_rd_en_i : in    std_logic;
      fifo_data_o  : out   std_logic_vector(21 downto 0);
      fifo_valid_o : out   std_logic;
      fifo_empty_o : out   std_logic
    );
  end component;

  component packer_data_selector is
    port (
      clk             : in    std_logic;
      rst             : in    std_logic;
      hitgen_data_i   : in    std_logic_vector(21 downto 0);
      hitgen_empty_i  : in    std_logic;
      hitgen_valid_i  : in    std_logic;
      hitgen_rd_en_o  : out   std_logic;
      rrpgen_data_i   : in    std_logic_vector(21 downto 0);
      rrpgen_empty_i  : in    std_logic;
      rrpgen_valid_i  : in    std_logic;
      rrpgen_rd_en_o  : out   std_logic;
      packer_data_o   : out   std_logic_vector(15 downto 0);
      packer_length_o : out   std_logic_vector( 4 downto 0);
      packer_last_o   : out   std_logic;
      packer_valid_o  : out   std_logic;
      packer_ready_i  : in    std_logic
    );
  end component;

  signal rrpkt_rd_en : std_logic;
  signal rrpkt_data  : std_logic_vector(21 downto 0);
  signal rrpkt_valid : std_logic;
  signal rrpkt_empty : std_logic;

  signal packer_input        : std_logic_vector(15 downto 0);
  signal packer_input_valid  : std_logic;
  signal packer_input_last   : std_logic;
  signal packer_input_length : std_logic_vector(4 downto 0);

begin

  --data_packed <= (others => '0');
  last_word <= last_word_o;
  --  start_end_emu <= packerOutValid;

  event_builder_1 : component event_builder
    port map (
      clk_40_i     => clk,
      trig_sel_i   => trig_sel,
      l0a_i        => l0a_i,
      l0a_tag_i    => l0a_tag_i,
      rst          => rst,
      bcr_i        => bcr_i,
      rd_en_event  => rd_en_event,
      start_event  => itks_emu_rd_ctl,
      event_info_o => event_info_s,
      empty_event  => empty_event,
      valid_event  => valid_event
    );

  hitgen : component hccstar_hitgen
    port map (
      clk                       => clk,
      rst                       => rst,
      trig_preamble             => event_info_s,
      empty_trig                => empty_event,
      valid_trig                => valid_event,
      data                      => data_prepacked,
      rd_en_trig                => rd_en_event,
      empty_hitgen_out          => empty_hitgen,
      itks_emu_hits             => itks_emu_hits,
      itks_emu_rd_ctl           => itks_emu_rd_ctl,
      itks_emu_hits_valid       => itks_emu_hits_valid,
      itks_emu_rd_en            => itks_emu_rd_en,
      itks_emu_hits_full        => itks_emu_hits_full,
      itks_emu_hits_almost_full => itks_emu_hits_almost_full,
      itks_emu_hits_empty       => itks_emu_hits_empty,
      rd_en_hitgen              => rd_en_hitgen,
      valid_hitgen_out          => valid_hitgen,
      chunk_length              => chunk_length_i
    );

  evbready <= '1';

  -- generate a reset to initialize packer
  genreset : process (clk, pckrst, pckrst1) is
  begin

    if (rising_edge(clk)) then
      pckrst1 <= pckrst;
      if (pckrst1 = '1') then
        pckrst <= '0';
      end if;
    end if;

  end process genreset;

  packer_1 : component packer
    port map (
      clk            => clk,
      reset          => pckrst1,
      packerin       => packer_input,
      packerinlength => packer_input_length,
      packerinvalid  => packer_input_valid,
      packerinlast   => packer_input_last,
      -- PackerIn       => data_prepacked (20 downto 5),
      -- PackerInLength => data_prepacked(4 downto 0),
      -- PackerInValid  => valid_hitgen,
      -- PackerInLast   => data_prepacked(21), --last packet (trailer)
      packerready    => packerready,
      evbready       => evbready,
      packerout      => packerout,
      packeroutvalid => packeroutvalid,
      packeroutlast  => packerlast,
      packercnt      => packedcount
    );

  hccstar_packer_fifo_1 : component hccstar_packer_fifo
    port map (
      clk              => clk,
      srst             => rst,
      din(16)          => packerlast,
      din(15 downto 0) => packerout,
      wr_en            => packeroutvalid,
      rd_en            => rd_en_packer,
      dout             => data_packed,
      full             => full_packer,
      almost_full      => almost_full_packer,
      empty            => empty_packer,
      valid            => valid_packer
    );

  endofemu_1 : component endofemu
    port map (
      clk                => clk,
      rst                => rst,
      din                => data_packed,
      empty_packer       => empty_packer,
      valid_packer       => valid_packer,
      full_packer        => full_packer,
      almost_full_packer => almost_full_packer,
      rd_en_packer       => rd_en_packer,
      k_in               => k_in,
      elinkdata_o        => elinkdata_o,
      last_word          => last_word_o,
      data_rdy           => elinkdata_rdy_o
    );

  reg_pkt_builder_1 : component reg_pkt_builder
    port map (
      clk          => clk,
      rst          => rst,
      reg_read_i   => reg_read_i,
      reg_addr_i   => reg_addr_i,
      reg_data_i   => reg_data_i,
      fifo_rd_en_i => rrpkt_rd_en,
      fifo_data_o  => rrpkt_data,
      fifo_valid_o => rrpkt_valid,
      fifo_empty_o => rrpkt_empty
    );

  packer_data_selector_1 : component packer_data_selector
    port map (
      clk             => clk,
      rst             => rst,
      hitgen_data_i   => data_prepacked,
      hitgen_empty_i  => empty_hitgen,
      hitgen_valid_i  => valid_hitgen,
      hitgen_rd_en_o  => rd_en_hitgen,
      rrpgen_data_i   => rrpkt_data,
      rrpgen_empty_i  => rrpkt_empty,
      rrpgen_valid_i  => rrpkt_valid,
      rrpgen_rd_en_o  => rrpkt_rd_en,
      packer_data_o   => packer_input,
      packer_length_o => packer_input_length,
      packer_last_o   => packer_input_last,
      packer_valid_o  => packer_input_valid,
      packer_ready_i  => packerready
    );

end architecture hccstar_body_str;
