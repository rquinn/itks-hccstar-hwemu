
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
-- library xil_defaultlib;
-- library hcc; 

--use hcc.centralRouter_package.all;
--use hcc.HCCStar_pkg.all;


entity itks_data_generator is
  port (
    clk240                 : in  std_logic; 
    itks_fifo_rst          : in  std_logic; 
    itks_fifo_rd_en        : in  std_logic; 
    itks_fifo_rd_ctl       : in  std_logic;
    itks_fifo_wr_en        : in  std_logic; 
    itks_fifo_data         : in  std_logic_vector(16 downto 0); 
    itks_fifo_wr_done      : out std_logic; 
    itks_fifo_full         : out std_logic; 
    itks_fifo_almost_full  : out std_logic; 
    itks_fifo_empty        : out std_logic; 
    hccstar_emu_hits       : out std_logic_vector(16 downto 0); 
    hccstar_emu_hits_valid : out std_logic
    
  ); 
end entity itks_data_generator; 

architecture Behavioral of itks_data_generator is

    COMPONENT itks_data_blk_mem_0 IS
        PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(16 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(16 DOWNTO 0)
     );
    END COMPONENT itks_data_blk_mem_0;

  --State Machine
  type itks_data_gen_type is
    (IDLE, WR, WR_DONE,RD_START, RD,RD_DONE, RST); 
  signal state_reg, state_next : itks_data_gen_type := IDLE; --for state machine
  
  -- Signals 
  signal wr_en_fifo       : STD_LOGIC                     := '0'; 
  signal wr_ack_fifo      : STD_LOGIC                     := '0'; 
  signal rst_fifo         : STD_LOGIC                     := '0'; 
  signal rd_en_fifo       : STD_LOGIC                     := '0'; 
  signal empty_fifo       : STD_LOGIC                     := '1'; 
  signal valid_fifo       : STD_LOGIC                     := '0'; 
  signal full_fifo        : STD_LOGIC                     := '0'; 
  signal almost_full_fifo : STD_LOGIC                     := '0'; 
  signal din_fifo         : STD_LOGIC_VECTOR(16 downto 0) := (others => '0'); 
  signal dout_fifo        : STD_LOGIC_VECTOR(16 downto 0) := (others => '0'); 
  signal data             : STD_LOGIC_VECTOR(16 downto 0) := (others => '0'); 
  signal last_hit         : STD_LOGIC; 
  signal wr_done_sig      : STD_LOGIC; 
  signal itks_fifo_wr_en_s : STD_LOGIC;
  signal itks_fifo_rd_en_s : STD_LOGIC;

  ------------------------------------------------------------------------------
  -- signals for bram 
  signal ena : STD_LOGIC;
  signal wea : STD_LOGIC;
  -- old length of addr when I had a different write depth for BRAM
  --signal current_addr : unsigned(11 downto 0)  := (others => '0');
  signal current_addr : unsigned(11 downto 0)  := (others => '0');
  signal rd_addr : unsigned(11 downto 0)  := (others => '0');
  signal wr_addr, wr_addr1 : unsigned(11 downto 0)  := (others => '0');
  signal max_addr : unsigned(11 downto 0)  := (others => '0');
  signal valid_data : std_logic;
  signal valid_data1 : std_logic;
  signal data1 : STD_LOGIC_VECTOR(16 downto 0) := (others => '0'); 
   
  
begin
  
  itks_fifo_empty               <= empty_fifo; 
  itks_fifo_full                <= full_fifo; 
  hccstar_emu_hits              <= data; 
  hccstar_emu_hits_valid        <= valid_data;
  --hccstar_emu_hits(16)          <= last_hit; 
  itks_fifo_wr_done             <= wr_done_sig; 
  itks_fifo_wr_en_s             <= itks_fifo_wr_en;
  itks_fifo_almost_full         <=  almost_full_fifo;
  itks_fifo_rd_en_s  <=  itks_fifo_rd_en;
  
  
  --itks_data_fifo_0: entity hcc.itks_data_fifo
  --itks_data_fifo_0 : itks_data_fifo
  
  --  port map (
  --    clk         => clk240, 
  --    srst        => rst_fifo,
  --    din         => din_fifo,
  --    wr_en       => wr_en_fifo,
  --    wr_ack      => wr_ack_fifo,
  --    rd_en       => rd_en_fifo,
  --    dout        => dout_fifo,
  --    full        => full_fifo,
  --    almost_full => almost_full_fifo,
  --    empty       => empty_fifo,
  --    valid       => valid_fifo
      
  --  ); 

  itks_data_bram_0 : itks_data_blk_mem_0
    PORT MAP (
    clka  => clk240,
    ena  => ena,
    wea(0)  => wea,
    addra  => std_logic_vector(current_addr),
    dina  => itks_fifo_data,
    douta => data
  );

  current_addr  <=  wr_addr1 when (state_reg = WR) or (state_reg = WR_DONE) else rd_addr;
  valid_data  <= '1' when state_reg = RD else '0';
 
  
  process(clk240, itks_fifo_rst,state_reg,max_addr,rd_addr,data,itks_fifo_rd_en) 
  begin
    if rising_edge(clk240) then 
      state_reg <= state_next; 
      
      --defaults 
      wr_en_fifo             <= '0'; 
      rst_fifo               <= '0'; 
      rd_en_fifo             <= '0'; 
      din_fifo               <= (others => '0'); 
      wr_done_sig            <= '0'; 
      
      --hccstar_emu_hits        <= (others => '0'); 
      --hccstar_emu_hits_valid <= valid_fifo; 
      wea                    <= '0'; 
      ena                    <= itks_fifo_rd_en;
      --valid_data  <= '0';
      valid_data1 <= valid_data;
      wr_addr1  <=  wr_addr;
      --data_o  <= (others => '0'); 
      

      if (state_next = IDLE) then  
        --last_hit  <= '0'; 
        if (itks_fifo_rd_en = '1') then 
          wea  <= '0';
          --current_addr  <=  rd_addr;
          rd_addr  <= rd_addr + 1;
        end if;

      end if;


      if (state_reg = WR) then 
        wea  <= '1'; 
        ena  <= '1';
        --current_addr  <= wr_addr; 
        max_addr  <=  wr_addr;
        wr_addr  <= wr_addr + 1;
        wr_done_sig <= '1';    
      end if; 
      
      if (state_reg = WR_DONE) then 
        wea  <= '0'; 
        ena  <= '0'; 
        wr_done_sig <= '1'; 
      end if; 
      
      --if (state_next = RD) then 
        
      --end if; 
      if (state_reg = RD_START) then 
        --ena <= itks_fifo_rd_en;
        wea  <= '0';
        --current_addr  <=  rd_addr;
        rd_addr  <= rd_addr + 1;

      end if;

      if (state_reg = RD) then 
        --ena <= itks_fifo_rd_en;
        wea  <= '0';
        --valid_data  <= '1';
        --current_addr  <=  rd_addr;
        
        --data1  <= data;
        
      

        --if (valid_data1 = '1') then 
        --  if (data(16) = '1') then 
        --      last_hit <= '1';
        --  end if; 
        --end if;

        --if (valid_data = '1') then 
        --  --data_o     <= data(16 downto 0); 
        --  --last_hit <= data(16); 
        --end if; 

        if (data(16) = '1') then 
          ena <= '0';
        else
          rd_addr  <= rd_addr + 1;

        end if;

        if (rd_addr = max_addr) then 
          rd_addr  <= (others => '0');
        end if;
        

      end if; 
      
      if (state_reg = RST) then 
        rst_fifo      <= '1'; 
        rd_addr       <= (others => '0');
        wr_addr       <= (others => '0');
        --current_addr  <= (others => '0');
        max_addr      <= (others => '0');
        
      end if; 
      
      
    end if; 
  end process; 
  
  
  process(state_reg,itks_fifo_rst,itks_fifo_rd_en,itks_fifo_wr_en,last_hit,data,valid_data1)
  begin
    case state_reg is
        
        
      when IDLE => 
        last_hit  <= '0';
        state_next <= IDLE; 
        
        if (itks_fifo_wr_en = '1') then
          state_next <= WR; 
        end if; 
        
        if (itks_fifo_rd_en = '1') then
          state_next <= RD_START; 
        end if; 
        
        if (itks_fifo_rst='1') then
          state_next <= RST; 
        end if; 
        
        
      when WR => 
        state_next <= WR_DONE; 
        if (itks_fifo_rst='1') then
          state_next <= RST; 
        end if; 
        
      when WR_DONE => 

        state_next <= WR_DONE; 
        
        if (itks_fifo_wr_en = '0') then
          state_next <= IDLE; 
        end if; 
        if (itks_fifo_rst='1') then
          state_next <= RST; 
        end if; 
        
      when RD_START => 
        state_next <= RD;
      
      when RD => 
        state_next <= RD; 

        --if last_hit = '1' then 
        --  state_next <= IDLE; 
        --end if; 
         if (data(16) = '1') then 
          state_next <= RD_DONE; 
        end if; 

        if (itks_fifo_rd_en = '0') then 
          state_next <= IDLE; 
        end if; 
        
        
        if (itks_fifo_rst='1') then
          state_next <= RST; 
        end if; 

      when RD_DONE  => 
        state_next <= IDLE;
        
        
      when RST => 
        if (itks_fifo_rst='1') then
          state_next <= RST; 
        else
          state_next <= IDLE; 
        end if; 
        
        
        
    end case; 
  end process; 
  
  
  --ila_itks_data_gen : entity hcc.ila_itks_data_gen
  --          port map (
  --            clk         => clk240,
  --            probe0(0)   => wea,
  --            probe1      => data,
  --            probe2(0)   => ena,
  --            probe3(0)   => itks_fifo_rd_en_s,
  --            probe4(0)   => wr_done_sig,
  --            probe5(0)   => valid_data,
  --            probe6(0)   => itks_fifo_wr_en_s
  --          );   

  
end Behavioral; 

