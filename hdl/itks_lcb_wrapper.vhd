--
-- Wrapper for LCB decoder
--

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library hccemu_lcbdec;
  use hccemu_lcbdec.lcb_pkg_globals.all;

entity itks_lcb_wrapper is
  port (
    clk40   : in    std_logic;
    rx_flag : in    std_logic;
    lcb_4b  : in    std_logic_vector(3 downto 0);
    rst     : in    std_logic;
    l0a     : out   std_logic;
    bcr     : out   std_logic;
    l0a_tag : out   std_logic_vector(6 downto 0);

    reg_read_o : out   std_logic;
    reg_addr_o : out   std_logic_vector( 7 downto 0);
    reg_data_o : out   std_logic_vector(31 downto 0)
  );
end entity itks_lcb_wrapper;

---------------------------------------------------------------------------

architecture rtl of itks_lcb_wrapper is

  component lcb_decoder is
    generic (
      asic_is_abc : natural := 1
    );
    port (
      hccid_i          : in    std_logic_vector(3 downto 0);
      par4_i           : in    std_logic_vector(3 downto 0);
      locked_i         : in    std_logic;
      frame_sync_i     : in    std_logic_vector(1 downto 0);
      decoder_err_o    : out   std_logic;
      state_hamm_err_o : out   std_logic;
      tmr_err_o        : out   std_logic;
      l0a_o            : out   std_logic;
      l0a_tag_o        : out   std_logic_vector(6 downto 0);
      bcr_o            : out   std_logic;
      cbus_data_o      : out   std_logic_vector(6 downto 0);
      cbus_valid_o     : out   std_logic;
      cbus_soc_o       : out   std_logic;
      cbus_eoc_o       : out   std_logic;
      cbus_all_o       : out   std_logic_vector(8 downto 0);
      fast_cmd_o       : out   std_logic_vector(3 downto 0);
      fast_cmd_valid_o : out   std_logic;
      clk40            : in    std_logic;
      rstb             : in    std_logic;
      initb            : in    std_logic
    );
  end component lcb_decoder;

  component lcb_frame_sync is
    port (
      par4_o           : out   std_logic_vector(3 downto 0);
      frame_sync_o     : out   std_logic_vector(1 downto 0);
      dbg_frame_o      : out   std_logic_vector(15 downto 0);
      dbg_symbol_o     : out   std_logic_vector(7 downto 0);
      nibble_err_o     : out   std_logic;
      locked_o         : out   std_logic;
      frame_raw_o      : out   std_logic_vector(15 downto 0);
      decoder_err_i    : in    std_logic;
      errcount_ovfl_o  : out   std_logic;
      errcount_o       : out   std_logic_vector(15 downto 0);
      errcount_thr_i   : in    std_logic_vector(15 downto 0);
      errcount_clr_i   : in    std_logic;
      decode_err_o     : out   std_logic;
      state_hamm_err_o : out   std_logic;
      lock_hamm_err_o  : out   std_logic;
      clk40            : in    std_logic;
      initb            : in    std_logic;
      par4_i           : in    std_logic_vector(3 downto 0)
    );
  end component lcb_frame_sync;

  component lcb_regblock_if is
    generic (
      asic_is_abc : natural := 1
    );
    port (
      chipid_i : in    std_logic_vector(3 downto 0);
      -- RegBlock interface
      rb_addr_o : out   std_logic_vector(7 downto 0);
      rb_rnw_o  : out   std_logic;
      rb_init_o : out   std_logic;
      rb_load_o : out   std_logic;
      rb_sdat_o : out   std_logic;
      rb_shen_o : out   std_logic;
      rb_busy_i : in    std_logic;
      -- CmdFIFO
      cfifo_all_i   : in    std_logic_vector(8 downto 0);
      cfifo_empty_i : in    std_logic;
      cfifo_re_o    : out   std_logic;
      --Other
      --err_soc_u_o       : out std_logic;
      --err_eoc_u_o       : out std_logic;
      --err_words_ovrf_o  : out std_logic;
      --err_words_undf_o       : out std_logic;
      scmd_err_o       : out   std_logic;
      state_hamm_err_o : out   std_logic;
      tmr_err_o        : out   std_logic;
      k2_pending_o     : out   std_logic;
      -- Infra
      clk40 : in    std_logic;
      rstb  : in    std_logic;
      initb : in    std_logic
    );
  end component lcb_regblock_if;

  -- component lcb_regblock is
  component lcb_reg is
    -- generic (
    --     NREG : natural range 0 to 255 := 32
    -- );
    port (
      rb_addr_i : in    std_logic_vector(7 downto 0);
      rb_rnw_i  : in    std_logic;
      rb_init_i : in    std_logic;
      rb_load_i : in    std_logic;
      rb_sdat_i : in    std_logic;
      rb_shen_i : in    std_logic;
      rb_busy_o : out   std_logic;

      ----------
      -- reg_o : out slv32_array(NREG-1 downto 0);
      reg_o       : out   unsigned(31 downto 0);
      reg_valid_o : out   std_logic;

      -- Infra
      clk40 : in    std_logic;
      rstb  : in    std_logic
    );
    -- end component lcb_regblock;
  end component lcb_reg;

  ---------------------------------------------------------------------------
  -- signals
  ---------------------------------------------------------------------------

  signal hccid_i              : std_logic_vector(3 downto 0) := (others  => '0');
  signal par4_i               : std_logic_vector(3 downto 0);
  signal par4_o               : std_logic_vector(3 downto 0);
  signal locked_o             : std_logic;
  signal frame_sync_o         : std_logic_vector(1 downto 0);
  signal decoder_err_o        : std_logic;
  signal dec_state_hamm_err_o : std_logic;
  signal tmr_err_o            : std_logic;
  signal l0a_o                : std_logic;
  signal l0a_tag_o            : std_logic_vector(6 downto 0);
  signal bcr_o                : std_logic;
  signal cbus_data_o          : std_logic_vector(6 downto 0);
  signal cbus_valid_o         : std_logic;
  signal cbus_soc_o           : std_logic;
  signal cbus_eoc_o           : std_logic;
  signal cbus_all_o           : std_logic_vector(8 downto 0);
  signal fast_cmd_o           : std_logic_vector(3 downto 0);
  signal fast_cmd_valid_o     : std_logic;
  signal rstb                 : std_logic;
  signal initb                : std_logic;
  --signal align_done         : std_logic;

  signal dbg_frame_o           : std_logic_vector(15 downto 0);
  signal dbg_symbol_o          : std_logic_vector(7 downto 0);
  signal nibble_err_o          : std_logic;
  signal frame_raw_o           : std_logic_vector(15 downto 0);
  signal decoder_err_i         : std_logic;
  signal errcount_ovfl_o       : std_logic;
  signal errcount_o            : std_logic_vector(15 downto 0);
  signal errcount_thr_i        : std_logic_vector(15 downto 0);
  signal errcount_clr_i        : std_logic;
  signal sync_state_hamm_err_o : std_logic;
  signal lock_hamm_err_o       : std_logic;
  signal decode_err_o          : std_logic;

  signal rb_addr     : std_logic_vector(7 downto 0);
  signal rb_rnw      : std_logic;
  signal rb_init     : std_logic;
  signal rb_load     : std_logic;
  signal rb_sdat     : std_logic;
  signal rb_shen     : std_logic;
  signal rb_busy     : std_logic;
  signal rb_valid    : std_logic;
  signal cfifo_all   : std_logic_vector(8 downto 0);
  signal cfifo_empty : std_logic;
  signal cfifo_re    : std_logic;

  signal rb_sdato     : std_logic;
  signal rb_sdato_vld : std_logic;

begin

  par4_i  <= lcb_4b;
  initb   <= not rst; --Matt uses reset opposite to FELIG
  l0a     <= l0a_o;
  l0a_tag <= l0a_tag_o;
  bcr     <= bcr_o;

  ----------------------------------------------------------------------------

  lcb_frame_sync_1 : entity hccemu_lcbdec.lcb_frame_sync
    port map (
      par4_o           => par4_o,
      frame_sync_o     => frame_sync_o,
      dbg_frame_o      => dbg_frame_o,
      dbg_symbol_o     => dbg_symbol_o,
      nibble_err_o     => nibble_err_o,
      locked_o         => locked_o,
      frame_raw_o      => frame_raw_o,
      decoder_err_i    => decoder_err_o,
      errcount_ovfl_o  => errcount_ovfl_o,
      errcount_o       => errcount_o,
      errcount_thr_i   => errcount_thr_i,
      errcount_clr_i   => errcount_clr_i,
      decode_err_o     => decode_err_o,
      state_hamm_err_o => sync_state_hamm_err_o,
      lock_hamm_err_o  => lock_hamm_err_o,
      clk40            => clk40,
      initb            => initb,
      par4_i           => par4_i
    );

  lcb_decoder_1 : entity hccemu_lcbdec.lcb_decoder
    generic map (
      asic_is_abc => 0
    )
    port map (
      hccid_i          => hccid_i,
      par4_i           => par4_o,
      locked_i         => locked_o,
      frame_sync_i     => frame_sync_o,
      decoder_err_o    => decoder_err_o,
      state_hamm_err_o => dec_state_hamm_err_o,
      tmr_err_o        => tmr_err_o,
      l0a_o            => l0a_o,
      l0a_tag_o        => l0a_tag_o,
      bcr_o            => bcr_o,
      cbus_data_o      => cbus_data_o,
      cbus_valid_o     => cbus_valid_o,
      cbus_soc_o       => cbus_soc_o,
      cbus_eoc_o       => cbus_eoc_o,
      cbus_all_o       => cbus_all_o,
      fast_cmd_o       => fast_cmd_o,
      fast_cmd_valid_o => fast_cmd_valid_o,
      clk40            => clk40,
      rstb             => rstb,
      initb            => initb
    );

  lcb_syncfifo_1 : entity hccemu_lcbdec.lcb_syncfifo
    port map (
      clk   => clk40,
      rstb  => initb,
      din   => cbus_all_o,
      we    => cbus_valid_o,
      dout  => cfifo_all,
      re    => cfifo_re,
      full  => open,
      empty => cfifo_empty
    );

  lcb_regblock_if_1 : entity hccemu_lcbdec.lcb_regblock_if
    generic map (
      asic_is_abc => 0
    )
    port map (
      chipid_i         => hccid_i,
      rb_addr_o        => rb_addr,
      rb_rnw_o         => rb_rnw,
      rb_init_o        => rb_init,
      rb_load_o        => rb_load,
      rb_sdat_o        => rb_sdat,
      rb_shen_o        => rb_shen,
      rb_busy_i        => rb_busy,
      cfifo_all_i      => cfifo_all,
      cfifo_empty_i    => cfifo_empty,
      cfifo_re_o       => cfifo_re,
      scmd_err_o       => open,
      state_hamm_err_o => open,
      tmr_err_o        => open,
      k2_pending_o     => open,
      clk40            => clk40,
      rstb             => rstb,
      initb            => initb
    );

  lcb_regblock_1 : entity hccemu_lcbdec.lcbtst_regblock16
    port map (
      rb_addr_i      => rb_addr,
      rb_rnw_i       => rb_rnw,
      rb_init_i      => rb_init,
      rb_load_i      => rb_load,
      rb_sdat_i      => rb_sdat,
      rb_shen_i      => rb_shen,
      rb_busy_o      => rb_busy,
      rb_sdato_o     => rb_sdato,
      rb_sdato_vld_o => rb_sdato_vld,
      -- regblock_o => open,
      -- regblock_flat_o => open,
      -- reg_done_o => open,
      -- reg00_o => open,
      -- reg01_o => open,
      -- reg02_o => open,
      -- reg03_o => open,
      -- reg04_o => open,
      -- reg05_o => open,
      -- reg06_o => open,
      -- reg07_o => open,
      -- reg08_o => open,
      -- reg09_o => open,
      -- reg10_o => open,
      -- reg11_o => open,
      -- reg12_o => open,
      -- reg13_o => open,
      -- reg14_o => open,
      -- reg15_o => open,
      -- reg32_o => open,
      clk40 => clk40,
      rstb  => rstb
    );

  lcb_srsync_1 : entity hccemu_lcbdec.lcb_srsync
    port map (
      clk40         => clk40,
      rstb          => rstb,
      rb_addr_i     => rb_addr,
      rb_rnw_i      => rb_rnw,
      rb_init_i     => rb_init,
      rb_sdat_i     => rb_sdato,
      rb_sdat_vld_i => rb_sdato_vld,
      reg_read_o    => reg_read_o,
      reg_addr_o    => reg_addr_o,
      reg_data_o    => reg_data_o
    );

end architecture rtl;
