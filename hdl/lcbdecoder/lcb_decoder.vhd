--
-- LCB Decoder
--
--
-- Matt Warren Aug 2016
--  
-- Last update Nov 2017
--
-- 20180425 Added decoder err resets command decoder post fifo via soc=eoc=1


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library hccemu_lcbdec;
use hccemu_lcbdec.lcb_pkg_globals.all;

entity lcb_decoder is
  generic(
    ASIC_IS_ABC : natural := 1
    );
  port(
    hccid_i       : in  std_logic_vector (3 downto 0);
    par4_i        : in  std_logic_vector (3 downto 0);
    locked_i      : in  std_logic;
    frame_sync_i  : in  std_logic_vector (1 downto 0);
    decoder_err_o : out std_logic;

    state_hamm_err_o : out std_logic;
    tmr_err_o        : out std_logic;

    -- Signals/data out
    l0a_o     : out std_logic;
    l0a_tag_o : out std_logic_vector (6 downto 0);
    --l0a_tag_valid_o : out std_logic;
    bcr_o     : out std_logic;

    ---- L1R3 signals
    --l1_valid_o   : out std_logic;
    --r3_valid_o   : out std_logic;
    --l1r3_valid_o : out std_logic;
    --r3_mask_o    : out std_logic_vector (4 downto 0);
    --l1r3_tag_o   : out std_logic_vector (6 downto 0);

    -- Register Access
    cbus_data_o      : out std_logic_vector (6 downto 0);
    cbus_valid_o     : out std_logic;
    cbus_soc_o       : out std_logic;
    cbus_eoc_o       : out std_logic;
    cbus_all_o       : out std_logic_vector (8 downto 0);
    fast_cmd_o       : out std_logic_vector (3 downto 0);
    fast_cmd_valid_o : out std_logic;

    -- Infra
    clk40 : in std_logic;               --40MHz BCO
    rstb  : in std_logic;               -- sync reset
    initb : in std_logic                -- async reset (ext,por)
    );

-- Declarations

end entity lcb_decoder;

---------------------------------------------------------------------------
architecture rtl of lcb_decoder is

  component lcb_tmr_reg
    generic (
      WIDTH   : natural := 8;
      RST_VAL : natural := 0
      );
    port(
      signal initb : in  std_logic;
      signal rstb  : in  std_logic;
      signal clk   : in  std_logic;
      signal ena   : in  std_logic;
      signal d     : in  unsigned(WIDTH-1 downto 0);
      signal q     : out unsigned(WIDTH-1 downto 0);
      signal err   : out std_logic
      );
  end component lcb_tmr_reg;


  signal frame_sync_q : std_logic_vector(1 downto 0);

  signal par4_q  : unsigned(3 downto 0);
  signal par4_sr : slv4_array(2 downto 0);

  signal symbol_in   : unsigned(7 downto 0);
  signal symbol_sync : std_logic;

  signal dec_6b       : unsigned(5 downto 0);
  signal dec_k        : unsigned(3 downto 0);
  signal dec_err      : std_logic;
  signal dec_bcr      : std_logic;
  signal dec_l0a_map  : unsigned(3 downto 0);
  signal dec_tag_msb  : std_logic;
  signal dec_tag_lsbs : unsigned(5 downto 0);
  signal dec_is_k     : std_logic;
  signal dec_is_cmd   : std_logic;
  signal dec_symb_err : std_logic;

  signal frame_err : std_logic;

  signal dec_k2_symbol   : unsigned(5 downto 0);
  signal dec_k2_startend : std_logic;

  signal dec_r3mask : unsigned(4 downto 0);

  signal bcr0         : std_logic;
  signal bcr_q        : unsigned(5 downto 0);
  signal l0a_sr       : unsigned(3 downto 0);
  signal l0a_sr_d     : unsigned(3 downto 0);
  signal l0a_map      : unsigned(3 downto 0);
  --signal l0a_delayed : unsigned(3 downto 0);
  signal l0a_map_load : std_logic;

  signal tag_load_l0a_go : std_logic;

  signal tag_counter  : unsigned(6 downto 0);
  signal tag_msb      : std_logic;
  signal tag_msb_load : std_logic;
  --signal l1r3_tag_load    : std_logic;

  signal cbus_valid   : std_logic;
  signal cbus_soc     : std_logic;
  signal cbus_eoc     : std_logic;
  signal cbus_data    : unsigned(6 downto 0);
  signal cbus_valid_q : std_logic;
  signal cbus_soc_q   : std_logic;
  signal cbus_eoc_q   : std_logic;
  signal cbus_data_q  : unsigned(6 downto 0);

  signal k3_bc          : unsigned(1 downto 0);
  signal fs_k3          : unsigned(3 downto 0);  -- added FA
  signal k3_data        : unsigned(3 downto 0);
  signal k3_go          : std_logic;
  signal k3_in_progress : std_logic;

  signal fcv_a : std_logic;
  signal fcv_b : std_logic;
  signal fcv_c : std_logic;

  --signal l1_valid   : std_logic;
  --signal r3_valid   : std_logic;
  --signal l1r3_valid : std_logic;
  --signal r3_mask    : unsigned(4 downto 0);
  --signal l1r3_tag   : unsigned(6 downto 0);

  -- State definitions using constants for easier Hamming
  --constant sUnlockedOrError : natural := 0;
  --constant sNibble0         : natural := 1;
  --constant sSymbol0         : natural := 2;
  --constant sWaitK1          : natural := 3;
  --constant sWaitTag         : natural := 4;
  --constant sWaitCmd         : natural := 5;
  --constant sK2WaitSymbol1  : natural := 6;
  --constant sK3WaitSymbol1  : natural := 7;

  --signal state  : natural range 0 to 7;
  --signal nstate : natural range 0 to 7;

  type t_states is (UnlockedOrError,  
                    Nibble0, Symbol0,
                    WaitK1, WaitTag, WaitCmd,
                    K2WaitSymbol1, K3WaitSymbol1,
                    InvalidState -- need to make full complement of 8 for SEE reasons
                    );

  signal state, nstate : t_states;
  signal state_uns     : unsigned(2 downto 0);

  signal state_hamm : unsigned(6 downto 0);
  --signal error_hamm : unsigned(1 downto 0);

  signal dbg_dec_cmdfrm : unsigned(48 downto 0);

  signal sigs_disable : std_logic;

  -- synopsys translate_off
  signal dbg_symb0_sync      : std_logic;
  signal dbg_symb1_sync      : std_logic;
  signal dbg_symb1_sync_10ps : std_logic;
  signal dbg_symb0           : unsigned(7 downto 0);
  signal dbg_symb1           : unsigned(7 downto 0);
  signal dbg_frame           : unsigned(15 downto 0);
  signal dbg_frame0          : unsigned(15 downto 0);
  signal dbg_dec_6b0         : unsigned(5 downto 0);
  signal dbg_dec_6b1         : unsigned(5 downto 0);
  signal dbg_bcr             : std_logic;
  signal dbg_l0a_map         : unsigned(3 downto 0);
  signal dbg_tag             : unsigned(6 downto 0);
  -- synopsys translate_on


  signal k3_data_err        : std_logic;
  signal l0a_map_err        : std_logic;
  signal l0a_sr_err         : std_logic;
  signal fast_cmd_valid_err : std_logic;
  signal decoder_err        : std_logic;

-------------------------------------------------------------------------------
begin

  tmr_err_o <= k3_data_err or l0a_map_err or l0a_sr_err or fast_cmd_valid_err;



  -- NOTE: the first few processes are reset by initb, asynchronously 

-- get frame_sync and par4 into 40MHz domain

  prc_fsync40 : process (initb, clk40)
  begin
    if (initb = '0') then
      frame_sync_q <= "00";
      par4_q       <= (others => '0');

    elsif rising_edge(clk40) then
      frame_sync_q <= frame_sync_i;
      par4_q       <= unsigned(par4_i);

    end if;
  end process;

  par4_sr(0)          <= par4_q;
  par4_sr(2 downto 1) <= par4_sr(1 downto 0) when rising_edge(clk40);

  --par4_o    <= par4_sr(2);
  symbol_in   <= (par4_sr(1) & par4_sr(0));
  symbol_sync <= frame_sync_q(0);


  ----------------------------------------------------------------------------------- 
  -- 8b/6b symbol decoder
  -- decoder output must be strobed by frame_sync

  dec_8b_6b(symbol_in,                  --in
            dec_6b, dec_k, dec_err);    -- out

  dec_symb_err <= (dec_err and symbol_sync);


  prc_decerr_clk : process (initb, clk40)
  begin
    if (initb = '0') then
      decoder_err <= '0';

    elsif rising_edge(clk40) then
      decoder_err <= dec_symb_err or frame_err;

    end if;
  end process;

  decoder_err_o <= decoder_err;


  -- These mappings are not valid all the time
  --  Must use in conjunction with frame_sync

  dec_bcr      <= dec_6b(5);
  dec_l0a_map  <= dec_6b(4 downto 1);
  dec_tag_msb  <= dec_6b(0);
  dec_tag_lsbs <= dec_6b;
  dec_is_k     <= '1' when (dec_k /= "0000")                                   else '0';
  dec_is_cmd   <= '1' when (dec_6b(5 downto 1) = "00000") and (dec_is_k = '0') else '0';

  dec_k2_symbol   <= dec_6b;
  dec_k2_startend <= dec_6b(4);

  dec_r3mask <= dec_6b(5 downto 1);


  ---------------------------------------------------------------------------
  -- synopsys translate_off

  -- lots of debug - not for synth, of course
  dbg_symb0_sync <= '1' when (frame_sync_q = FSYNC_SYMB0) else '0';
  dbg_symb1_sync <= '1' when (frame_sync_q = FSYNC_FRAME) else '0';

  dbg_symb0   <= symbol_in when (dbg_symb0_sync = '1');  --rising_edge(dbg_symb0_sync);
  dbg_symb1   <= symbol_in when (dbg_symb1_sync = '1');  --rising_edge(dbg_symb1_sync);
  dbg_dec_6b0 <= dec_6b    when (dbg_symb0_sync = '1');  --rising_edge(dbg_symb0_sync);
  dbg_dec_6b1 <= dec_6b    when (dbg_symb1_sync = '1');  --rising_edge(dbg_symb1_sync);


  -- cheeky trick to make marker in the sim wafeform display ...
  dbg_symb1_sync_10ps <= dbg_symb1_sync after 10 ps;
  dbg_frame0          <= (dbg_symb0 & dbg_symb1) when falling_edge(dbg_symb1_sync);
  dbg_frame           <= x"0000"                 when falling_edge(dbg_symb1_sync) else
               dbg_frame0 when falling_edge(dbg_symb1_sync_10ps);

  dbg_bcr     <= dbg_dec_6b0(5);
  dbg_l0a_map <= dbg_dec_6b0(4 downto 1);
  dbg_tag     <= dbg_dec_6b0(0) & dbg_dec_6b1 when (frame_sync_q = FSYNC_FRAME);

  -- synopsys translate_on
  -------------------------------------------------------------------------


  -- frame decoder/sequencer
  --==========================================================

  g_hamming : if (C_HAMMING_EN = 1) generate
  begin

    prc_sm_sync_part : process (initb, clk40)
    begin
      if (initb = '0') then
        state_hamm <= hamm_enc_3b(0);   --(sUnlockedOrError);

      elsif rising_edge(clk40) then
        -- abort frame if symbol error, and wait for next frame if needed
        if (dec_symb_err = '1') and (frame_sync_q /= FSYNC_FRAME) then
          state_hamm <= hamm_enc_3b(0);  --(sUnlockedOrError);
        else
          state_hamm <= hamm_enc_3b(t_states'pos(nstate));
        end if;

      end if;
    end process;


    hamm_dec_3b(state_hamm,             --in
                --error_hamm, -- out
                state_hamm_err_o,       --out
                state_uns);             --out

    state <= t_states'val(to_integer(state_uns));

  end generate;


  g_no_hamming : if (C_HAMMING_EN = 0) generate
  begin

    prc_sm_sync_part : process (initb, clk40)
    begin
      if (initb = '0') then
        state <= UnlockedOrError;

      elsif rising_edge(clk40) then
        -- abort frame if symbol error, and wait for next frame if needed
        if (dec_symb_err = '1') and (frame_sync_q /= FSYNC_FRAME) then
          state <= UnlockedOrError;
        else
          state <= nstate;
        end if;

      end if;
    end process;
  end generate;



-----------------------------------------

  prc_sm_async : process (dec_bcr, dec_err, dec_is_cmd, dec_is_k,
                          dec_k, dec_k2_startend, dec_k2_symbol, dec_l0a_map,
                          dec_tag_lsbs, frame_sync_q, locked_i, state, tag_msb)
  begin
    -- defaults
    bcr0            <= '0';
    l0a_map_load    <= '0';
    tag_msb_load    <= '0';
    tag_load_l0a_go <= '0';
    --l1r3_tag_load    <= '0';
    cbus_soc        <= '0';
    cbus_eoc        <= '0';
    cbus_data       <= '0' & dec_k2_symbol;
    cbus_valid      <= '0';
    k3_go           <= '0';
    sigs_disable    <= '0';
    frame_err       <= '0';
    --l0a_flush        <= '0';


    case state is

      when UnlockedOrError =>
        nstate       <= UnlockedOrError;
        sigs_disable <= '1';
        -- Wait for locked and correct position in frame cycle
        if (locked_i = '1') and (frame_sync_q = FSYNC_FRAME) then
          nstate <= Nibble0;
        end if;


      when Nibble0 =>
        nstate <= Symbol0;


      -- first symbol processing
      -----------------------------------------------------
      when Symbol0 =>                   -- aka FSYNC_SYMB0
   
        tag_msb_load <= '1';

        if (dec_bcr = '1') then
          bcr0 <= '1';
        end if;

        --if (dec_l0a_map /= "0000") then
        --  l0a_map_load <= '1';
        --elsif (dec_bcr = '1') then
        --  l0a_map_load <= '1'; -- load zeros if only BCR
        --end if;

        if (dec_l0a_map /= "0000") or (dec_bcr = '1') then
          l0a_map_load <= '1';
        end if;


        if (dec_k(0) = '1') then        -- Idle
          nstate <= WaitK1;

        elsif (dec_k(1) = '1') then     -- ERROR! Should not have K1 here!
          frame_err <= '1';
          nstate    <= UnlockedOrError;

        elsif (dec_k(2) = '1') then     -- CMD header
          nstate <= K2WaitSymbol1;      --WaitCmdStart;

        elsif (dec_k(3) = '1') then
          nstate <= K3WaitSymbol1;

        elsif (dec_is_cmd = '1') then
          nstate <= WaitCmd;

        --elsif (dec_l0a_map /= "0000") then -- redundant 
        --  tag_msb_load <= '1';
        --  l0a_map_load <= '1';
        --  nstate       <= WaitTag;

        else                   
          nstate <= WaitTag;

        end if;



-- second symbol/frame processing
-------------------------------------------------------
      when WaitK1 =>
        nstate <= WaitK1;
        if (frame_sync_q = FSYNC_FRAME) then  --and (dec_err = '0') Doh! don't need this here!!
          if (dec_k(1) = '1') then
            nstate <= Nibble0;
          else
            frame_err <= '1';
            nstate    <= UnlockedOrError;
          end if;
        end if;


      when WaitTag =>
        nstate <= WaitTag;

        if (frame_sync_q = FSYNC_FRAME) then
          if (dec_is_k = '0') then
            if (dec_err = '0') then
              tag_load_l0a_go <= '1';
            --l1r3_tag_load <= '1';
            end if;
            nstate <= Nibble0;
          else
            frame_err <= '1';
            nstate    <= UnlockedOrError;
          end if;

        end if;


        -- Would like to do this, but means 9 states, bleuughhh!
        --when WaitTagBCRONly =>
        --  nstate <= WaitTagBCROnly;
        --  if (frame_sync_q = FSYNC_FRAME) and (dec_err = '0') then
        --    nstate          <= Nibble0;
        --  end if;


      when WaitCmd =>
        nstate <= WaitCmd;
        if (frame_sync_q = FSYNC_FRAME) then
          if (dec_is_k = '0') then
            if (dec_err = '0') then
              --l1r3_tag_load <= '1';         -- for L1R3 dual use of block
              cbus_data  <= tag_msb & dec_tag_lsbs;
              cbus_valid <= '1';
            end if;
            nstate <= Nibble0;
          else
            frame_err <= '1';
            nstate    <= UnlockedOrError;
          end if;
        end if;


      when K2WaitSymbol1 =>             --WaitCmdStart =>
        nstate <= K2WaitSymbol1;
        if (frame_sync_q = FSYNC_FRAME) then
          if (dec_is_k = '0') then
            if (dec_err = '0') then
              if (dec_k2_startend = '1') then
                cbus_soc <= '1';
              end if;

              if (dec_k2_startend = '0') then
                cbus_eoc <= '1';
              end if;

              cbus_data  <= '0' & dec_k2_symbol;
              cbus_valid <= '1';
            end if;
            nstate <= Nibble0;
          else
            frame_err <= '1';
            nstate    <= UnlockedOrError;
          end if;

        end if;


      when K3WaitSymbol1 =>
        nstate <= K3WaitSymbol1;
        if (frame_sync_q = FSYNC_FRAME) then
          if (dec_is_k = '0') then
            if (dec_err = '0') then
              k3_go <= '1';
            end if;
            nstate <= Nibble0;
          else
            frame_err <= '1';
            nstate    <= UnlockedOrError;
          end if;
        end if;


      when InvalidState =>  -- 8th state
        nstate <= UnlockedOrError; -- if state somehow ends up here, then we know where to go

        
      when others =>  -- should never happen, but lets make it 1000% clear
        nstate <= UnlockedOrError;

        
    end case;

  end process;


-- L0A "processing"
----------------------------------------------------

  g_tmr_l0a : if (C_TMR_EN = 1) generate
  begin

    Utmr_reg_l0a_map : lcb_tmr_reg
      generic map (
        WIDTH   => 4,
        RST_VAL => 0
        )
      port map (
        initb => initb,
        rstb  => '1',                   --rstb,
        clk   => clk40,
        ena   => l0a_map_load,
        d     => dec_l0a_map,
        q     => l0a_map,
        err   => l0a_map_err
        );

    Utmr_reg_l0a_sr : lcb_tmr_reg
      generic map (
        WIDTH   => 4,
        RST_VAL => 0
        )
      port map (
        initb => initb,
        rstb  => '1',                   --rstb,
        clk   => clk40,
        ena   => '1',
        d     => l0a_sr_d,
        q     => l0a_sr,
        err   => l0a_sr_err
        );


    l0a_sr_d <= l0a_map when (tag_load_l0a_go = '1') else
                (l0a_sr(2 downto 0) & '0');

  end generate;


  g_no_tmr_l0a : if (C_TMR_EN = 0) generate
  begin

    prc_l0a_sr : process (initb, clk40)
    begin
      if (initb = '0') then
        l0a_sr  <= (others => '0');
        l0a_map <= (others => '0');

      elsif rising_edge(clk40) then
        --if (rstb = '0') then
        --  l0a_sr  <= (others => '0');
        --  l0a_map <= (others => '0');
        --else
        if (l0a_map_load = '1') then
          l0a_map <= dec_l0a_map;
        end if;

        -- l0a_shift ---------------------------------
        if (tag_load_l0a_go = '1') then
          l0a_sr <= l0a_map;
        else
          l0a_sr <= l0a_sr(2 downto 0) & '0';
        end if;

      --end if;
      end if;
    end process;
  end generate;




  prc_l0a_tag : process (initb, clk40)
  begin

    if (initb = '0') then
      tag_msb     <= '0';
      tag_counter <= (others => '0');

    elsif rising_edge(clk40) then

      if (tag_msb_load = '1') then
        tag_msb <= dec_tag_msb;
      end if;

      -- tag counter ----------------------------------
      if (tag_load_l0a_go = '1') then
        tag_counter <= tag_msb & dec_tag_lsbs;

      else
        if (l0a_sr(3) = '1') then       -- adjust tap as needed
          if (tag_counter = "1111111") then
            tag_counter <= "0000000";
          else
            tag_counter <= tag_counter + 1;
          end if;

        end if;
      end if;

    end if;
  end process;


  l0a_o     <= l0a_sr(3);
  l0a_tag_o <= std_logic_vector(tag_counter);
--l0a_tag_valid_o <= l0a_delayed(1);    -- adjust as per above


-- BCR "processing"
----------------------------------------------------

  prc_bcr : process (initb, clk40)
  begin

    if (initb = '0') then
      bcr_o <= '0';
      bcr_q <= (others => '0');

    elsif rising_edge(clk40) then
      --if (sigs_disable = '1') then -- not needed because BCR has nothing to do with second frame
      bcr_q <= bcr_q(4 downto 0) & bcr0;  -- delay to put BCR in known phase wrt frame L0As
      bcr_o <= bcr_q(4);  -- but a 5 clock delay seems excessive ??

    end if;
  end process;


-- K3 (fast_command) "processing"
----------------------------------------------------

  fs_k3 <= unsigned(frame_sync_q) & k3_bc;  -- added FA


  g_tmr_fcmd : if (C_TMR_EN = 1) generate
  begin

    Utmr_reg_fcmd : lcb_tmr_reg
      generic map (
        WIDTH   => 4,
        RST_VAL => 0
        )
      port map (
        initb => initb,
        rstb  => '1',
        clk   => clk40,
        ena   => k3_go,
        d     => dec_6b(3 downto 0),
        q     => k3_data,
        err   => k3_data_err
        );


    prc_k3 : process (initb, clk40)
    begin

      if (initb = '0') then
        k3_bc          <= "00";
        --k3_data      <= "0000";
        fast_cmd_o     <= "0000";
        k3_in_progress <= '0';
        fcv_a          <= '0';
        fcv_b          <= '0';
        fcv_c          <= '0';

      elsif rising_edge(clk40) then
        -- default
        fcv_a <= '0'; fcv_b <= '0'; fcv_c <= '0';

        if (k3_go = '1') then
          k3_bc          <= dec_6b(5 downto 4);
          --k3_data        <= dec_6b(3 downto 0);
          k3_in_progress <= '1';

          fast_cmd_o <= std_logic_vector(dec_6b(3 downto 0));

          -- doing first one here to save a clock
          if (dec_6b(5 downto 4) = "00") then  --frame_sync = "11"
            fcv_a <= '1'; fcv_b <= '1'; fcv_c <= '1';
          end if;

        elsif (k3_in_progress = '1') then
          -- need to align fast command using this messy solution (got a better
          -- one?)

          case fs_k3 is                 -- Was (frame_sync_q & k3_bc)
            when "0001" => fcv_a <= '1'; fcv_b <= '1'; fcv_c <= '1';
            when "0110" => fcv_a <= '1'; fcv_b <= '1'; fcv_c <= '1';
            when "1011" => fcv_a <= '1'; fcv_b <= '1'; fcv_c <= '1';

            when others => null;
          end case;

          if (frame_sync_q = "10") then
            k3_in_progress <= '0';
          end if;

        --end if;
        end if;
      end if;
    end process;

    -- Is this really TMR? And is it even helpful??
    fast_cmd_valid_o <= (fcv_a and fcv_b) or
                        (fcv_a and fcv_c) or
                        (fcv_b and fcv_c);

    fast_cmd_valid_err <= (fcv_a xor fcv_b) or
                          (fcv_a xor fcv_c) or
                          (fcv_b xor fcv_c);
    
  end generate;



  g_no_tmr_fcmd : if (C_TMR_EN = 0) generate
  begin

    prc_k3 : process (initb, clk40)
    begin

      if (initb = '0') then
        k3_bc            <= "00";
        k3_data          <= "0000";
        fast_cmd_o       <= "0000";
        k3_in_progress   <= '0';
        fast_cmd_valid_o <= '0';

      elsif rising_edge(clk40) then


        -- default
        fast_cmd_valid_o <= '0';

        if (k3_go = '1') then
          k3_bc          <= dec_6b(5 downto 4);
          k3_data        <= dec_6b(3 downto 0);
          k3_in_progress <= '1';

          fast_cmd_o <= std_logic_vector(dec_6b(3 downto 0));

          -- doing first one here to save a clock
          if (dec_6b(5 downto 4) = "00") then  --frame_sync = "11"
            fast_cmd_valid_o <= '1';
          end if;

        elsif (k3_in_progress = '1') then
          -- need to align fast command using this messy solution (got a better
          -- one?)

          case fs_k3 is                 -- Was (frame_sync_q & k3_bc)
            when "0001" => fast_cmd_valid_o <= '1';
            when "0110" => fast_cmd_valid_o <= '1';
            when "1011" => fast_cmd_valid_o <= '1';
            when others => null;
          end case;

          if (frame_sync_q = "10") then
            k3_in_progress <= '0';
          end if;

        --end if;
        end if;
      end if;
    end process;
  end generate;


---- R3L1 processing
------------------------------------------------------
--prc_r3l1 : process (initb, clk40)
--begin
--  if (initb = '0') then
--    r3_mask  <= "00000";
--    l1r3_tag <= "0000000";
--  elsif rising_edge(clk40) then
--    if (sigs_disable = '1') then
--      r3_mask    <= "00000";
--      l1r3_tag   <= "0000000";
--      l1_valid   <= '0';
--      r3_valid   <= '0';
--      l1r3_valid <= '0';
--    else
--      if (tag_msb_load = '1') then  -- same as symbol0 decoded store
--        r3_mask     <= dec_r3mask;
--        l1r3_tag(6) <= dec_tag_msb;
--      end if;
--      -- default
--      l1_valid   <= '0';
--      r3_valid   <= '0';
--      l1r3_valid <= '0';
--      if (l1r3_tag_load = '1') then
--        l1r3_tag(5 downto 0) <= dec_tag_lsbs;
--        l1r3_valid           <= '1';
--        if (r3_mask = "00000") then
--          l1_valid <= '1';
--        else
--          case hccid_i(3 downto 1) is
--            when "001"  => r3_valid   <= r3_mask(0);
--            when "010"  => r3_valid   <= r3_mask(1);
--            when "011"  => r3_valid   <= r3_mask(2);
--            when "100"  => r3_valid   <= r3_mask(3);
--            when "101"  => r3_valid   <= r3_mask(4);
--            when others => l1r3_valid <= '0';
--          end case;
--        end if;
--      end if;
--    end if;
--  end if;
--end process;

--l1_valid_o   <= l1_valid;
--r3_valid_o   <= r3_valid;
--l1r3_valid_o <= l1r3_valid;
--r3_mask_o    <= std_logic_vector(r3_mask);
--l1r3_tag_o   <= std_logic_vector(l1r3_tag);



-- CMD "processing"
----------------------------------------------------


  
  prc_cmd : process (clk40, initb)
  begin
    if (initb = '0') then
      cbus_soc_q   <= '0';
      cbus_eoc_q   <= '0';
      cbus_data_q  <= "0000000";
      cbus_valid_q <= '0';

    elsif rising_edge(clk40) then
      --if (rstb = '0') then
      --  cbus_soc_q   <= '0';
      --  cbus_eoc_q   <= '0';
      --  cbus_data_q  <= "0000000";
      --  cbus_valid_q <= '0';
      --else

      if (sigs_disable = '1') then
        cbus_soc_q   <= (decoder_err and locked_i);
        cbus_eoc_q   <= (decoder_err and locked_i);
        cbus_data_q  <= "0000000";
        cbus_valid_q <= (decoder_err and locked_i);

      else
        cbus_soc_q   <= cbus_soc or (decoder_err and locked_i);
        cbus_eoc_q   <= cbus_eoc or (decoder_err and locked_i);
        cbus_data_q  <= cbus_data;
        cbus_valid_q <= cbus_valid or (decoder_err and locked_i);

      end if;
    --end if;
    end if;
  end process;


  cbus_soc_o   <= cbus_soc_q;           -- in sync with first cmd7 word
  cbus_eoc_o   <= cbus_eoc_q;           -- in sync with last cmd7 word
  cbus_data_o  <= std_logic_vector(cbus_data_q);
  cbus_valid_o <= cbus_valid_q;

  cbus_all_o <= cbus_soc_q & cbus_eoc_q & std_logic_vector(cbus_data_q);


  -- synopsys translate_off
  prc_dbg_dec_cmdfrm : process(clk40)
  begin
    if rising_edge(clk40) then
      if (cbus_valid_q = '1') then
        dbg_dec_cmdfrm <= dbg_dec_cmdfrm(41 downto 0) & cbus_data_q;

      end if;
    end if;
  end process;
  -- synopsys translate_on

end rtl;
