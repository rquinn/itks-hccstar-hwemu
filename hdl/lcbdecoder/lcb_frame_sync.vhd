--
-- ITk Strips LCB
-- Frame Sync
--
-- Matt Warren Aug 2016
--
-- Last update Nov 2017
--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library hccemu_lcbdec;
use hccemu_lcbdec.lcb_pkg_globals.all;


entity lcb_frame_sync is
  port(

    -- 40MHz domain
    --------------------------------------------------------
    par4_o           : out std_logic_vector (3 downto 0);
    frame_sync_o     : out std_logic_vector (1 downto 0);
    dbg_frame_o      : out std_logic_vector (15 downto 0);
    dbg_symbol_o     : out std_logic_vector (7 downto 0);
    nibble_err_o     : out std_logic;
    --parity_err_o     : out std_logic;
    locked_o         : out std_logic;
    frame_raw_o      : out std_logic_vector (15 downto 0);  -- for HPR
    decoder_err_i    : in  std_logic;
    errcount_ovfl_o  : out std_logic;
    errcount_o       : out std_logic_vector (15 downto 0);
    errcount_thr_i   : in  std_logic_vector (15 downto 0);
    errcount_clr_i   : in  std_logic;
    decode_err_o     : out std_logic;
    state_hamm_err_o : out std_logic;
    lock_hamm_err_o  : out std_logic;
    clk40            : in  std_logic;
    initb            : in  std_logic;
    par4_i           : in  std_logic_vector (3 downto 0)
    );

-- Declarations

end entity lcb_frame_sync;

---------------------------------------------------------------------------
architecture rtl of lcb_frame_sync is

  constant LOCK_COUNT_BITS : natural := 4;
  constant LOCK_COUNT_MAX  : natural := 2**LOCK_COUNT_BITS - 1;
  constant LOCK_COUNT_MID  : natural := (2**LOCK_COUNT_BITS)/2;

  signal lock_count      : natural range 0 to LOCK_COUNT_MAX;
  signal lock_count_uns  : unsigned(LOCK_COUNT_BITS-1 downto 0);

  signal lock_count_hamm : unsigned(7 downto 0);

  --constant LOCK_COUNT_BIT_MAX : natural := 3;
  --constant LOCK_COUNT_MAX  : unsigned(LOCK_COUNT_BIT_MAX downto 0) := (others => '1');
  --constant LOCK_COUNT_ZERO : unsigned(LOCK_COUNT_BIT_MAX downto 0) := (others => '0');
  --signal lock_count : unsigned(LOCK_COUNT_BIT_MAX downto 0);

  signal lock_count_inc : std_logic;
  signal lock_count_dec : std_logic;
  signal lock_count_clr : std_logic;


  signal errcount       : unsigned(15 downto 0);
  signal errcount_inc   : std_logic;
  --signal errcount_clr : std_logic;
  constant ERRCOUNT_MAX : natural := 16#ffff#;


  signal sr160  : unsigned(3 downto 0);
  signal sr440  : unsigned(15 downto 0);
  signal frame  : unsigned(15 downto 0);
  signal nibble : unsigned(3 downto 0);
  signal symbol : unsigned(7 downto 0);

  signal nibble_err : std_logic;
--  signal parity_err : std_logic;

  signal locked      : std_logic;
  signal symbol_sync : std_logic;

  -- Special case - stays as slv for now ...
  signal frame_sync    : std_logic_vector(1 downto 0);
  signal frame_sync_q0 : std_logic_vector(1 downto 0);


  type t_states is (
    --Reset,
    WaitIdleFrame,  -- 1
    PreLockSymb0, PreLockNib2, PreLockSymb1, --4
    LockedNib0, LockedSymb0, LockedNib2, LockedSymb1 --8
    );

  signal state, nstate : t_states;
  signal state_uns     : unsigned(2 downto 0);

  signal state_hamm : unsigned(6 downto 0);
  --signal error_hamm : unsigned(1 downto 0);
  signal coded_hamm : unsigned(7 downto 0);
  
begin
-------------------------------------------------------------------------------

-- 160Mb Deserialiser
-- ===========================================================


  prc_rst : process (clk40)
  begin

    if (initb = '0') then
      --sr160 <= "1010";
      sr160 <= (others => '0');

    elsif rising_edge(clk40) then
      sr160 <= unsigned(par4_i);

    end if;
  end process;


  prc_sr40 : process (clk40)
  begin
    if (initb = '0') then
      --sr440 <= x"aaaa";
      sr440 <= (others => '0');

    elsif rising_edge(clk40) then
      sr440 <= sr440(11 downto 0) & sr160(3 downto 0);

    end if;
  end process;


  frame       <= sr440(15 downto 0);
  frame_raw_o <= std_logic_vector(frame) when rising_edge(clk40);
  nibble      <= frame(3 downto 0);
  symbol      <= frame(7 downto 0);


-- Very basic integrity checking
-- Async for now - trying to keep things fast


  nibble_err <= '1' when (nibble = "0000") or (nibble = "1111") else '0';
  --parity_err <= symbol(7) xor
  --              symbol(6) xor
  --              symbol(5) xor
  --              symbol(4) xor
  --              symbol(3) xor
  --              symbol(2) xor
  --              symbol(1) xor
  --              symbol(0);


-- State Machine
--==========================================================
  g_hamming : if (C_HAMMING_EN = 1) generate
  begin

    prc_sm_sync_part : process (initb, clk40)
    begin
      if (initb = '0') then
        state_hamm <= hamm_enc_3b(0);  --WaitIdleFrame --Reset;

      elsif rising_edge(clk40) then
        state_hamm <= hamm_enc_3b(t_states'pos(nstate));
      end if;

    end process;


    hamm_dec_3b(
      state_hamm,                       --in
      state_hamm_err_o,                 --out
      state_uns                         --out
      );
    state <= t_states'val(to_integer(state_uns));

  end generate;

  g_no_hamming : if (C_HAMMING_EN = 0) generate
  begin

    prc_sm_sync_part : process (initb, clk40)
    begin
      if (initb = '0') then
        state <= WaitIdleFrame;         --Reset;

      elsif rising_edge(clk40) then
        state <= nstate;
      end if;

    end process;

  end generate;


  prc_sm_decode : process (frame, lock_count, state)
  begin

    -- defaults
    lock_count_clr <= '0';
    lock_count_inc <= '0';
    locked         <= '0';
    frame_sync_q0  <= "00";

    case state is

      ---------------------------------------------------
--      when Reset =>
--        lock_count_clr <= '1';
--        nstate         <= WaitIdleFrame;

      ---------------------------------------------------
      when WaitIdleFrame =>
        nstate <= WaitIdleFrame;

        if (frame = IDLE_FRAME) then
          lock_count_inc <= '1';
        end if;

        if (lock_count = LOCK_COUNT_MAX) then
          nstate <= PreLockSymb0;  --LockedSymb0;  --LockedNib0
        -- Note offset - accounts for 1 clk40 delay to detect IDLE
        end if;

        --------------------------------------------------------------
        -- Align asserting of "locked" to frame boundary

      when PreLockSymb0 =>
        nstate <= PreLockNib2;


      when PreLockNib2 =>
        nstate <= PreLockSymb1;


      when PreLockSymb1 =>
        locked        <= '1';           -- Early because of clocking delay at output
        frame_sync_q0 <= "00";
        nstate        <= LockedNib0;


      -----------------------------------------------------------
      when LockedNib0 =>
        locked        <= '1';
        frame_sync_q0 <= "01";
        nstate        <= LockedSymb0;



      when LockedSymb0 =>
        locked        <= '1';
        frame_sync_q0 <= "10";
        nstate        <= LockedNib2;


      when LockedNib2 =>
        locked        <= '1';
        frame_sync_q0 <= "11";
        nstate        <= LockedSymb1;


      when LockedSymb1 =>
        locked        <= '1';
        frame_sync_q0 <= "00";
        lock_count_inc <= '1';       -- Do this here so can be overridden by decoder_err_i
        nstate <= LockedNib0;
        if (lock_count = 0) then
          nstate <= WaitIdleFrame;
        end if;

        
     when others =>  -- should never happen, but being 1000% safe
          nstate <= WaitIdleFrame;

    ---------------------------------------------------
    end case;

  end process;


  frame_sync <= frame_sync_q0 when rising_edge(clk40);

  symbol_sync <= '1' when (frame_sync = FSYNC_FRAME) or (frame_sync = FSYNC_SYMB0) else '0';


----------------------------------------------------------------------------------



  lock_count_dec <= decoder_err_i when (locked = '1') else nibble_err; -- or (parity_err and symbol_sync);
    


  g_hamm_lock_count : if (C_HAMMING_EN = 1) generate
  begin
    prc_lock_count : process (initb, clk40)
    begin
      if (initb = '0') then
        lock_count_hamm <= hamm_enc_4b(0);

      elsif rising_edge(clk40) then
        if (lock_count_clr = '1') then
          lock_count_hamm <= hamm_enc_4b(0);

        else
          if(lock_count_dec = '1') and (lock_count /= 0) then
            lock_count_hamm <= hamm_enc_4b(lock_count - 1);

          elsif (lock_count_inc = '1') and (lock_count /= LOCK_COUNT_MAX) then
            lock_count_hamm <= hamm_enc_4b(lock_count + 1);

          end if;

        end if;
      end if;
    end process;

    hamm_dec_4b(lock_count_hamm,        --in
                LOCK_COUNT_MID,         --in
                lock_hamm_err_o,        --out
                lock_count_uns          --out
                );
    
    lock_count <= to_integer(lock_count_uns);

  end generate;




  g_no_hamm_lock_count : if (C_HAMMING_EN = 0) generate
  begin
    prc_lock_count : process (initb, clk40)
    begin
      if (initb = '0') then
        lock_count <= 0;

      elsif rising_edge(clk40) then
        if (lock_count_clr = '1') then
          lock_count <= 0;

        else
          if(lock_count_dec = '1') and (lock_count /= 0) then
            lock_count <= lock_count - 1;

          elsif (lock_count_inc = '1') and (lock_count /= LOCK_COUNT_MAX) then
            lock_count <= lock_count + 1;

          end if;

        end if;
      end if;
    end process;
  end generate;


  --prc_lock_count : process (initb, clk40)
  --begin
  --  if (initb = '0') then
  --    lock_count <= (others => '0');

  --  elsif rising_edge(clk40) then
  --    if (lock_count_clr = '1') then
  --      lock_count <= (others => '0');

  --    else
  --      if(lock_count_dec = '1') and (lock_count /= LOCK_COUNT_ZERO) then
  --        lock_count <= lock_count - 1;

  --      elsif (lock_count_inc = '1') and (lock_count /= LOCK_COUNT_MAX) then
  --        lock_count <= lock_count + 1;

  --      end if;

  --    end if;
  --  end if;
  --end process;


-------------------------------------------------------------------------------



  -- duplicate transfer from 160 domain here to keep latency down
  par4_o <= std_logic_vector(sr160(3 downto 0)) when rising_edge(clk40);

  nibble_err_o <= nibble_err when rising_edge(clk40);
  --parity_err_o <= parity_err when rising_edge(clk40);
  locked_o     <= locked     when rising_edge(clk40);  -- Clock because it's better and makes no difference
  frame_sync_o <= frame_sync;                          -- Already clocked;

  dbg_frame_o  <= std_logic_vector(frame)  when (frame_sync = FSYNC_FRAME);  -- yes, yes, a latch, I know,
  dbg_symbol_o <= std_logic_vector(symbol) when (symbol_sync = '1');         -- it's just for debug!





-------------------------------------------------------------------------------
-- Error Counter

  errcount_inc <= lock_count_dec and locked;
  

  prc_errcount : process (initb, clk40)
  begin
    if (initb = '0') then
      errcount        <= (others => '0');
      errcount_ovfl_o <= '0';

    elsif rising_edge(clk40) then
      if (errcount_clr_i = '1') then
        errcount <= (others => '0');

      elsif (errcount_inc = '1') and (errcount /= ERRCOUNT_MAX) then
        errcount <= errcount + 1;

      end if;

      errcount_ovfl_o <= '0';
      if (errcount > unsigned(errcount_thr_i)) then
        errcount_ovfl_o <= '1';
      end if;
    end if;

  end process;

  errcount_o   <= std_logic_vector(errcount);
  decode_err_o <= errcount_inc;

-------------------------------------------------------------------------------



end rtl;
