--
-- LCB Global Definitions Package
-- 
-- Matt Warren Aug 2016
--
-- Last update Nov 2017
--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;


package lcb_pkg_globals is

  constant C_HAMMING_EN : natural := 1;  --1=Enable Hamming protection where used, 0=Not
  constant C_TMR_EN     : natural := 1;  --1=Enable TMR where used, 0=Not
  constant C_C40_5MR_EN : natural := 1;  --1=Enable Using 5-way MR for clk40 gen instead of TMR  


  --subtype slv4 is unsigned(3 downto 0);
  type slv4_array is array (natural range <>) of unsigned(3 downto 0);

  --subtype slv7 is unsigned(6 downto 0);
  type slv7_array is array (natural range <>) of unsigned(6 downto 0);

  --subtype slv8 is unsigned(7 downto 0);
  type slv8_array is array (natural range <>) of unsigned(7 downto 0);

  --subtype slv32 is unsigned(31 downto 0);
  type slv32_array is array (natural range <>) of unsigned(31 downto 0);

  type uns32_array is array (natural range <>) of unsigned(31 downto 0);


  -- From Paul:

  constant MAP_6B_8B : slv8_array(0 to 63) := (x"59",   --  0  0x00
                                               x"71",   --  1  0x01
                                               x"72",   --  2  0x02
                                               x"c3",   --  3  0x03
                                               x"65",   --  4  0x04
                                               x"c5",   --  5  0x05
                                               x"c6",   --  6  0x06
                                               x"87",   --  7  0x07
                                               x"69",   --  8  0x08
                                               x"c9",   --  9  0x09
                                               x"ca",   -- 10  0x0a
                                               x"8b",   -- 11  0x0b
                                               x"cc",   -- 12  0x0c
                                               x"8d",   -- 13  0x0d
                                               x"8e",   -- 14  0x0e
                                               x"4b",   -- 15  0x0f
                                               x"53",   -- 16  0x10
                                               x"d1",   -- 17  0x11
                                               x"d2",   -- 18  0x12
                                               x"93",   -- 19  0x13
                                               x"d4",   -- 20  0x14
                                               x"95",   -- 21  0x15
                                               x"96",   -- 22  0x16
                                               x"17",   -- 23  0x17
                                               x"d8",   -- 24  0x18
                                               x"99",   -- 25  0x19
                                               x"9a",   -- 26  0x1a
                                               x"1b",   -- 27  0x1b
                                               x"9c",   -- 28  0x1c
                                               x"1d",   -- 29  0x1d
                                               x"1e",   -- 30  0x1e
                                               x"5c",   -- 31  0x1f
                                               x"63",   -- 32  0x20
                                               x"e1",   -- 33  0x21
                                               x"e2",   -- 34  0x22
                                               x"a3",   -- 35  0x23
                                               x"e4",   -- 36  0x24
                                               x"a5",   -- 37  0x25
                                               x"a6",   -- 38  0x26
                                               x"27",   -- 39  0x27
                                               x"e8",   -- 40  0x28
                                               x"a9",   -- 41  0x29
                                               x"aa",   -- 42  0x2a
                                               x"2b",   -- 43  0x2b
                                               x"ac",   -- 44  0x2c
                                               x"2d",   -- 45  0x2d
                                               x"2e",   -- 46  0x2e
                                               x"6c",   -- 47  0x2f
                                               x"74",   -- 48  0x30
                                               x"b1",   -- 49  0x31
                                               x"b2",   -- 50  0x32
                                               x"33",   -- 51  0x33
                                               x"b4",   -- 52  0x34
                                               x"35",   -- 53  0x35
                                               x"36",   -- 54  0x36
                                               x"56",   -- 55  0x37
                                               x"b8",   -- 56  0x38
                                               x"39",   -- 57  0x39
                                               x"3a",   -- 58  0x3a
                                               x"5a",   -- 59  0x3b
                                               x"3c",   -- 60  0x3c
                                               x"4d",   -- 61  0x3d
                                               x"4e",   -- 62  0x3e
                                               x"66");  -- 63  0x3f


  constant MAP_K_8B : slv8_array(0 to 3) := (x"78",   -- K0
                                             x"55",   -- K1
                                             x"47",   -- K2
                                             x"6A");  -- K3

  --    K0       K1
  -- 76543210 76543210

  -- IDLE is K0, K1 , sent msb first to get it in nice order on the scope ;-)
  constant IDLE_FRAME : unsigned(15 downto 0) := (MAP_K_8B(0) & MAP_K_8B(1));

  -- K2 + X & 0=end & "1110" [btw K2 = 0x78]
  constant SYMB_CMD_NOP : unsigned(7 downto 0) := MAP_6B_8B(2#001110#);  --0x87

  --constant SYMB_CMD_END : unsigned(7 downto 0) := MAP_6B_8B(2#001110#);  --0x87

  -- speculative ... 
  constant SYMB_ECR    : unsigned(7 downto 0) := MAP_6B_8B(2#000010#);  -- 0x72
  constant SYMB_ABCRST : unsigned(7 downto 0) := MAP_6B_8B(2#000100#);  -- 0x65 
  constant SYMB_HCCRST : unsigned(7 downto 0) := MAP_6B_8B(2#001000#);  -- 0x69

  constant FSYNC_NIB0  : std_logic_vector(1 downto 0) := "00";
  constant FSYNC_SYMB0 : std_logic_vector(1 downto 0) := "01";
  constant FSYNC_NIB2  : std_logic_vector(1 downto 0) := "10";
  constant FSYNC_FRAME : std_logic_vector(1 downto 0) := "11";


  constant HCC : natural := 16#cc#;
  constant ABC : natural := 16#ab#;


  procedure dec_8b_6b(
    signal symbol_in : in  unsigned(7 downto 0);
    signal dec_6b    : out unsigned(5 downto 0);
    signal dec_k     : out unsigned(3 downto 0);
    signal dec_err   : out std_logic
    );


  -- Hamming stuff, produced using this program: https://opencores.org/project,hamming_gen
  -- Modified for our needs
  -- NOTE: The first state must be the Error/Reset state
  -----------------------------------------------------------------------  

  -- 2b is not good when optimised - using TMR instead
  --function hamm_enc_2b(data_val_in : natural) return unsigned;

  --procedure hamm_dec_2b(
  --  data_parity_in     :     unsigned(5 downto 0);
  --  --signal error_out   : out unsigned(1 downto 0);
  --  signal err_out     : out std_logic;
  --  signal decoded_out : out natural range 0 to 3
  --  );
  -----------------------------------------------------------------------  

  function hamm_enc_3b(data_val_in : natural) return unsigned;

  procedure hamm_dec_3b(
    data_parity_in  :     unsigned(6 downto 0);
    signal err_out  : out std_logic;
    signal data_out : out unsigned(2 downto 0)
    );
  -----------------------------------------------------------------------  

  function hamm_enc_4b(data_val_in : natural) return unsigned;

  procedure hamm_dec_4b(
    data_parity_in  :     unsigned(7 downto 0);
    error_value_in  :     natural;
    signal err_out  : out std_logic;
    signal data_out : out unsigned(3 downto 0)
    );
  -----------------------------------------------------------------------  

  function hamm_enc_5b(data_val_in : natural) return unsigned;

  procedure hamm_dec_5b(
    data_parity_in  :     unsigned(9 downto 0);
    error_value_in  :     natural;
    signal err_out  : out std_logic;
    signal data_out : out unsigned(4 downto 0)
    );
  -----------------------------------------------------------------------  



  -- function hamm_enc_32b(data_in : unsigned(31 downto 0)) return unsigned;

  -- procedure hamm_dec_32b(
  --   data_parity_in     :     unsigned(38 downto 0);
  --   signal error_out   : out unsigned(1 downto 0);
  --   signal decoded_out : out unsigned(31 downto 0)
  --   );
  -----------------------------------------------------------------------  


end lcb_pkg_globals;


--=========================================================================================
--
-- B O D Y
--
--=========================================================================================

package body lcb_pkg_globals is


  procedure dec_8b_6b(
    signal symbol_in : in  unsigned(7 downto 0);
    signal dec_6b    : out unsigned(5 downto 0);
    signal dec_k     : out unsigned(3 downto 0);
    signal dec_err   : out std_logic
    ) is
  begin

    --defaults
    dec_6b  <= "000000";
    dec_k   <= "0000";
    dec_err <= '1';

    for n in 0 to 63 loop
      if (symbol_in = MAP_6B_8B(n)) then
        dec_6b  <= to_unsigned(n, 6);
        dec_err <= '0';
      end if;
    end loop;

    for n in 0 to 3 loop
      if (symbol_in = MAP_K_8B(n)) then
        dec_k(n) <= '1';
        dec_err  <= '0';
      end if;
    end loop;

  end procedure;


---------------------------------------------------------------------------
-- HAMMING ENCODERS --
---------------------------------------------------------------------------

  -- 2b is not good when optimised - using TMR instead
  --function hamm_enc_2b(data_val_in : natural) return unsigned is
  --  variable data_in   : unsigned(1 downto 0);
  --  variable parity    : unsigned(3 downto 0);
  --  variable coded_out : unsigned(5 downto 0);
  --begin
  --  data_in   := to_unsigned(data_val_in, 2);
  --  parity(3) := data_in(1);
  --  parity(2) := data_in(0);
  --  parity(1) := data_in(0) xor data_in(1);
  --  parity(0) := data_in(0) xor data_in(1) xor parity(1) xor parity(2) xor parity(3);
  --  coded_out := (data_in & parity);
  --  return coded_out;
  --end function;

  -------------------------------------------------------------
  function hamm_enc_3b(data_val_in : natural) return unsigned is
    variable data_in   : unsigned(2 downto 0);
    variable parity    : unsigned(3 downto 0);
    variable coded_out : unsigned(6 downto 0);
  begin
    data_in   := to_unsigned(data_val_in, 3);
    parity(3) := data_in(1) xor data_in(2);
    parity(2) := data_in(0) xor data_in(2);
    parity(1) := data_in(0) xor data_in(1);
    parity(0) := data_in(0) xor data_in(1) xor data_in(2) xor parity(1) xor parity(2) xor parity(3);
    coded_out := (data_in & parity);
    return coded_out;
  end function;

  --------------------------------------------------------------
  function hamm_enc_4b(data_val_in : natural) return unsigned is
    variable data_in   : unsigned(3 downto 0);
    variable parity    : unsigned(3 downto 0);
    variable coded_out : unsigned(7 downto 0);
  begin
    data_in   := to_unsigned(data_val_in, 4);
    parity(3) := data_in(1) xor data_in(2) xor data_in(3);
    parity(2) := data_in(0) xor data_in(2) xor data_in(3);
    parity(1) := data_in(0) xor data_in(1) xor data_in(3);
    parity(0) := data_in(0) xor data_in(1) xor data_in(2) xor data_in(3) xor parity(1) xor parity(2) xor parity(3);
    coded_out := (data_in & parity);
    return coded_out;
  end function;

  --------------------------------------------------------------
  function hamm_enc_5b(data_val_in : natural) return unsigned is
    variable data_in   : unsigned(4 downto 0);
    variable parity    : unsigned(4 downto 0);
    variable coded_out : unsigned(9 downto 0);
  begin
    data_in   := to_unsigned(data_val_in, 5);
    parity(4) := data_in(4);
    parity(3) := data_in(1) xor data_in(2) xor data_in(3);
    parity(2) := data_in(0) xor data_in(2) xor data_in(3);
    parity(1) := data_in(0) xor data_in(1) xor data_in(3) xor data_in(4);
    parity(0) := data_in(0) xor data_in(1) xor data_in(2) xor data_in(3) xor data_in(4) xor
                 parity(1) xor parity(2) xor parity(3) xor parity(4);
    coded_out := (data_in & parity);
    return coded_out;
  end function;

--------------------------------------------------------------
  -- function hamm_enc_32b(data_in : unsigned(31 downto 0)) return unsigned is
  --   variable parity    : unsigned(6 downto 0);
  --   variable coded_out : unsigned(38 downto 0);
  -- begin
  --   data_in := to_unsigned(data_val_in, 32);

  --   parity(6) := data_in(26) xor data_in(27) xor data_in(28) xor data_in(29) xor data_in(30) xor
  --                data_in(31);

  --   parity(5) := data_in(11) xor data_in(12) xor data_in(13) xor data_in(14) xor data_in(15) xor
  --                data_in(16) xor data_in(17) xor data_in(18) xor data_in(19) xor data_in(20) xor
  --                data_in(21) xor data_in(22) xor data_in(23) xor data_in(24) xor data_in(25);


  --   parity(4) := data_in(4) xor data_in(5) xor data_in(6) xor data_in(7) xor data_in(8) xor
  --                data_in(9) xor data_in(10) xor data_in(18) xor data_in(19) xor data_in(20) xor
  --                data_in(21) xor data_in(22) xor data_in(23) xor data_in(24) xor data_in(25);


  --   parity(3) := data_in(1) xor data_in(2) xor data_in(3) xor data_in(7) xor data_in(8) xor
  --                data_in(9) xor data_in(10) xor data_in(14) xor data_in(15) xor data_in(16) xor
  --                data_in(17) xor data_in(22) xor data_in(23) xor data_in(24) xor data_in(25) xor
  --                data_in(29) xor data_in(30) xor data_in(31);

  --   parity(2) := data_in(0) xor data_in(2) xor data_in(3) xor data_in(5) xor data_in(6) xor
  --                data_in(9) xor data_in(10) xor data_in(12) xor data_in(13) xor data_in(16) xor
  --                data_in(17) xor data_in(20) xor data_in(21) xor data_in(24) xor data_in(25) xor
  --                data_in(27) xor data_in(28) xor data_in(31);

  --   parity(1) := data_in(0) xor data_in(1) xor data_in(3) xor data_in(4) xor data_in(6) xor
  --                data_in(8) xor data_in(10) xor data_in(11) xor data_in(13) xor data_in(15) xor
  --                data_in(17) xor data_in(19) xor data_in(21) xor data_in(23) xor data_in(25) xor
  --                data_in(26) xor data_in(28) xor data_in(30);

  --   parity(0) := data_in(0) xor data_in(1) xor data_in(2) xor data_in(3) xor data_in(4) xor
  --                data_in(5) xor data_in(6) xor data_in(7) xor data_in(8) xor data_in(9) xor
  --                data_in(10) xor data_in(11) xor data_in(12) xor data_in(13) xor data_in(14) xor
  --                data_in(15) xor data_in(16) xor data_in(17) xor data_in(18) xor data_in(19) xor
  --                data_in(20) xor data_in(21) xor data_in(22) xor data_in(23) xor data_in(24) xor
  --                data_in(25) xor data_in(26) xor data_in(27) xor data_in(28) xor data_in(29) xor
  --                data_in(30) xor data_in(31) xor parity(1) xor parity(2) xor parity(3) xor
  --                parity(4) xor parity(5) xor parity(6);


  --   coded_out := (data_in & parity);
  --   return coded_out;
  -- end function;



---------------------------------------------------------------------------
-- HAMMING DECODERS --
---------------------------------------------------------------------------

  -- 2b is not good when optimised - using TMR instead
  --procedure hamm_dec_2b(
  --  data_parity_in     :     unsigned(5 downto 0);
  --  --error_state_in     :     natural;
  --  --signal error_out   : out unsigned(1 downto 0);
  --  signal err_out     : out std_logic;
  --  signal decoded_out : out natural range 0 to 3
  --  ) is

  --  variable coded       : unsigned(5 downto 0);
  --  variable syndrome    : natural range 0 to 5;
  --  variable parity      : unsigned(3 downto 0);
  --  variable parity_in   : unsigned(3 downto 0);
  --  variable syn         : unsigned(3 downto 0);
  --  variable data_in     : unsigned(1 downto 0);
  --  variable decoded_uns : unsigned(1 downto 0);
  --  variable P0, P1      : std_logic;

  --begin

  --  data_in   := data_parity_in(5 downto 4);
  --  parity_in := data_parity_in(3 downto 0);

  --  parity(3) := data_in(1);
  --  parity(2) := data_in(0);
  --  parity(1) := data_in(0) xor data_in(1);
  --  parity(0) := data_in(0) xor data_in(1) xor parity(1) xor parity(2) xor parity(3);

  --  coded(0) := data_parity_in(0);
  --  coded(1) := data_parity_in(1);
  --  coded(2) := data_parity_in(2);
  --  coded(4) := data_parity_in(3);      -- note 4 -> 3
  --  coded(3) := data_parity_in(4);      -- note 3 -> 4
  --  coded(5) := data_parity_in(5);

  --  -- syndrome generation
  --  syn(3 downto 1) := parity(3 downto 1) xor parity_in(3 downto 1);
  --  P0              := '0';
  --  P1              := '0';
  --  for i in 0 to 3 loop
  --    P0 := P0 xor parity(i);
  --    P1 := P1 xor parity_in(i);
  --  end loop;
  --  syn(0) := P0 xor P1;

  --  case syn(3 downto 1) is
  --    when "011"  => syndrome := 3;
  --    when "101"  => syndrome := 5;
  --    when others => syndrome := 0;
  --  end case;

  --  -- defaults
  --  decoded_uns := coded(5) & coded(3);
  --  decoded_out <= to_integer(decoded_uns);
  --  err_out     <= '0';

  --  if syn(0) = '1' then
  --    coded(syndrome) := not(coded(syndrome));
  --    --error_out       <= "01";          -- There is an error
  --    err_out         <= '1';           -- There is an error
  --  elsif syndrome /= 0 then            -- There are more than one error
  --    coded       := (others => '0');   -- FATAL ERROR
  --    decoded_out <= 0;  -- Aka default error state --error_state_in;
  --    --error_out   <= "11";
  --    err_out     <= '1';
  --  --else
  --  --error_out <= "00";                -- No errors detected
  --  end if;

  --end procedure;

  ----------------------------------------------------------------------  

  procedure hamm_dec_3b(
    data_parity_in  :     unsigned(6 downto 0);
    signal err_out  : out std_logic;
    signal data_out : out unsigned(2 downto 0)
    ) is

    variable coded        : unsigned(6 downto 0);
    variable syndrome     : natural range 0 to 6;
    variable syndrome_do  : natural range 0 to 2;
    variable syn_do_valid : std_logic;
    variable parity       : unsigned(3 downto 0);
    variable parity_in    : unsigned(3 downto 0);
    variable syn          : unsigned(3 downto 0);
    variable data_in      : unsigned(2 downto 0);
    variable decoded_uns  : unsigned(2 downto 0);
    variable P0, P1       : std_logic;

  begin

    data_in   := data_parity_in(6 downto 4);
    parity_in := data_parity_in(3 downto 0);

    parity(3) := data_in(1) xor data_in(2);
    parity(2) := data_in(0) xor data_in(2);
    parity(1) := data_in(0) xor data_in(1);
    parity(0) := data_in(0) xor data_in(1) xor data_in(2) xor parity(1) xor parity(2) xor parity(3);

    coded(0) := data_parity_in(0);
    coded(1) := data_parity_in(1);
    coded(2) := data_parity_in(2);
    coded(4) := data_parity_in(3);      -- note 4 -> 3
    coded(3) := data_parity_in(4);      -- note 3 -> 4
    coded(5) := data_parity_in(5);
    coded(6) := data_parity_in(6);

    -- syndrome generation
    syn(3 downto 1) := parity(3 downto 1) xor parity_in(3 downto 1);
    P0              := '0';
    P1              := '0';
    for i in 0 to 3 loop
      P0 := P0 xor parity(i);
      P1 := P1 xor parity_in(i);
    end loop;
    syn(0) := P0 xor P1;

    case syn(3 downto 1) is
      when "011"  => syndrome := 3; syndrome_do := 0; syn_do_valid := '1';
      when "101"  => syndrome := 5; syndrome_do := 1; syn_do_valid := '1';
      when "110"  => syndrome := 6; syndrome_do := 2; syn_do_valid := '1';
      when others => syndrome := 0; syndrome_do := 0; syn_do_valid := '0';
    end case;


    -- defaults
    data_out <= coded(6) & coded(5) & coded(3);
    err_out  <= '0';

    if syn(0) = '1' then
      err_out <= '1';
      if (syn_do_valid = '1') then
        data_out(syndrome_do) <= not(coded(syndrome));
      end if;

    elsif syndrome /= 0 then            -- There are more than one error
      data_out <= (others => '0');      -- FATAL ERROR
      err_out  <= '1';

    end if;

  end procedure;



  ----------------------------------------------------------------------  

  procedure hamm_dec_4b(
    data_parity_in  :     unsigned(7 downto 0);
    error_value_in  :     natural;
    signal err_out  : out std_logic;
    signal data_out : out unsigned(3 downto 0)
    ) is

    variable coded        : unsigned(7 downto 0);
    variable syndrome     : natural range 0 to 7;
    variable syndrome_do  : natural range 0 to 3;
    variable syn_do_valid : std_logic;
    variable parity       : unsigned(3 downto 0);
    variable parity_in    : unsigned(3 downto 0);
    variable syn          : unsigned(3 downto 0);
    variable data_in      : unsigned(3 downto 0);
    variable decoded_uns  : unsigned(3 downto 0);
    variable P0, P1       : std_logic;


  begin

    data_in   := data_parity_in(7 downto 4);
    parity_in := data_parity_in(3 downto 0);

    parity(3) := data_in(1) xor data_in(2) xor data_in(3);
    parity(2) := data_in(0) xor data_in(2) xor data_in(3);
    parity(1) := data_in(0) xor data_in(1) xor data_in(3);
    parity(0) := data_in(0) xor data_in(1) xor data_in(2) xor data_in(3) xor
                 parity(1) xor parity(2) xor parity(3);

    coded(0) := data_parity_in(0);
    coded(1) := data_parity_in(1);
    coded(2) := data_parity_in(2);
    coded(4) := data_parity_in(3);      -- note 4 -> 3
    coded(3) := data_parity_in(4);      -- note 3 -> 4
    coded(5) := data_parity_in(5);
    coded(6) := data_parity_in(6);
    coded(7) := data_parity_in(7);

    -- syndrome generation
    syn(3 downto 1) := parity(3 downto 1) xor parity_in(3 downto 1);
    P0              := '0';
    P1              := '0';
    for i in 0 to 3 loop
      P0 := P0 xor parity(i);
      P1 := P1 xor parity_in(i);
    end loop;
    syn(0) := P0 xor P1;

    case syn(3 downto 1) is
      when "011"  => syndrome := 3; syndrome_do := 0; syn_do_valid := '1';
      when "101"  => syndrome := 5; syndrome_do := 1; syn_do_valid := '1';
      when "110"  => syndrome := 6; syndrome_do := 2; syn_do_valid := '1';
      when "111"  => syndrome := 7; syndrome_do := 3; syn_do_valid := '1';
      when others => syndrome := 0; syndrome_do := 0; syn_do_valid := '0';
    end case;


    -- defaults
    data_out <= coded(7) & coded(6) &coded(5) & coded(3);
    err_out  <= '0';

    if (syn(0) = '1') then
      err_out <= '1';
      if (syn_do_valid = '1') then
        data_out(syndrome_do) <= not(coded(syndrome));
      end if;

    elsif (syndrome /= 0) then          -- There are more than one error
      data_out <= to_unsigned(error_value_in, 4);
      err_out  <= '1';

    end if;



  end procedure;

  ----------------------------------------------------------------------  

  procedure hamm_dec_5b(
    data_parity_in  :     unsigned(9 downto 0);
    error_value_in  :     natural;
    signal err_out  : out std_logic;
    signal data_out : out unsigned(4 downto 0)
    ) is

    variable coded        : unsigned(9 downto 0);
    variable syndrome     : natural range 0 to 9;
    variable syndrome_do  : natural range 0 to 4;
    variable syn_do_valid : std_logic;
    variable parity       : unsigned(4 downto 0);
    variable parity_in    : unsigned(4 downto 0);
    variable syn          : unsigned(4 downto 0);
    variable data_in      : unsigned(4 downto 0);
    variable decoded_uns  : unsigned(4 downto 0);
    variable P0, P1       : std_logic;

  begin

    data_in   := data_parity_in(9 downto 5);
    parity_in := data_parity_in(4 downto 0);

    parity(4) := data_in(4);
    parity(3) := data_in(1) xor data_in(2) xor data_in(3);
    parity(2) := data_in(0) xor data_in(2) xor data_in(3);
    parity(1) := data_in(0) xor data_in(1) xor data_in(3) xor data_in(4);
    parity(0) := data_in(0) xor data_in(1) xor data_in(2) xor data_in(3) xor data_in(4) xor
                 parity(1) xor parity(2) xor parity(3) xor parity(4);

    coded(0) := data_parity_in(0);
    coded(1) := data_parity_in(1);
    coded(2) := data_parity_in(2);
    coded(4) := data_parity_in(3);
    coded(8) := data_parity_in(4);
    coded(3) := data_parity_in(5);
    coded(5) := data_parity_in(6);
    coded(6) := data_parity_in(7);
    coded(7) := data_parity_in(8);
    coded(9) := data_parity_in(9);

    -- syndrome generation
    syn(4 downto 1) := parity(4 downto 1) xor parity_in(4 downto 1);
    P0              := '0';
    P1              := '0';
    for i in 0 to 4 loop
      P0 := P0 xor parity(i);
      P1 := P1 xor parity_in(i);
    end loop;
    syn(0) := P0 xor P1;

    case syn(4 downto 1) is
      when "0011" => syndrome := 3; syndrome_do := 0; syn_do_valid := '1';
      when "0101" => syndrome := 5; syndrome_do := 1; syn_do_valid := '1';
      when "0110" => syndrome := 6; syndrome_do := 2; syn_do_valid := '1';
      when "0111" => syndrome := 7; syndrome_do := 3; syn_do_valid := '1';
      when "1001" => syndrome := 9; syndrome_do := 4; syn_do_valid := '1';
      when others => syndrome := 0; syndrome_do := 0; syn_do_valid := '0';
    end case;


    -- defaults
    data_out <= coded(9) & coded(7) & coded(6) & coded(5) & coded(3);
    err_out  <= '0';

    if (syn(0) = '1') then
      err_out <= '1';
      if (syn_do_valid = '1') then
        data_out(syndrome_do) <= not(coded(syndrome));
      end if;

    elsif (syndrome /= 0) then          -- There is more than one error
      data_out <= to_unsigned(error_value_in, 5);
      err_out  <= '1';

    end if;

  end procedure;


  -- ------------------------------------------------------------------------------------------
  -- procedure hamming_decoder_32bit(data_parity_in   :     unsigned(38 downto 0);
  --                                 signal error_out : out std_logic_vector(1 downto 0);
  --                                 signal decoded   : out unsigned(31 downto 0)) is
  --   variable coded     :   unsigned(38 downto 0);
  --   variable syndrome  : natural range 0 to 38;
  --   variable parity    :   unsigned(6 downto 0);
  --   variable parity_in :   unsigned(6 downto 0);
  --   variable syn       :   unsigned(6 downto 0);
  --   variable data_in   :   unsigned(31 downto 0);
  --   variable P0, P1    : std_logic;
  -- begin

  --   data_in   := data_parity_in(38 downto 7);
  --   parity_in := data_parity_in(6 downto 0);

  --   parity(6) := data_in(26) xor data_in(27) xor data_in(28) xor data_in(29) xor data_in(30) xor
  --                data_in(31);

  --   parity(5) := data_in(11) xor data_in(12) xor data_in(13) xor data_in(14) xor data_in(15) xor
  --                data_in(16) xor data_in(17) xor data_in(18) xor data_in(19) xor data_in(20) xor
  --                data_in(21) xor data_in(22) xor data_in(23) xor data_in(24) xor data_in(25);


  --   parity(4) := data_in(4) xor data_in(5) xor data_in(6) xor data_in(7) xor data_in(8) xor
  --                data_in(9) xor data_in(10) xor data_in(18) xor data_in(19) xor data_in(20) xor
  --                data_in(21) xor data_in(22) xor data_in(23) xor data_in(24) xor data_in(25);


  --   parity(3) := data_in(1) xor data_in(2) xor data_in(3) xor data_in(7) xor data_in(8) xor
  --                data_in(9) xor data_in(10) xor data_in(14) xor data_in(15) xor data_in(16) xor
  --                data_in(17) xor data_in(22) xor data_in(23) xor data_in(24) xor data_in(25) xor
  --                data_in(29) xor data_in(30) xor data_in(31);

  --   parity(2) := data_in(0) xor data_in(2) xor data_in(3) xor data_in(5) xor data_in(6) xor
  --                data_in(9) xor data_in(10) xor data_in(12) xor data_in(13) xor data_in(16) xor
  --                data_in(17) xor data_in(20) xor data_in(21) xor data_in(24) xor data_in(25) xor
  --                data_in(27) xor data_in(28) xor data_in(31);

  --   parity(1) := data_in(0) xor data_in(1) xor data_in(3) xor data_in(4) xor data_in(6) xor
  --                data_in(8) xor data_in(10) xor data_in(11) xor data_in(13) xor data_in(15) xor
  --                data_in(17) xor data_in(19) xor data_in(21) xor data_in(23) xor data_in(25) xor
  --                data_in(26) xor data_in(28) xor data_in(30);

  --   parity(0) := data_in(0) xor data_in(1) xor data_in(2) xor data_in(3) xor data_in(4) xor
  --                data_in(5) xor data_in(6) xor data_in(7) xor data_in(8) xor data_in(9) xor
  --                data_in(10) xor data_in(11) xor data_in(12) xor data_in(13) xor data_in(14) xor
  --                data_in(15) xor data_in(16) xor data_in(17) xor data_in(18) xor data_in(19) xor
  --                data_in(20) xor data_in(21) xor data_in(22) xor data_in(23) xor data_in(24) xor
  --                data_in(25) xor data_in(26) xor data_in(27) xor data_in(28) xor data_in(29) xor
  --                data_in(30) xor data_in(31) xor parity(1) xor parity(2) xor parity(3) xor
  --                parity(4) xor parity(5) xor parity(6);

  --   coded(0)  := data_parity_in(0);
  --   coded(1)  := data_parity_in(1);
  --   coded(2)  := data_parity_in(2);
  --   coded(4)  := data_parity_in(3);
  --   coded(8)  := data_parity_in(4);
  --   coded(16) := data_parity_in(5);
  --   coded(32) := data_parity_in(6);
  --   coded(3)  := data_parity_in(7);
  --   coded(5)  := data_parity_in(8);
  --   coded(6)  := data_parity_in(9);
  --   coded(7)  := data_parity_in(10);
  --   coded(9)  := data_parity_in(11);
  --   coded(10) := data_parity_in(12);
  --   coded(11) := data_parity_in(13);
  --   coded(12) := data_parity_in(14);
  --   coded(13) := data_parity_in(15);
  --   coded(14) := data_parity_in(16);
  --   coded(15) := data_parity_in(17);
  --   coded(17) := data_parity_in(18);
  --   coded(18) := data_parity_in(19);
  --   coded(19) := data_parity_in(20);
  --   coded(20) := data_parity_in(21);
  --   coded(21) := data_parity_in(22);
  --   coded(22) := data_parity_in(23);
  --   coded(23) := data_parity_in(24);
  --   coded(24) := data_parity_in(25);
  --   coded(25) := data_parity_in(26);
  --   coded(26) := data_parity_in(27);
  --   coded(27) := data_parity_in(28);
  --   coded(28) := data_parity_in(29);
  --   coded(29) := data_parity_in(30);
  --   coded(30) := data_parity_in(31);
  --   coded(31) := data_parity_in(32);
  --   coded(33) := data_parity_in(33);
  --   coded(34) := data_parity_in(34);
  --   coded(35) := data_parity_in(35);
  --   coded(36) := data_parity_in(36);
  --   coded(37) := data_parity_in(37);
  --   coded(38) := data_parity_in(38);

  --   -- syndrome generation
  --   syn(6 downto 1) := parity(6 downto 1) xor parity_in(6 downto 1);
  --   P0              := '0';
  --   P1              := '0';
  --   for i in 0 to 6 loop
  --     P0 := P0 xor parity(i);
  --     P1 := P1 xor parity_in(i);
  --   end loop;
  --   syn(0) := P0 xor P1;

  --   case syn(6 downto 1) is
  --     when "000011" => syndrome := 3;
  --     when "000101" => syndrome := 5;
  --     when "000110" => syndrome := 6;
  --     when "000111" => syndrome := 7;
  --     when "001001" => syndrome := 9;
  --     when "001010" => syndrome := 10;
  --     when "001011" => syndrome := 11;
  --     when "001100" => syndrome := 12;
  --     when "001101" => syndrome := 13;
  --     when "001110" => syndrome := 14;
  --     when "001111" => syndrome := 15;
  --     when "010001" => syndrome := 17;
  --     when "010010" => syndrome := 18;
  --     when "010011" => syndrome := 19;
  --     when "010100" => syndrome := 20;
  --     when "010101" => syndrome := 21;
  --     when "010110" => syndrome := 22;
  --     when "010111" => syndrome := 23;
  --     when "011000" => syndrome := 24;
  --     when "011001" => syndrome := 25;
  --     when "011010" => syndrome := 26;
  --     when "011011" => syndrome := 27;
  --     when "011100" => syndrome := 28;
  --     when "011101" => syndrome := 29;
  --     when "011110" => syndrome := 30;
  --     when "011111" => syndrome := 31;
  --     when "100001" => syndrome := 33;
  --     when "100010" => syndrome := 34;
  --     when "100011" => syndrome := 35;
  --     when "100100" => syndrome := 36;
  --     when "100101" => syndrome := 37;
  --     when "100110" => syndrome := 38;
  --     when others   => syndrome := 0;
  --   end case;

  --   if syn(0) = '1' then
  --     coded(syndrome) := not(coded(syndrome));
  --     error_out       <= "01";          -- There is an error
  --   elsif syndrome /= 0 then            -- There are more than one error
  --     coded     := (others => '0');     -- FATAL ERROR
  --     error_out <= "11";
  --   else
  --     error_out <= "00";                -- No errors detected
  --   end if;

  --   decoded(0)  <= coded(3);
  --   decoded(1)  <= coded(5);
  --   decoded(2)  <= coded(6);
  --   decoded(3)  <= coded(7);
  --   decoded(4)  <= coded(9);
  --   decoded(5)  <= coded(10);
  --   decoded(6)  <= coded(11);
  --   decoded(7)  <= coded(12);
  --   decoded(8)  <= coded(13);
  --   decoded(9)  <= coded(14);
  --   decoded(10) <= coded(15);
  --   decoded(11) <= coded(17);
  --   decoded(12) <= coded(18);
  --   decoded(13) <= coded(19);
  --   decoded(14) <= coded(20);
  --   decoded(15) <= coded(21);
  --   decoded(16) <= coded(22);
  --   decoded(17) <= coded(23);
  --   decoded(18) <= coded(24);
  --   decoded(19) <= coded(25);
  --   decoded(20) <= coded(26);
  --   decoded(21) <= coded(27);
  --   decoded(22) <= coded(28);
  --   decoded(23) <= coded(29);
  --   decoded(24) <= coded(30);
  --   decoded(25) <= coded(31);
  --   decoded(26) <= coded(33);
  --   decoded(27) <= coded(34);
  --   decoded(28) <= coded(35);
  --   decoded(29) <= coded(36);
  --   decoded(30) <= coded(37);
  --   decoded(31) <= coded(38);

  -- end;


end lcb_pkg_globals;
