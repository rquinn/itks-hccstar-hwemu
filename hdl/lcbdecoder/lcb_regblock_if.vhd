--
-- LCB Register Block Interface
--
-- 
-- Matt Warren Dec 2016
--
-- Last update Nov 2017
--
-- 20180425 Added soc=eoc=1 (aka decoder_err from decoder) resets the state-machine 



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library hccemu_lcbdec;
use hccemu_lcbdec.lcb_pkg_globals.all;

entity lcb_regblock_if is
   generic( 
      ASIC_IS_ABC : natural := 1
   );
   port( 
      chipid_i         : in     std_logic_vector (3 downto 0);
      -- RegBlock interface
      rb_addr_o        : out    std_logic_vector (7 downto 0);
      rb_rnw_o         : out    std_logic;
      rb_init_o        : out    std_logic;
      rb_load_o        : out    std_logic;
      rb_sdat_o        : out    std_logic;
      rb_shen_o        : out    std_logic;
      rb_busy_i        : in     std_logic;
      -- CmdFIFO
      cfifo_all_i      : in     std_logic_vector (8 downto 0);
      cfifo_empty_i    : in     std_logic;
      cfifo_re_o       : out    std_logic;
      --Other
      --err_soc_u_o       : out std_logic;
      --err_eoc_u_o       : out std_logic;
      --err_words_ovrf_o  : out std_logic;
      --err_words_undf_o       : out std_logic;
      scmd_err_o       : out    std_logic;
      state_hamm_err_o : out    std_logic;
      tmr_err_o        : out    std_logic;
      k2_pending_o     : out    std_logic;
      -- Infra
      clk40            : in     std_logic;                      --40MHz BCO
      rstb             : in     std_logic;                      --sync
      initb            : in     std_logic                       --async
   );

-- Declarations

end entity lcb_regblock_if ;

---------------------------------------------------------------------------
architecture rtl of lcb_regblock_if is

  constant C_RST_REG_ADDR     : unsigned(7 downto 0) := x"00";
  constant C_DEFAULT_REG_ADDR : unsigned(7 downto 0) := x"00";

  component lcb_tmr_reg
    generic (
      WIDTH   : natural := 8;
      RST_VAL : natural := 0
      );
    port(
      signal initb : in  std_logic;
      signal rstb  : in  std_logic;
      signal clk   : in  std_logic;
      signal ena   : in  std_logic;
      signal d     : in  unsigned(WIDTH-1 downto 0);
      signal q     : out unsigned(WIDTH-1 downto 0);
      signal err   : out std_logic
      );
  end component lcb_tmr_reg;

  component lcb_tmr_bit
    generic (
      RST_VAL : std_logic := '0'
      );
    port(
      signal initb : in  std_logic;
      signal rstb  : in  std_logic;
      signal clk   : in  std_logic;
      signal ena   : in  std_logic;
      signal d     : in  std_logic;
      signal q     : out std_logic;
      signal err   : out std_logic
      );
  end component lcb_tmr_bit;


  signal asic_is_abc_sl : std_logic;

  signal error_unexpected_soc : std_logic;  -- bring these out
  signal error_unexpected_eoc : std_logic;
  signal error_too_many_words : std_logic;
  signal error_too_few_words  : std_logic;
  signal error_bad_frame      : std_logic;

  signal cfifo_data : unsigned(6 downto 0);
  signal cfifo_soc  : std_logic;
  signal cfifo_eoc  : std_logic;
  signal cfifo_re   : std_logic;

  signal cfifo_data_abcid       : unsigned(3 downto 0);
  signal cfifo_data_seoc_chipid : unsigned(3 downto 0);
  signal cfifo_data_seoc_abchcc : std_logic;

  signal soc_only : std_logic;
  signal eoc_only : std_logic;

  signal word_count_clr : std_logic;
  signal word_count_inc : std_logic;
  signal word_count     : unsigned(2 downto 0);

  signal rb_addr : unsigned(7 downto 0);
  signal rb_rnw  : std_logic;
  signal rb_init : std_logic;
  signal rb_sdat : std_logic;
  signal rb_shen : std_logic;
  signal rb_load : std_logic;

  signal addr_store0 : std_logic;
  signal addr_store1 : std_logic;

  type t_states is (
    IdleWaitSoc,                                                       -- 1  
    Header0, Header1,                                                  -- 3
    CheckWriteDat3, CheckWriteDat6,                                    -- 5
    WriteDat5, WriteDat4, WriteDat3, WriteDat2, WriteDat1, WriteDat0,  --11
    WaitRBBusyUp, WaitRBBusyDown,                                      -- 13
    WaitEoC,                                                           -- 14
    InvalidState0, InvalidState1                                       --15, 16
    );

  
  signal state, nstate : t_states;
  signal state_uns     : unsigned(3 downto 0);

  signal state_hamm : unsigned(7 downto 0);
  --signal error_hamm : unsigned(1 downto 0);


  signal scmd_err : std_logic;

  signal rb_load_err   : std_logic;
  signal rb_rnw_err    : std_logic;
  signal rb_addr76_err : std_logic;
  signal rb_addr50_err : std_logic;


  -- synopsys translate_off
  signal dbg_rbif_cmdfrm : unsigned(48 downto 0);
  -- synopsys translate_on

  signal coded_hamm : unsigned(7 downto 0);

-------------------------------------------------------------------------------
begin

  tmr_err_o <= rb_load_err or rb_rnw_err or rb_addr76_err or rb_addr50_err;


  -- error_bus_o <= (error_unexpected_soc &
  --                 error_unexpected_eoc &
  --                 error_too_many_words &
  --                 error_too_few_words) when rising_edge(clk40);

  --err_soc_u_o      <= error_unexpected_soc when rising_edge(clk40);
  --err_eoc_u_o      <= error_unexpected_eoc when rising_edge(clk40);
  --err_words_ovrf_o <= error_too_many_words when rising_edge(clk40);
  --err_words_undf_o <= error_too_few_words  when rising_edge(clk40);

  scmd_err <= error_unexpected_soc or
              error_unexpected_eoc or
              error_too_many_words or
              error_too_few_words or
              error_bad_frame;

  scmd_err_o <= scmd_err when rising_edge(clk40);


  asic_is_abc_sl <= '1' when (ASIC_IS_ABC = 1) else '0';


  -- synopsys translate_off
  prc_dbg_cmdfrm : process(clk40)
  begin
    if rising_edge(clk40) then
      if (cfifo_re = '1') then
        dbg_rbif_cmdfrm <= dbg_rbif_cmdfrm(41 downto 0) & cfifo_data;
      end if;
    end if;
  end process;
  -- synopsys translate_on

  --           0      1       2       3       4       5       6
  -- Frame RCCCCAA AAAAAA0 000DDDD DDDDDDD DDDDDDD DDDDDDD DDDDDDD
  -- (R=RnW, CHIP-ID, A=Addr, D=Data)
  -- Mappings

  cfifo_re_o <= cfifo_re;
  cfifo_soc  <= cfifo_all_i(8);
  cfifo_eoc  <= cfifo_all_i(7);
  cfifo_data <= unsigned(cfifo_all_i(6 downto 0));

  cfifo_data_abcid       <= unsigned(cfifo_data(5 downto 2));
  cfifo_data_seoc_chipid <= unsigned(cfifo_data(3 downto 0));
  cfifo_data_seoc_abchcc <= cfifo_data(5);


  -- cmd frame decoder/sequencer
  --=============================================================================

  g_hamming : if (C_HAMMING_EN = 1) generate
  begin

    prc_ser_sm_sync_part : process (clk40, initb)
    begin
      if (initb = '0') then
        state_hamm      <= hamm_enc_4b(0);  --IdleWaitSoC;
        error_bad_frame <= '0';

      elsif rising_edge(clk40) then

        -- default
        error_bad_frame <= '0';

        if (rstb = '0') then
          state_hamm <= hamm_enc_4b(0);  --IdleWaitSoC;

        elsif (cfifo_soc = '1') and (cfifo_eoc = '1') and (cfifo_empty_i = '0') then
          state_hamm <= hamm_enc_4b(0);  --IdleWaitSoC;
          if (state /= IdleWaitSoC) then
            error_bad_frame <= '1';
          end if;

        else
          state_hamm <= hamm_enc_4b(t_states'pos(nstate));

        end if;
      end if;
    end process;

    hamm_dec_4b(
      state_hamm,                       --in
      0,  -- error_value -- in
      --error_hamm, --out
      state_hamm_err_o,                 --out
      state_uns                         --out
      );
    state <= t_states'val(to_integer(state_uns));
  end generate;


  g_no_hamming : if (C_HAMMING_EN = 0) generate
  begin
    prc_ser_sm_sync_part : process (clk40, initb)
    begin
      if (initb = '0') then
        state           <= IdleWaitSoC;
        error_bad_frame <= '0';

      elsif rising_edge(clk40) then

        -- default
        error_bad_frame <= '0';

        if (rstb = '0') then
          state <= IdleWaitSoC;

        elsif (cfifo_soc = '1') and (cfifo_eoc = '1') and (cfifo_empty_i = '0') then
          state <= IdleWaitSoC;
          if (state /= IdleWaitSoC) then
            error_bad_frame <= '1';
          end if;


        else
          state <= nstate;

        end if;
      end if;
    end process;

  end generate;


  soc_only <= cfifo_soc and not cfifo_eoc;
  eoc_only <= not cfifo_soc and cfifo_eoc;


  --------------------------------------
  prc_ser_sm_async_part : process (asic_is_abc_sl, cfifo_data,
                                   cfifo_data_abcid, cfifo_data_seoc_abchcc,
                                   cfifo_data_seoc_chipid, cfifo_empty_i,
                                   chipid_i, eoc_only, rb_busy_i, rb_rnw,
                                   soc_only, state, word_count)
  begin

    -- defaults
    cfifo_re             <= '0';
    addr_store0          <= '0';
    addr_store1          <= '0';
    rb_init              <= '0';
    rb_load              <= '0';
    rb_sdat              <= '0';
    rb_shen              <= '0';
    error_unexpected_soc <= '0';
    error_unexpected_eoc <= '0';
    error_too_many_words <= '0';
    error_too_few_words  <= '0';
    word_count_clr       <= '0';
    word_count_inc       <= '0';

    case state is

      when IdleWaitSoC =>               -- check if HCCID /= 0xe, and ABC/nHCC correct
        nstate         <= IdleWaitSoC;
        word_count_clr <= '1';

        if (cfifo_empty_i = '0') then
          cfifo_re <= '1';

          if (eoc_only = '1') then
            error_unexpected_eoc <= '1';

          else
            if (asic_is_abc_sl = '1') then  --ABC mode
              if (soc_only = '1') then
                nstate <= WaitEoC;

                if (cfifo_data_seoc_chipid /= "1110") then
                  if (cfifo_data_seoc_abchcc = '1') then
                    nstate <= Header0;
                  end if;
                end if;
              end if;

            else                        --HCC mode
              if (soc_only = '1') then
                nstate <= WaitEoC;

                if (cfifo_data_seoc_chipid = unsigned(chipid_i)) or (cfifo_data_seoc_chipid = "1111") then
                  if (cfifo_data_seoc_abchcc = '0') then
                    nstate <= Header0;
                  end if;
                end if;
              end if;
            end if;
          end if;
        end if;

        --------------------------------------------------------------------

      when Header0 =>                   -- check abcid
        nstate <= Header0;

        if (cfifo_empty_i = '0') then   -- wait new data
          cfifo_re <= '1';              -- "Ack" data 

          if (soc_only = '1') then
            error_unexpected_soc <= '1';
            nstate               <= IdleWaitSoC;  -- "reset" machine

          elsif (eoc_only = '1') then
            error_unexpected_eoc <= '1';
            nstate               <= IdleWaitSoC;  -- "reset" machine

          else
            word_count_inc <= '1';
            addr_store0    <= '1';
            nstate         <= Header1;

            -- In ABC mode we need to check the ABCID
            if (asic_is_abc_sl = '1') then
              if (cfifo_data_abcid /= unsigned(chipid_i)) and (cfifo_data_abcid /= "1111") then
                addr_store0 <= '0';
                nstate      <= WaitEoC;  -- not for me, wait for EoC 
              end if;
            end if;
          end if;
        end if;




      when Header1 =>
        nstate <= Header1;
        if (cfifo_empty_i = '0') then   -- wait new data

          if (soc_only = '1') then
            error_unexpected_soc <= '1';
            nstate               <= IdleWaitSoC;  -- "reset" machine
          elsif (eoc_only = '1') then
            error_unexpected_eoc <= '1';
            nstate               <= IdleWaitSoC;  -- "reset" machine
          else

            word_count_inc <= '1';
            addr_store1    <= '1';
            rb_init        <= '1';
            cfifo_re       <= '1';      -- "Ack" data

            if (rb_rnw = '1') then       --read
              nstate <= WaitRBBusyUp;
            else
              nstate <= CheckWriteDat3;  -- first write chunk has only 4b data
            end if;

          end if;
        end if;


        -- Read Logic -----------------------------------------------------

      when WaitRBBusyUp =>
        nstate <= WaitRBBusyUp;
        if (rb_busy_i = '1') then
          nstate <= WaitRBBusyDown;
        end if;


      when WaitRBBusyDown =>
        nstate <= WaitRBBusyDown;
        if (rb_busy_i = '0') then
          nstate <= WaitEoC;
        end if;


        -- Write Logic -----------------------------------------------------

      when CheckWriteDat3 =>
        nstate  <= CheckWriteDat3;
        rb_sdat <= cfifo_data(3);
        if (cfifo_empty_i = '0') then             -- wait new data
          if (soc_only = '1') then
            error_unexpected_soc <= '1';
            nstate               <= IdleWaitSoC;  -- "reset" machine

          elsif (eoc_only = '1') then
            error_unexpected_eoc <= '1';
            nstate               <= IdleWaitSoC;  -- "reset" machine

          else
            if (rb_busy_i = '0') then
              word_count_inc <= '1';
              rb_shen        <= '1';
              nstate         <= WriteDat2;
            end if;
          end if;
        end if;


        -----------------------------------------------------------        

      when CheckWriteDat6 =>
        nstate  <= CheckWriteDat6;
        rb_sdat <= cfifo_data(6);
        if (cfifo_empty_i = '0') then             -- wait new data
          if (soc_only = '1') then
            error_unexpected_soc <= '1';
            nstate               <= IdleWaitSoC;  -- "reset" machine

          else
            if (rb_busy_i = '0') then

              if (eoc_only = '1') then
                cfifo_re <= '1';        -- "ack" eoc
                nstate   <= IdleWaitSoC;

                if (word_count = "111") then
                  rb_load <= '1';
                else
                  error_too_few_words <= '1';
                end if;

              else                      -- not EoC yet
                if (word_count /= "111") then
                  word_count_inc <= '1';
                  rb_shen        <= '1';
                  nstate         <= WriteDat5;
                else
                  error_too_many_words <= '1';
                  nstate               <= IdleWaitSoC;
                end if;
              end if;
            end if;
          end if;
        end if;


      when WriteDat5 =>
        nstate  <= WriteDat5;
        rb_sdat <= cfifo_data(5);
        if (rb_busy_i = '0') then
          rb_shen <= '1';
          nstate  <= WriteDat4;
        end if;

      when WriteDat4 =>
        nstate  <= WriteDat4;
        rb_sdat <= cfifo_data(4);
        if (rb_busy_i = '0') then
          rb_shen <= '1';
          nstate  <= WriteDat3;
        end if;

      when WriteDat3 =>
        nstate  <= WriteDat3;
        rb_sdat <= cfifo_data(3);
        if (rb_busy_i = '0') then
          rb_shen <= '1';
          nstate  <= WriteDat2;
        end if;

      when WriteDat2 =>
        nstate  <= WriteDat2;
        rb_sdat <= cfifo_data(2);
        if (rb_busy_i = '0') then
          rb_shen <= '1';
          nstate  <= WriteDat1;
        end if;

      when WriteDat1 =>
        nstate  <= WriteDat1;
        rb_sdat <= cfifo_data(1);
        if (rb_busy_i = '0') then
          rb_shen <= '1';
          nstate  <= WriteDat0;
        end if;

      when WriteDat0 =>
        nstate  <= WriteDat0;
        rb_sdat <= cfifo_data(0);       -- and not(rb_rnw);
        if (rb_busy_i = '0') then
          rb_shen  <= '1';
          cfifo_re <= '1';              -- "ack" current data
          nstate   <= CheckWriteDat6;
        end if;


      ---------------------------------------------------------------
      when WaitEoC =>
        nstate <= WaitEoC;
        if (cfifo_empty_i = '0') then
          if (soc_only = '1') then
            error_unexpected_soc <= '1';
            nstate               <= IdleWaitSoC;  -- "reset" machine
          else
            cfifo_re <= '1';                      -- "ack" data
            if (eoc_only = '1') then
              nstate <= IdleWaitSoC;
            end if;
          end if;
        end if;


      when InvalidState0 =>             -- 15th state
        nstate <= IdleWaitSoC;          -- Abort, start again

      when InvalidState1 =>             -- 16th state
        nstate <= IdleWaitSoC;          -- Abort, start again
        
      when others =>                    -- should never happen, but lets make it 1000% clear
        nstate <= IdleWaitSoC;


    -------------------------------------------------------------------
    end case;

  end process;


  -----------------------------------------------------------------------

  prc_word_counter : process (clk40, initb)
  begin
    if (initb = '0') then
      word_count <= (others => '0');

    elsif rising_edge(clk40) then
      if (rstb = '0') then
        word_count <= (others => '0');

      else
        if (word_count_clr = '1') then
          word_count <= (others => '0');

        elsif (word_count_inc = '1') then
          word_count <= word_count + 1;

        end if;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------


  g_tmr_addr : if (C_TMR_EN = 1) generate
  begin

    Utmr_reg_addr76 : lcb_tmr_reg
      generic map (
        WIDTH   => 2,
        RST_VAL => 0
        )
      port map (
        initb => initb,
        rstb  => rstb,
        clk   => clk40,
        ena   => addr_store0,
        d     => cfifo_data(1 downto 0),
        q     => rb_addr(7 downto 6),
        err   => rb_addr76_err
        );

    Utmr_reg_addr50 : lcb_tmr_reg
      generic map (
        WIDTH   => 6,
        RST_VAL => 0
        )
      port map (
        initb => initb,
        rstb  => rstb,
        clk   => clk40,
        ena   => addr_store1,
        d     => cfifo_data(6 downto 1),
        q     => rb_addr(5 downto 0),
        err   => rb_addr50_err
        );


    Utmr_reg_rbrnw : lcb_tmr_bit
      generic map (
        RST_VAL => '0'
        )
      port map (
        initb => initb,
        rstb  => rstb,
        clk   => clk40,
        ena   => addr_store0,
        d     => cfifo_data(6),
        q     => rb_rnw,
        err   => rb_rnw_err
        );

    -- I really don't know if this is nessessary 
    Utmr_reg_rbload : lcb_tmr_bit
      generic map (
        RST_VAL => '0'
        )
      port map (
        initb => initb,
        rstb  => rstb,
        clk   => clk40,
        ena   => '1',
        d     => rb_load,
        q     => rb_load_o,
        err   => rb_load_err
        );

  end generate;


  g_no_tmr_addr : if (C_TMR_EN = 0) generate
  begin

    prc_stores : process (clk40, initb)
    begin
      if (initb = '0') then
        rb_rnw  <= '0';
        rb_addr <= x"00";

      elsif rising_edge(clk40) then
        if (rstb = '0') then
          rb_rnw  <= '0';
          rb_addr <= x"00";

        else
          if (addr_store0 = '1') then
            rb_rnw              <= cfifo_data(6);
            rb_addr(7 downto 6) <= cfifo_data(1 downto 0);
          end if;

          if (addr_store1 = '1') then
            rb_addr(5 downto 0) <= cfifo_data(6 downto 1);
          end if;

        end if;
      end if;
    end process;


    prc_rb_load : process (clk40, initb)
    begin
      if (initb = '0') then
        rb_load_o <= '0';

      elsif rising_edge(clk40) then
        if (rstb = '0') then
          rb_load_o <= '0';
        else
          rb_load_o <= rb_load;
        end if;
      end if;
    end process;

  end generate;


------------------------------------------------------------------------
  prc_clkout : process (clk40, initb)
  begin
    if (initb = '0') then
      rb_init_o <= '0';
      rb_sdat_o <= '0';
      rb_shen_o <= '0';

    elsif rising_edge(clk40) then
      if (rstb = '0') then
        rb_init_o <= '0';
        rb_sdat_o <= '0';
        rb_shen_o <= '0';
      else
        rb_init_o <= rb_init;
        rb_sdat_o <= rb_sdat;
        rb_shen_o <= rb_shen;
      end if;
    end if;
  end process;

  -- no need to clock - done already above
  rb_rnw_o  <= rb_rnw;
  rb_addr_o <= std_logic_vector(rb_addr);


------------------------------------------------------------------------
  prc_k2_pending : process (clk40, initb)
  begin
    if (initb = '0') then
      k2_pending_o <= '0';

    elsif rising_edge(clk40) then
      if (state = IdleWaitSoC) then
        k2_pending_o <= '0';
      else
        k2_pending_o <= '1';
      end if;
    end if;
  end process;

end rtl;
