library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library hccemu_lcbdec;
  use hccemu_lcbdec.lcb_pkg_globals.all;

entity lcb_srsync is
  port (
    clk40         : in    std_logic;
    rstb          : in    std_logic;
    rb_addr_i     : in    std_logic_vector(7 downto 0);
    rb_rnw_i      : in    std_logic;
    rb_init_i     : in    std_logic;
    rb_sdat_i     : in    std_logic;
    rb_sdat_vld_i : in    std_logic;
    reg_read_o    : out   std_logic;
    reg_data_o    : out   std_logic_vector(31 downto 0);
    reg_addr_o    : out   std_logic_vector(7 downto 0)
  );
end entity lcb_srsync;

---------------------------------------------------------------------------

architecture rtl of lcb_srsync is

  type t_states is (Idle, ReadShiftData, Output);

  signal state : t_states;

  signal shcnt : unsigned(7 downto 0);

  signal reg_data : std_logic_vector(31 downto 0);
  signal reg_addr : std_logic_vector(7 downto 0);

begin

  prc_sm_sync : process (clk40) is
  begin

    if (rising_edge(clk40)) then

      case state is

        when Idle =>
          reg_read_o <= '0';
          if ((rb_rnw_i = '1') and (rb_init_i = '1')) then
            shcnt    <= x"00";
            reg_addr <= rb_addr_i;
            state    <= ReadShiftData;
          end if;
        when ReadShiftData =>
          if (rb_sdat_vld_i = '1') then
            reg_data <= reg_data(30 downto 0) & rb_sdat_i;
            if (shcnt < 31) then
              shcnt <= shcnt + 1;
            elsif (shcnt = 31) then
              state <= Output;
            else
              state <= Idle;                               -- error state?
            end if;
          end if;
        when Output =>
          reg_addr_o <= reg_addr;
          reg_data_o <= reg_data;
          reg_read_o <= '1';
          state      <= Idle;

      end case;

    end if;

  end process prc_sm_sync;

end architecture rtl;
