----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/20/2021 12:21:11 PM
-- Design Name: 
-- Module Name: lcb_syncfifo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity lcb_syncfifo is
    port (
        clk     : in  std_logic;
        rstb    : in  std_logic;
        din     : in  std_logic_vector(8 downto 0);
        we      : in  std_logic;
        dout    : out std_logic_vector(8 downto 0);
        re      : in  std_logic;
        empty   : out std_logic;
        full    : out std_logic);
end lcb_syncfifo;

architecture behavioral of lcb_syncfifo is
    constant WORDWIDTH  : integer := 9;
    constant logDEPTH   : integer := 3;
    constant MAX        : integer := 2 ** logDEPTH;

    signal rptr : unsigned(logDEPTH-1 downto 0) := (others => '0');
    signal wptr : unsigned(logDEPTH-1 downto 0) := (others => '0');

    signal occupancy : integer range 0 to MAX := 0;

    type t_mem is array (0 to MAX-1) of std_logic_vector(WORDWIDTH-1 downto 0);
    signal mem : t_mem;

begin

    dout    <= mem(to_integer(rptr));   -- dout is always valid
    empty   <= '1' when occupancy = 0 else '0';
    full    <= '1' when occupancy = MAX else '0';

    -- pointer manipulation
    prc_ptr_sync : process(clk)
    begin
        if rising_edge(clk) then
            if rstb = '0' then
                wptr <= (others => '0');
                rptr <= (others => '0');
            else
                if we = '1' then
                    wptr <= wptr + 1;
                end if;
                if  re = '1' then
                    rptr <= rptr + 1;
                end if;
            end if;
        end if;
    end process;

    -- write synchronization
    prc_wr_sync : process(clk)
    begin
        if rising_edge(clk) then
            if rstb = '0' then
                for index in 0 to MAX-1 loop
                    mem(index) <= (others => '0');
                end loop;
            else
                if we = '1' then
                    mem(to_integer(wptr)) <= din;
                end if;
            end if;
        end if;
    end process;

    -- occupancy manipulation
    prc_occ_sync : process(clk)
    begin
        if rising_edge(clk) then
            if rstb = '0' then
                occupancy <= 0;
            else
                -- can have re and we at the same time, so we only need to
                -- check for cases when they're not the same
                if re /= we then
                    if re = '1' then
                        -- re = 1 and we = 0
                        occupancy <= occupancy - 1;
                    else
                        -- re = 0 and we = 1
                        occupancy <= occupancy + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
