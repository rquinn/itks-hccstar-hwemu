--
-- ITk Strips LCB
-- TMR bit
--
-- Translated from dffe_nbit_t.v (Paul Keener and the Penn guys) into VHDL
-- I wouldn't bet my life on this version lcbing!
--
-- Matt Warren 2017/11/03
--
-- 2018/02/20 Added err output for SEE monitoring
--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library hccemu_lcbdec;
use hccemu_lcbdec.lcb_pkg_globals.all;

entity lcb_tmr_bit is
  generic (
    RST_VAL : std_logic := '0'
    );
  port(
    signal initb : in std_logic;        -- asynchronous reset
    signal rstb  : in std_logic;        -- synchronous reset

    signal clk : in  std_logic;
    signal ena : in  std_logic;
    signal d   : in  std_logic;
    signal q   : out std_logic;
    signal err : out std_logic
    );


end entity lcb_tmr_bit;

---------------------------------------------------------------------------
architecture rtl of lcb_tmr_bit is

  signal bit_a : std_logic;
  signal bit_b : std_logic;
  signal bit_c : std_logic;

  signal q_int : std_logic;

begin
-----------------------------------------------------------------------------

  -- majority logic
  q_int <= (bit_a and bit_b) or
           (bit_a and bit_c) or
           (bit_b and bit_c);

  proc_reg : process(clk, initb)
  begin
    if (initb = '0') then
      bit_a <= RST_VAL;
      bit_b <= RST_VAL;
      bit_c <= RST_VAL;

    elsif rising_edge(clk) then
      if (rstb = '0') then
        bit_a <= RST_VAL;
        bit_b <= RST_VAL;
        bit_c <= RST_VAL;

      else
        if (ena = '1') then
          bit_a <= d;
          bit_b <= d;
          bit_c <= d;

        else
          bit_a <= q_int;               -- refresh majority-logic value
          bit_b <= q_int;
          bit_c <= q_int;

        end if;
      end if;
    end if;
  end process;

  q <= q_int;

--  PTK hack to fix this logic
--  err <= '0' when ((bit_a and bit_b) = bit_c) else '1';
--  err <= '0' when
--       ((bit_a xor bit_b) or (bit_a xor bit_c) or (bit_b xor bit_c)) = '0'
--        else '1';

-- MRMW tidies hack
  err <= (bit_a xor bit_b) or
         (bit_a xor bit_c) or
         (bit_b xor bit_c);


  
end rtl;
