--
-- ITk Strips LCB
-- TMR register
--
-- Translated from dffe_nbit_t.v (Paul Keener and the Penn guys) into VHDL
-- I wouldn't bet my life on this version lcbing!
--
-- Matt Warren 2017/11/03
--
-- 2018/02/20 Added err output for SEE monitoring
--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library hccemu_lcbdec;
use hccemu_lcbdec.lcb_pkg_globals.all;

entity lcb_tmr_reg is
  generic (
    WIDTH   : natural := 8;
    RST_VAL : natural := 0
    );
  port(
    signal initb : in  std_logic;       -- asynchronous reset
    signal rstb  : in  std_logic;       -- synchronous reset
    signal clk   : in  std_logic;
    signal ena   : in  std_logic;
    signal d     : in  unsigned(WIDTH-1 downto 0);
    signal q     : out unsigned(WIDTH-1 downto 0);
    signal err   : out std_logic
    );


end entity lcb_tmr_reg;

---------------------------------------------------------------------------
architecture rtl of lcb_tmr_reg is

  signal reg_a : unsigned(WIDTH-1 downto 0);
  signal reg_b : unsigned(WIDTH-1 downto 0);
  signal reg_c : unsigned(WIDTH-1 downto 0);

  signal q_int : unsigned(WIDTH-1 downto 0);

  
begin
-----------------------------------------------------------------------------

  -- majority logic
  q_int <= (reg_a and reg_b) or
           (reg_a and reg_c) or
           (reg_b and reg_c);

  proc_reg : process(clk, initb)
  begin
    if (initb = '0') then
      reg_a <= to_unsigned(RST_VAL, WIDTH);
      reg_b <= to_unsigned(RST_VAL, WIDTH);
      reg_c <= to_unsigned(RST_VAL, WIDTH);

    elsif rising_edge(clk) then
      if (rstb = '0') then
        reg_a <= to_unsigned(RST_VAL, WIDTH);
        reg_b <= to_unsigned(RST_VAL, WIDTH);
        reg_c <= to_unsigned(RST_VAL, WIDTH);
        
      else
        if (ena = '1') then
          reg_a <= d;
          reg_b <= d;
          reg_c <= d;

        else
          reg_a <= q_int;               -- refresh majority-logic value
          reg_b <= q_int;
          reg_c <= q_int;

        end if;
      end if;
    end if;
  end process;

  q <= q_int;

  err <= '0' when
         ((reg_a xor reg_b) or (reg_a xor reg_c) or (reg_b xor reg_c)) = to_unsigned( 0, WIDTH)
         else '1';

end rtl;
