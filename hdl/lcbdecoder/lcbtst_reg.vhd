--
-- LCB Test Regblock
--
--
-- Matt Warren Apr 2017
--
--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;

library hccemu_lcbdec;
use hccemu_lcbdec.lcb_pkg_globals.all;



entity lcbtst_reg is
  generic(
    ADDR : natural range 0 to 255 := 0
    );
  port(

    rb_addr_i : in  std_logic_vector (7 downto 0);
    rb_rnw_i  : in  std_logic;
    rb_init_i : in  std_logic;
    rb_load_i : in  std_logic;
    rb_sdat_i : in  std_logic;
    rb_shen_i : in  std_logic;
    rb_busy_o : out std_logic;

    rb_sdato_o : out std_logic;
    rb_sdato_vld_o : out std_logic;
    
    ----------
    -- reg_o      : out unsigned(31 downto 0);
    -- reg_done_o : out std_logic;

    -- Infra
    clk40 : in std_logic;               --40MHz BCO
    rstb  : in std_logic
    );

-- Declarations

end lcbtst_reg;

---------------------------------------------------------------------------
architecture rtl of lcbtst_reg is

  signal reg    : unsigned(31 downto 0);
  signal reg_sr : unsigned(31 downto 0);
  signal scount : unsigned(7 downto 0);


begin


  prc_register : process (clk40)
  begin
    if (rstb = '0') then
      reg_sr <= (others => '0');
      reg    <= (others => '0');
      -- reg_done_o <= '0';
      rb_sdato_o <= '0';
      rb_sdato_vld_o <= '0';
      rb_busy_o <= '0';


    elsif rising_edge(clk40) then

      -- default
      -- reg_done_o <= '0';
      rb_sdato_o  <= '0';
      rb_sdato_vld_o <= '0';
      rb_busy_o <= '0';
      scount <= x"ff";

      if (rb_addr_i = std_logic_vector(to_unsigned(ADDR, 8))) then

        -- Write
        if (rb_rnw_i = '0') then
          if (rb_shen_i = '1') then
            reg_sr <= reg_sr(30 downto 0) & rb_sdat_i;
          end if;

          if (rb_load_i = '1') then
            reg        <= reg_sr;
            -- reg_done_o <= '1';
          end if;

        -- Read
        else

          if (rb_init_i = '1') then
            scount <= (others => '0');
            rb_busy_o <= '1';
            reg_sr    <= reg;


          elsif (scount < x"20") then
            scount <= scount + 1;
            rb_busy_o <= '1';
            rb_sdato_o <= reg_sr(31);
            rb_sdato_vld_o <= '1';
            reg_sr    <= reg_sr(30 downto 0) & '0';
            
          end if;
          
        end if;

      end if;
    end if;
  end process;

  -- reg_o <= reg;
  


end rtl;
