--
-- LCB Test Regblock
--
--
-- Matt Warren Apr 2017
--
--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;

library hccemu_lcbdec;
use hccemu_lcbdec.lcb_pkg_globals.all;

entity lcbtst_regblock16 is
   port( 
      rb_addr_i      : in     std_logic_vector (7 downto 0);
      rb_rnw_i       : in     std_logic;
      rb_init_i      : in     std_logic;
      rb_load_i      : in     std_logic;
      rb_sdat_i      : in     std_logic;
      rb_shen_i      : in     std_logic;
      rb_busy_o      : out    std_logic;
      rb_sdato_o     : out    std_logic;
      rb_sdato_vld_o : out    std_logic;
      ----------
      -- regblock_o     : out    uns32_array (63 downto 0);
      -- regblock_flat_o   : out  std_logic_vector (64*32-1 downto 0);
      -- reg_done_o     : out    std_logic_vector (63 downto 0);
      -- reg00_o        : out    std_logic_vector (31 downto 0);
      -- reg01_o        : out    std_logic_vector (31 downto 0);
      -- reg02_o        : out    std_logic_vector (31 downto 0);
      -- reg03_o        : out    std_logic_vector (31 downto 0);
      -- reg04_o        : out    std_logic_vector (31 downto 0);
      -- reg05_o        : out    std_logic_vector (31 downto 0);
      -- reg06_o        : out    std_logic_vector (31 downto 0);
      -- reg07_o        : out    std_logic_vector (31 downto 0);
      -- reg08_o        : out    std_logic_vector (31 downto 0);
      -- reg09_o        : out    std_logic_vector (31 downto 0);
      -- reg10_o        : out    std_logic_vector (31 downto 0);
      -- reg11_o        : out    std_logic_vector (31 downto 0);
      -- reg12_o        : out    std_logic_vector (31 downto 0);
      -- reg13_o        : out    std_logic_vector (31 downto 0);
      -- reg14_o        : out    std_logic_vector (31 downto 0);
      -- reg15_o        : out    std_logic_vector (31 downto 0);
      -- reg32_o        : out    std_logic_vector (31 downto 0);
      -- Infra
      clk40          : in     std_logic;                       --40MHz BCO
      rstb           : in     std_logic
   );

-- Declarations

end entity lcbtst_regblock16 ;

---------------------------------------------------------------------------
architecture rtl of lcbtst_regblock16 is

  component lcbtst_reg
    generic(
      ADDR : natural range 0 to 255 := 0
      );
    port(
      rb_addr_i  : in  std_logic_vector (7 downto 0);
      rb_rnw_i   : in  std_logic;
      rb_init_i  : in  std_logic;
      rb_load_i  : in  std_logic;
      rb_sdat_i  : in  std_logic;
      rb_shen_i  : in  std_logic;
      rb_busy_o  : out std_logic;
      rb_sdato_o  : out std_logic;
      rb_sdato_vld_o  : out std_logic;
      -- reg_o      : out unsigned(31 downto 0);
      -- reg_done_o : out std_logic;
      clk40      : in  std_logic;
      rstb       : in  std_logic
      );
  end component;

  constant NUM_REGS : integer := 64;
  
  -- signal regblock : uns32_array(NUM_REGS-1 downto 0);
  -- signal regblock_flat : unsigned(NUM_REGS*32-1 downto 0);
  signal rb_sdato : std_logic_vector(NUM_REGS-1 downto 0);
  signal rb_sdato_vld : std_logic_vector(NUM_REGS-1 downto 0);
  signal rb_busy : std_logic_vector(NUM_REGS-1 downto 0);
  constant ZERON : std_logic_vector(NUM_REGS-1 downto 0) := (others => '0');
  
begin

  g_regblock : for n in 0 to (NUM_REGS-1) generate

    Ureg_inst : lcbtst_reg
      generic map (
        ADDR => n
        )
      port map (
        rb_addr_i       => rb_addr_i,
        rb_rnw_i        => rb_rnw_i,
        rb_init_i       => rb_init_i,
        rb_load_i       => rb_load_i,
        rb_sdat_i       => rb_sdat_i,
        rb_shen_i       => rb_shen_i,
        rb_busy_o       => rb_busy(n),
        rb_sdato_o       => rb_sdato(n),
        rb_sdato_vld_o  => rb_sdato_vld(n),
        -- reg_o           => regblock(n),
        -- reg_done_o      => reg_done_o(n),
        clk40           => clk40,
        rstb            => rstb
        );

    -- regblock_flat(n*32+31 downto n*32) <= regblock(n);

  end generate;

  -- regblock_o <= regblock;
  -- regblock_flat_o <= std_logic_vector(regblock_flat);

  rb_sdato_o  <= '0' when (rb_sdato = ZERON) else '1';
  rb_sdato_vld_o  <= '0' when (rb_sdato_vld = ZERON) else '1';
  rb_busy_o  <= '0' when (rb_busy = ZERON) else '1';

                     


  -- reg00_o <= std_logic_vector(regblock(0));
  -- reg01_o <= std_logic_vector(regblock(1));
  -- reg02_o <= std_logic_vector(regblock(2));
  -- reg03_o <= std_logic_vector(regblock(3));
  -- reg04_o <= std_logic_vector(regblock(4));
  -- reg05_o <= std_logic_vector(regblock(5));
  -- reg06_o <= std_logic_vector(regblock(6));
  -- reg07_o <= std_logic_vector(regblock(7));
  -- reg08_o <= std_logic_vector(regblock(8));
  -- reg09_o <= std_logic_vector(regblock(9));
  -- reg10_o <= std_logic_vector(regblock(10));
  -- reg11_o <= std_logic_vector(regblock(11));
  -- reg12_o <= std_logic_vector(regblock(12));
  -- reg13_o <= std_logic_vector(regblock(13));
  -- reg14_o <= std_logic_vector(regblock(14));
  -- reg15_o <= std_logic_vector(regblock(15));


  -- reg32_o <= std_logic_vector(regblock(32));


end rtl;
