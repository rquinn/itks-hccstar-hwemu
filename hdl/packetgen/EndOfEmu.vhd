----------------------------------------------------------------------------------
-- Company: UBC
-- Engineer: Dominique Trischuk
--
-- Create Date: 01/17/2018 02:51:55 PM
-- Design Name:
-- Module Name: top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;

library hccemu_pktgen;
  use hccemu_pktgen.HCCStar_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity endofemu is
  port (
    clk                : in    std_logic;
    rst                : in    std_logic;
    din                : in    packed_plus_last;
    empty_packer       : in    std_logic;
    valid_packer       : in    std_logic;
    full_packer        : in    std_logic;
    almost_full_packer : in    std_logic;
    rd_en_packer       : out   std_logic;
    k_in               : out   std_logic;
    elinkdata_o        : out   to_elink;
    last_word          : out   std_logic;
    data_rdy           : out   std_logic
  );
end entity endofemu;

architecture endofemur_str of endofemu is

  constant sof_code   : std_logic_vector(1 downto 0) := "10"; --[comma][sof]
  constant eof_code   : std_logic_vector(1 downto 0) := "01"; --[eof][comma]
  constant data_code  : std_logic_vector(1 downto 0) := "00"; --[data][data]
  constant comma_code : std_logic_vector(1 downto 0) := "11"; --[comma][comma]

  signal rd_en_packer_sig : std_logic;
  signal k_in_sig         : std_logic:= '1';
  signal data_last        : std_logic;
  signal k_start          : std_logic := '1';
  signal datacode         : std_logic_vector(1 downto 0);
  signal last_word_s      : std_logic;

begin

  rd_en_packer <= rd_en_packer_sig;
  data_last    <= din(16);
  last_word    <= last_word_s;

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --if you need 16 bits as input to 8b/10b
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  process (clk, empty_packer, valid_packer, data_last) is
  begin

    --rd_en_packer_sig  <= '0';
    if (rising_edge(clk)) then
      --wait for packer_delay;
      data_rdy    <= '0';
      last_word_s <= '0';
      if (empty_packer = '0') then
        rd_en_packer_sig <= '1';
        if (valid_packer = '0') then
          if (rd_en_packer_sig = '1') then
            k_start <= '0';
          else
            k_start <= '1';
          end if;
        end if;
      else
        rd_en_packer_sig <= '0';
      end if;

      if (valid_packer = '1') then
        elinkdata_o <= data_code & din(15 downto 0);
        data_rdy    <= '1';
        if (k_in_sig = '1') then
          elinkdata_o <= sof_code & din(15 downto 0);
        end if;

        if (data_last = '1') then
          elinkdata_o <= eof_code & din(15 downto 0);
          last_word_s <= '1';
        end if;
      else
        elinkdata_o <= comma_code & "1011110010111100"; --2x K28.5 IDLE, maybe change this to already be in 8 bit chunks...
      end if;

      k_in_sig <= k_start;
      k_in     <= k_in_sig or data_last;
    end if;

  end process;

end architecture endofemur_str;
