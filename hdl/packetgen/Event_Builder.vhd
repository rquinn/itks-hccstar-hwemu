----------------------------------------------------------------------------------
-- Company:
-- Engineer: Dominique Trischuk
--
-- Create Date: 05/24/2018 11:23:09 AM
-- Design Name:
-- Module Name: Event_Builder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;

library hccemu_pktgen;
  use hccemu_pktgen.HCCStar_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity event_builder is
  port (
    clk_40_i     : in    std_logic;
    trig_sel_i   : in    std_logic;
    l0a_i        : in    std_logic;
    l0a_tag_i    : in    std_logic_vector(6 downto 0);
    rst          : in    std_logic;
    bcr_i        : in    std_logic;
    rd_en_event  : in    std_logic;
    start_event  : in    std_logic;
    event_info_o : out   event_info;
    empty_event  : out   std_logic;
    valid_event  : out   std_logic
  );
end entity event_builder;

architecture behavioral of event_builder is

  signal event_info_s : event_info;
  signal wr_en        : std_logic;
  signal full         : std_logic;
  signal almost_full  : std_logic;

  component event_counter is
    port (
      trig_sel_i   : in    std_logic;
      l0a_i        : in    std_logic;
      l0a_tag_i    : in    std_logic_vector(6 downto 0);
      clk_40_i     : in    std_logic;
      bcr_i        : in    std_logic;
      full         : in    std_logic;
      almost_full  : in    std_logic;
      start_event  : in    std_logic;
      event_info_o : out   event_info;
      wr_en        : out   std_logic
    );
  end component event_counter;

  component hccstar_trig_fifo is
    port (
      clk         : in    std_logic;
      srst        : in    std_logic;
      din         : in    std_logic_vector(10 downto 0);
      wr_en       : in    std_logic;
      rd_en       : in    std_logic;
      dout        : out   std_logic_vector(10 downto 0);
      full        : out   std_logic;
      almost_full : out   std_logic;
      empty       : out   std_logic;
      valid       : out   std_logic
    );
  end component hccstar_trig_fifo;

begin

  event_counter_1 : component event_counter
    port map (
      trig_sel_i   => trig_sel_i,
      l0a_i        => l0a_i,
      l0a_tag_i    => l0a_tag_i,
      clk_40_i     => clk_40_i,
      bcr_i        => bcr_i,
      full         => full,
      almost_full  => almost_full,
      start_event  => start_event,
      event_info_o => event_info_s,
      wr_en        => wr_en
    );

  event_fifo : component hccstar_trig_fifo
    port map (
      clk   => clk_40_i,
      srst  => rst,
      din   => event_info_s,
      dout  => event_info_o,
      valid => valid_event,
      wr_en => wr_en,
      rd_en => rd_en_event,
      empty => empty_event,
      full  => full
    );

end architecture behavioral;
