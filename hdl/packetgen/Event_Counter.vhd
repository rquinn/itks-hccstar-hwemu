----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 05/24/2018 11:04:47 AM
-- Design Name:
-- Module Name: event_counter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.NUMERIC_STD.ALL;

library hccemu_pktgen;
  use hccemu_pktgen.HCCStar_pkg.all;


entity event_counter is
  port (
    trig_sel_i   : in    std_logic;
    l0a_i        : in    std_logic;
    l0a_tag_i    : in    std_logic_vector(6 downto 0);
    clk_40_i     : in    std_logic;
    bcr_i        : in    std_logic;
    full         : in    std_logic; --interal
    almost_full  : in    std_logic;
    start_event  : in    std_logic;
    event_info_o : out   event_info;
    wr_en        : out   std_logic  --internal
  );
end entity event_counter;

architecture behavioral of event_counter is

  signal bcid_count   : unsigned(7 downto 0) := (others => '0');
  signal tag_count    : unsigned(6 downto 0) := (others => '0');
  signal bcid         : std_logic_vector(7 downto 0);
  signal bcid_less1   : std_logic_vector(7 downto 0);
  signal bcid_part    : std_logic_vector(3 downto 0) := (others => '0');
  signal wr_en_s      : std_logic := '0';
  signal tag_int      : std_logic_vector(6 downto 0);
  signal tag_ext      : std_logic_vector(6 downto 0);
  signal event_info_s : event_info;

begin

  tag_ext      <= l0a_tag_i;
  event_info_o <= event_info_s;

  bcid_counter : process (clk_40_i, bcr_i, bcid_count) is
    --variable bcid_counter: unsigned(7 downto 0) := "00000000";
  begin

    if (rising_edge(clk_40_i)) then
      if (bcr_i = '1') then
        bcid_count <= "00000000";
      else
        bcid_count <= bcid_count + 1;
      end if;
    end if;

    bcid       <= std_logic_vector(bcid_count);
    bcid_less1 <= std_logic_vector(bcid_count - 1);

  end process  bcid_counter; --bcid_counter

  tag_counter : process (clk_40_i, l0a_i, tag_count, start_event, wr_en_s) is
  begin

    if (rising_edge(clk_40_i)) then
      if (start_event = '1') then
        if (l0a_i='1') then
          wr_en_s   <= '1';
          tag_count <= tag_count + 1;
        elsif (wr_en_s='1') then
          wr_en_s <= '0';
        end if;
      else
        wr_en_s <= '0';
      end if;
    end if;

    tag_int <= std_logic_vector(tag_count);

  end process tag_counter; -- tag_counter

  bcid_part <= bcid_less1(3 downto 0);

  trig_sel : process (trig_sel_i, event_info_s, tag_int, bcid_part) is
  begin

    if (trig_sel_i='0') then
      event_info_s <= tag_int & bcid_part;
    else
      event_info_s <= tag_ext & bcid_part;
    end if;

  end process trig_sel;

  wr_en <= wr_en_s;

end architecture behavioral;
