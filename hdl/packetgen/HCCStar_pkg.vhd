library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package HCCStar_pkg is
	subtype trig_info is STD_LOGIC_VECTOR(10 downto 0);
	subtype event_info is STD_LOGIC_VECTOR(10 downto 0);
	subtype st_hits is STD_LOGIC_VECTOR (16 downto 0); --includes last hit flag 16-bits + last hit bit
	subtype prepacked is STD_LOGIC_VECTOR(21 downto 0);
	subtype packerlong is STD_LOGIC_VECTOR(31 downto 0);
	subtype packed is STD_LOGIC_VECTOR(15 downto 0);
	subtype to_elink is STD_LOGIC_VECTOR(17 downto 0);
	subtype packed_plus_last is STD_LOGIC_VECTOR (16 downto 0);
	subtype encoded is STD_LOGIC_VECTOR (7 downto 0);
end HCCStar_pkg;
