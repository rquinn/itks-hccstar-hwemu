----------------------------------------------------------------------------------
-- Company: UBC
-- Engineer: Dominique Trischuk
--
-- Create Date: 01/17/2018 02:51:55 PM
-- Design Name:
-- Module Name: top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;

library hccemu_pktgen;
  use hccemu_pktgen.HCCStar_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hccstar_hitgen is
  port (
    clk                       : in    std_logic;
    rst                       : in    std_logic;
    trig_preamble             : in    trig_info;
    empty_trig                : in    std_logic; -- from trig emu FIFO
    valid_trig                : in    std_logic; -- from trig emu FIFO
    rd_en_hitgen              : in    std_logic;
    chunk_length              : in    std_logic_vector(11 downto 0);
    itks_emu_hits             : in    std_logic_vector(16 downto 0);
    itks_emu_hits_valid       : in    std_logic;
    itks_emu_hits_empty       : in    std_logic;
    itks_emu_hits_full        : in    std_logic;
    itks_emu_hits_almost_full : in    std_logic;
    itks_emu_rd_ctl           : in    std_logic;
    data                      : out   prepacked;
    rd_en_trig                : out   std_logic; --signal for trig emu FIFO
    empty_hitgen_out          : out   std_logic;
    valid_hitgen_out          : out   std_logic;
    itks_emu_rd_en            : out   std_logic
  );
end entity hccstar_hitgen;

architecture hccstar_hitgen_str of hccstar_hitgen is

  signal data_ctl_out        : prepacked;
  signal data_sig            : prepacked;
  -- signal hits_prod_out       : st_hits; --into FIFO before ctl
  -- signal hits_fifo_out       : st_hits; --out of FIFO before ctl
  -- signal start_hits_flag_sig : std_logic;

  --Hit Gen FIFO
  signal wr_en_hitgen : std_logic;
  --signal rd_en_hitgen     : STD_LOGIC; --temporary should come from other side
  signal full_hitgen        : std_logic;
  signal empty_hitgen       : std_logic;
  signal valid_hitgen       : std_logic;
  signal almost_full_hitgen : std_logic;

  --Hit Producer FIFO
  -- signal wr_en_hitprod       : std_logic;
  -- signal full_hitprod        : std_logic;
  -- signal empty_hitprod       : std_logic;
  -- signal valid_hitprod       : std_logic;
  -- signal almost_full_hitprod : std_logic;

  component hccstar_hitgen_ctl is
    port (
      clk                 : in    std_logic;
      rst                 : in    std_logic;
      trig_preamble       : in    trig_info;
      empty_trig          : in    std_logic;
      valid_trig          : in    std_logic;
      itks_emu_hits       : in    std_logic_vector(16 downto 0);
      itks_emu_hits_valid : in    std_logic;
      itks_emu_hits_empty : in    std_logic;
      itks_emu_rd_en      : out   std_logic;
      data                : out   prepacked;
      rd_en_trig          : out   std_logic;
      wr_en_hitgen        : out   std_logic
    );
  end component hccstar_hitgen_ctl;

  component hccstar_hitgen_fifo is
    port (
      clk         : in    std_logic;
      srst        : in    std_logic;
      din         : in    prepacked;
      wr_en       : in    std_logic;
      rd_en       : in    std_logic;
      dout        : out   prepacked;
      full        : out   std_logic;
      almost_full : out   std_logic;
      empty       : out   std_logic;
      valid       : out   std_logic
    );
  end component hccstar_hitgen_fifo;

begin

  data             <= data_sig;
  empty_hitgen_out <= empty_hitgen;
  valid_hitgen_out <= valid_hitgen;

  hccstar_hitgen_ctl_1 : component hccstar_hitgen_ctl
    port map (
      clk           => clk,
      rst           => rst,
      trig_preamble => trig_preamble,
      empty_trig    => empty_trig,
      valid_trig    => valid_trig,
      -- hits                      => hits_fifo_out,
      -- empty_hitprod             => empty_hitprod,
      -- full_hitgen               => full_hitgen,
      -- almost_full_hitgen        => almost_full_hitgen,
      -- full_hitprod              => full_hitprod,
      -- almost_full_hitprod       => almost_full_hitprod,
      itks_emu_hits       => itks_emu_hits,
      itks_emu_hits_valid => itks_emu_hits_valid,
      -- itks_emu_hits_full        => itks_emu_hits_full,
      -- itks_emu_hits_almost_full => itks_emu_hits_almost_full,
      itks_emu_hits_empty => itks_emu_hits_empty,
      itks_emu_rd_en      => itks_emu_rd_en,
      -- itks_emu_rd_ctl           => itks_emu_rd_ctl,
      data            => data_ctl_out,
      rd_en_trig      => rd_en_trig,
      wr_en_hitgen    => wr_en_hitgen
      -- rd_en_hitprod   => rd_en_hitprod,
      -- valid_hitprod   => valid_hitprod,
      -- start_hits_flag => start_hits_flag_sig
    );

  hccstar_hitgen_fifo_1 : component hccstar_hitgen_fifo
    port map (
      clk         => clk,
      srst        => rst,
      din         => data_ctl_out,
      wr_en       => wr_en_hitgen,
      rd_en       => rd_en_hitgen,
      dout        => data_sig,
      full        => full_hitgen,
      almost_full => almost_full_hitgen,
      empty       => empty_hitgen,
      valid       => valid_hitgen
    );

end architecture hccstar_hitgen_str;
