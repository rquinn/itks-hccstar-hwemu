----------------------------------------------------------------------------------
-- Company: UBC
-- Engineer: Dominique Trischuk
--
-- Create Date: 01/17/2018 02:51:55 PM
-- Design Name:
-- Module Name: top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;

library hccemu_pktgen;
  use hccemu_pktgen.HCCStar_pkg.all;

  -- Uncomment the following library declaration if using
  -- arithmetic functions with Signed or Unsigned values
  use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hccstar_hitgen_ctl is
  port (
    clk                       : in    std_logic;
    rst                       : in    std_logic;
    trig_preamble             : in    trig_info;
    empty_trig                : in    std_logic;
    valid_trig                : in    std_logic;
    itks_emu_hits             : in    std_logic_vector(16 downto 0);
    itks_emu_hits_valid       : in    std_logic;
    itks_emu_hits_empty       : in    std_logic;
    itks_emu_rd_en            : out   std_logic;
    data                      : out   prepacked;
    rd_en_trig                : out   std_logic;
    wr_en_hitgen              : out   std_logic
  );
end entity hccstar_hitgen_ctl;

architecture hccstar_hitgen_ctl_str of hccstar_hitgen_ctl is

  type hccstar_hitgen_ctl_type is (
    IDLE, START, START_CTL_K28_1, HEADER, READ_HITS, TRAILER, END_CTL_K28_6
  );

  signal state_reg, state_next : hccstar_hitgen_ctl_type;

  signal packet_type   : std_logic_vector(3 downto 0);
  signal error_flag    : std_logic;
  signal last_hit      : std_logic;
  signal hits_to_write : st_hits;

begin

  packet_type <= "0010"; --LP packet
  error_flag  <= '0';    --currently unused and set to 1'b0 as of March 23 2018

  -- output and reset logic
  prc_sm_output_sync : process (clk, rst) is

  begin

    if (rising_edge(clk)) then
      state_reg <= state_next;

      -- defaults  (will apply in IDLE)
      rd_en_trig      <= '0';
      wr_en_hitgen    <= '0';
      itks_emu_rd_en  <= '0';

      if (rst='1') then
        state_reg <= IDLE;
      end if;

      if (state_reg = START) then
        -- start reading from trigger FIFO
        rd_en_trig <= '1';
      end if;

      if (state_reg = START_CTL_K28_1) then
        if (valid_trig = '1') then
          -- start write to the HitGen FIFO, but only if the data coming from
          -- the trigger fifo is valid
          wr_en_hitgen <= '1';
        end if;
        -- not the last word in the packet, obviously
        data(21 downto 21) <= "0";
        -- start with IDLE pattern for the FELIG encoder
        data(20 downto 13) <= "10111100";
        -- Start of Packet command K28.1
        data(12 downto 5)  <= "00111100";
        -- data length is 16 bits
        data(4 downto 0)   <= "10000";
      end if;

      if (state_reg = HEADER) then
        -- continue writing to HitGen FIFO
        wr_en_hitgen       <= '1';
        -- still not the last word in the packet!
        data(21 downto 21) <= "0";
        -- fill header with packet type, error flag, and trigger preamble
        data(20 downto 5)  <= packet_type
                              & error_flag
                              & trig_preamble(10 downto 0);
        -- 16-bit data length
        data(4 downto 0)   <= "10000";
      end if;

      if (state_reg = READ_HITS) then
        if (itks_emu_hits_valid = '1') then
          -- don't write anything unless the hits are valid!
          -- remember wr_en_hitgen is set low as default, so we don't need an
          -- `else` statement here
          wr_en_hitgen <= '1';
        end if;


        if (last_hit = '1') then
          -- stop reading if we're already at the last hit
          itks_emu_rd_en <= '0';
        else
          -- if not the last hit, keep reading hits!
          itks_emu_rd_en <= '1';
        end if;
        -- again, not the last word in the packet!
        data(21 downto 21) <= "0";
        -- hit data
        data(20 downto 5)  <= hits_to_write(15 downto 0);
        -- 16-bit data length
        data(4 downto 0 )  <= "10000";
      end if;

      if (state_reg = TRAILER) then
        -- keep writing to HitGen FIFO
        wr_en_hitgen       <= '1';
        -- still not the last word!
        data(21 downto 21) <= "0";
        -- fixed end pattern -- invalid if read as actual cluster data
        data(20 downto 5)  <= X"6fed";
        -- 16-bit data length
        data(4 downto 0)   <= "10000";
      end if;

      if (state_reg = END_CTL_K28_6) then
        -- keep writing to HitGen FIFO
        wr_en_hitgen       <= '1';
        -- finally the last word in the packet!
        data(21 downto 21) <= "1";
        -- End of Packet command K28.6
        data(20 downto 13) <= "11011100";
        -- next 8 bits are IDLE -- for the FELIG encoder
        data(12 downto 5)  <= "10111100";
        -- 16-bit data length
        data(4 downto 0)   <= "10000";
      end if;
    end if;

  end process prc_sm_output_sync;

  -- next-state logic
  prc_sm_logic_async : process (state_reg, valid_trig, empty_trig, last_hit,
                                hits_to_write, itks_emu_hits_empty,
                                itks_emu_hits_valid, itks_emu_hits) is
  begin

    hits_to_write <= (others => '0');

    case state_reg is

      when IDLE =>
        state_next <= IDLE;

        if (empty_trig = '0') then
          state_next <= START;
        end if;

      when START =>
        state_next <= START_CTL_K28_1;

      when START_CTL_K28_1 =>
        state_next <= START_CTL_K28_1;

        if (valid_trig = '1') then
          state_next <= HEADER;
        end if;

      when HEADER =>
        state_next <= READ_HITS;

      when READ_HITS =>
        state_next <= READ_HITS;
        last_hit   <= hits_to_write(16);  --latch

        if (itks_emu_hits_valid='1') then
          hits_to_write <= itks_emu_hits;
        end if;

        if (last_hit = '1') then
          state_next <= TRAILER;
        end if;

      when TRAILER =>
        state_next <= END_CTL_K28_6;
        last_hit   <= '0';

      when END_CTL_K28_6 =>
        state_next <= IDLE;

    end case;

  end process prc_sm_logic_async;

end architecture hccstar_hitgen_ctl_str;
