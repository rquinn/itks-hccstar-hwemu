-------------------------------------------------------------------------------
-- Company:        UBC
-- Engineer:       Colin Gay
--
-- Create Date:    16:42:12 07/28/05
-- Design Name:
-- Module Name:    Packer - Behavioral
-- Project Name:   TRT ROD
-- Target Device:  V4LX60
-- Tool versions:
-- Description:    This module takes in variable length data with
--                 a max length of 48 bits and densely packs it into
--                 32 bit words.  Also has bypass mode where data
--                 27 bit input words are simply put into output
--                 words 1 to 1 without packing.
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-------------------------------------------------------------------------------

library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.STD_LOGIC_ARITH.all;
  use IEEE.STD_LOGIC_UNSIGNED.all;

library hccemu_pktgen;
  use hccemu_pktgen.HCCStar_pkg.all;

entity packer is
  port (
    clk            : in    std_logic;
    reset          : in    std_logic;
    packerin       : in    packed;
    packerinlength : in    std_logic_vector(4 downto 0);
    packerinvalid  : in    std_logic;
    packerinlast   : in    std_logic;
    packerready    : out   std_logic;
    EVBready       : in    std_logic;
    packerout      : out   packed;
    packeroutvalid : out   std_logic;
    packeroutlast  : out   std_logic;
    packercnt      : out   packerlong
  );
end entity packer;

architecture behavioral of packer is

  signal lastlatched : std_logic;
  --signal lastLatched1 : std_logic; --DT added

  -- New packing signals
  signal shiftptr       : std_logic_vector(4 downto 0);
  signal shiftptr1      : std_logic_vector(4 downto 0);
  signal shiftptr2      : std_logic_vector(4 downto 0);
  signal shiftptr3      : std_logic_vector(4 downto 0);
  signal data17         : std_logic_vector(16 downto 0);
  signal data19         : std_logic_vector(18 downto 0);
  signal data23         : std_logic_vector(22 downto 0);
  signal data31         : std_logic_vector(30 downto 0);
  signal packeroutlower : std_logic_vector(15 downto 0);
  signal packeroutupper : std_logic_vector(15 downto 0);
  signal last1          : std_logic;
  signal last2          : std_logic;
  signal last3          : std_logic;
  signal last4          : std_logic;
  signal lastor         : std_logic;
  signal ipackerlastcnt : std_logic_vector(31 downto 0);
  -- FSM

  type state_type is (IDLE, FILL_LOW, SEND_LOW, FINISH_LOW, FILL_UPP, SEND_UPP, FINISH_UPP);

  signal state     : state_type;
  signal nextstate : state_type;

begin

  -- Packer passes readiness state of EventBuilder
  lastor      <= (PackerInLast and PackerInValid) or lastlatched  or last1 or last2 or last3 or last4; --DT added changed lastLatched to lastLatch1
  PackerReady <= EVBReady and not lastor;

  -- Count the number of times return to IDLE, ie number of processed evts
  PackerCNT <= ipackerlastcnt;

  -- shift in powers of 2
  shift : process (Reset, Clk, PackerInValid, shiftptr, state) is
  begin

    if (rising_edge(Clk)) then
      -- default
      PackerOutValid <= '0';
      PackerOutLast  <= '0';

      if (Reset = '1') then
        data17      <= (others => '0');
        data19      <= (others => '0');
        data23      <= (others => '0');
        data31      <= (others => '0');
        shiftptr1   <= (others => '0');
        shiftptr2   <= (others => '0');
        shiftptr3   <= (others => '0');
        last1       <= '0';
        last2       <= '0';
        last3       <= '0';
        last4       <= '0';
        state       <= IDLE;
        lastlatched <= '0';
        --  lastLatched1   <= '0';
        shiftptr       <= (others => '0');
        ipackerlastcnt <= (others => '0');
      elsif (PackerInValid = '1' or lastor = '1') then
        state <= nextstate;

        if (shiftptr(0) = '1') then
          data17 <= PackerIn(15 downto 0) & '0';
        else
          data17(15 downto 0) <= PackerIn(15 downto 0);
          data17(16)          <= '0';
        end if;

        if (shiftptr1(1) = '1') then
          data19 <= data17 & "00";
        else
          data19(16 downto 0)  <= data17;
          data19(18 downto 17) <= (others => '0');
        end if;

        if (shiftptr2(2) = '1') then
          data23 <= data19 & X"0";
        else
          data23(18 downto 0)  <= data19;
          data23(22 downto 19) <= (others => '0');
        end if;

        if (shiftptr3(3) = '1') then
          data31 <= data23 & X"00";
        else
          data31(22 downto 0)  <= data23;
          data31(30 downto 23) <= (others => '0');
        end if;

        -- defaults
        PackerOut      <= (others => '0');
        packeroutlower <= (others => '0');
        packeroutupper <= (others => '0');

        shiftptr  <= shiftptr + PackerInLength;
        shiftptr1 <= shiftptr;
        shiftptr2 <= shiftptr1;
        shiftptr3 <= shiftptr2;

        lastlatched <= PackerInLast and PackerInValid;
        --lastLatched1<= lastLatched; --DT added this to try to delay last by 1 clock cycle.
        last1 <= lastlatched;                                      --DT changed lastLatched to lastLatched1
        last2 <= last1;
        last3 <= last2;
        last4 <= last3;                                            --do we need this??

        if (state = IDLE) then
          data17   <= (others => '0');
          shiftptr <= (others => '0');
          if (PackerInValid = '1') then
            data17(15 downto 0) <= PackerIn(15 downto 0);
            shiftptr            <= PackerInLength;
          end if;
          data19    <= (others => '0');
          data23    <= (others => '0');
          data31    <= (others => '0');
          shiftptr1 <= (others => '0');
          shiftptr2 <= (others => '0');
          shiftptr3 <= (others => '0');
          last1     <= '0';
          last2     <= '0';
          last3     <= '0';
          last4     <= '0';
        end if;

        if (state = FILL_LOW) then
          packeroutlower <= packeroutlower or data31(15 downto 0);
        end if;

        if (state = SEND_LOW) then
          PackerOut      <= packeroutlower or data31(15 downto 0);
          packeroutupper <= '0' & data31(30 downto 16);
          PackerOutValid <= '1';
          if (shiftptr2  <= 16) then                               -- Original
            --if shiftPtr3  <= 16 then --DT edits
            PackerOutLast <= last3;
          end if;
          if (last3 = '1') then
            ipackerlastcnt <= ipackerlastcnt + 1;
          end if;
        end if;

        if (state = FILL_UPP) then
          packeroutupper <= packeroutupper or data31(15 downto 0);
        end if;

        if (state = SEND_UPP) then
          PackerOut      <= packeroutupper or data31(15 downto 0);
          packeroutlower <= '0' & data31(30 downto 16);
          PackerOutValid <= '1';
          if (shiftptr2 > 15 or shiftptr = 0) then                 --Original
            --if shiftPtr1 > 15 or shiftPtr = 0 then --DT edits
            PackerOutLast <= last3;
          end if;
          if (last3 = '1') then
            ipackerlastcnt <= ipackerlastcnt + 1;
          end if;
        end if;

        if (state = FINISH_LOW) then
          PackerOut      <= packeroutupper;
          PackerOutValid <= '1';
          PackerOutLast  <= last4;
          ipackerlastcnt <= ipackerlastcnt + 1;
        end if;

        if (state = FINISH_UPP) then
          PackerOut      <= packeroutlower;
          PackerOutValid <= '1';
          PackerOutLast  <= last4;
          ipackerlastcnt <= ipackerlastcnt + 1;
        end if;
      end if;
    end if;

  end process shift;

  -- purpose: PackerOut Control
  -- type : combinational
  -- inputs : State, shiftPtr3, last4, PackerInValid
  -- outputs:
  state_machine : process (state, shiftptr2, shiftptr1, shiftptr3, last2, last3, PackerInValid, shiftptr) is
  begin                                                                 -- process state

    -- defaults
    nextstate <= IDLE;

    case state is

      when IDLE =>
        nextstate <= IDLE;

        if (PackerInValid = '1') then
          nextstate <= FILL_LOW;
        end if;

      when FILL_LOW =>
        nextstate <= FILL_LOW;

        if (shiftptr2 > 15 or last3 = '1') then                       --2??
          nextstate <= SEND_LOW;
        end if;

      when SEND_LOW =>
        nextstate <= FILL_UPP;

        if (last2 = '1') then
          nextstate <= SEND_UPP;
        --elsif last3 = '1' and shiftPtr2 > 16 then --Original
        elsif (last3 = '1' and shiftptr3 > 16) then                   --DT edits
          nextstate <= FINISH_LOW;
        elsif (last3 = '1') then
          nextstate <= IDLE;
        elsif (shiftptr2 < 16) then
          nextstate <= SEND_UPP;
        end if;

      when FILL_UPP =>
        nextstate <= FILL_UPP;

        if (shiftptr2 < 16 or last2 = '1') then
          nextstate <= SEND_UPP;
        end if;

      when SEND_UPP =>
        nextstate <= FILL_LOW;

        if (last2 = '1') then
          nextstate <= SEND_LOW;
        --elsif last3 = '1' and shiftPtr2 > 0 and shiftPtr < 16 then --Original
        elsif (last3 = '1' and shiftptr3 > 0 and shiftptr1 < 16) then --DT edits
          nextstate <= FINISH_UPP;
        elsif (last3 = '1') then
          nextstate <= IDLE;
        elsif (shiftptr2 > 15) then
          nextstate <= SEND_LOW;
        end if;

      when FINISH_LOW =>
        nextstate <= IDLE;

      when FINISH_UPP =>
        nextstate <= IDLE;

    end case;

  end process state_machine;

end architecture behavioral;
