library ieee;
  use ieee.std_logic_1164.all;

entity packer_data_selector is
  port (
    clk : in    std_logic;
    rst : in    std_logic;

    hitgen_data_i  : in    std_logic_vector(21 downto 0);
    hitgen_empty_i : in    std_logic;
    hitgen_valid_i : in    std_logic;
    hitgen_rd_en_o : out   std_logic;

    rrpgen_data_i  : in    std_logic_vector(21 downto 0);
    rrpgen_empty_i : in    std_logic;
    rrpgen_valid_i : in    std_logic;
    rrpgen_rd_en_o : out   std_logic;

    packer_data_o   : out   std_logic_vector(15 downto 0);
    packer_length_o : out   std_logic_vector( 4 downto 0);
    packer_last_o   : out   std_logic;
    packer_valid_o  : out   std_logic;
    packer_ready_i  : in    std_logic
  );
end entity packer_data_selector;

architecture behavioral of packer_data_selector is

  type t_states is (Idle, HitPacket, RegReadPacket);

  signal state : t_states;

  signal fifo_data  : std_logic_vector(21 downto 0);
  signal fifo_valid : std_logic;

begin

  packer_last_o   <= fifo_data(21);
  packer_data_o   <= fifo_data(20 downto 5);
  packer_length_o <= fifo_data(4 downto 0);
  packer_valid_o  <= fifo_valid;

  prc_sm_logic_sync : process (clk) is
  begin

    if (rst = '1') then
      state <= Idle;
    elsif (rising_edge(clk)) then

      case state is

        when Idle =>
          hitgen_rd_en_o <= '0';
          rrpgen_rd_en_o <= '0';
          fifo_data      <= (others => '0');
          fifo_valid     <= '0';
          if (packer_ready_i = '1') then
            if (hitgen_empty_i /= '1') then
              state <= HitPacket;
            elsif (rrpgen_empty_i /= '1') then
              state <= RegReadPacket;
            else
              state <= Idle;
            end if;
          end if;
        when HitPacket =>
          rrpgen_rd_en_o <= '0';
          fifo_data      <= hitgen_data_i;
          fifo_valid     <= hitgen_valid_i;
          if (fifo_valid = '1' and fifo_data(21) = '1') then
            hitgen_rd_en_o <= '0';
            state <= Idle;
          else
            hitgen_rd_en_o <= packer_ready_i and (not hitgen_empty_i);
            state <= HitPacket;
          end if;
        when RegReadPacket =>
          hitgen_rd_en_o <= '0';
          fifo_data      <= rrpgen_data_i;
          fifo_valid     <= rrpgen_valid_i;
          if (fifo_valid = '1' and fifo_data(21) = '1') then
            rrpgen_rd_en_o <= '0';
            state <= Idle;
          else
            rrpgen_rd_en_o <= packer_ready_i and (not rrpgen_empty_i);
            state <= RegReadPacket;
          end if;

      end case;

    end if;

  end process prc_sm_logic_sync;

end architecture behavioral;
