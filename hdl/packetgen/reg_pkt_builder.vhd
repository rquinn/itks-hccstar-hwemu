library ieee;
  use ieee.std_logic_1164.all;

entity reg_pkt_builder is
  port (
    clk          : in    std_logic;
    rst          : in    std_logic;
    reg_read_i   : in    std_logic;
    reg_addr_i   : in    std_logic_vector( 7 downto 0);
    reg_data_i   : in    std_logic_vector(31 downto 0);
    fifo_rd_en_i : in    std_logic;
    fifo_data_o  : out   std_logic_vector(21 downto 0);
    fifo_valid_o : out   std_logic;
    fifo_empty_o : out   std_logic
  );
end entity reg_pkt_builder;

architecture behavioral of reg_pkt_builder is

  component reg_pkt_fifo is
    port (
      clk   : in    std_logic;
      srst  : in    std_logic;
      din   : in    std_logic_vector(21 downto 0);
      wr_en : in    std_logic;
      rd_en : in    std_logic;
      dout  : out   std_logic_vector(21 downto 0);
      full  : out   std_logic;
      empty : out   std_logic;
      valid : out   std_logic
    );
  end component;

  signal reg_addr : std_logic_vector(7 downto 0);
  signal reg_data : std_logic_vector(31 downto 0);

  constant typ     : std_logic_vector(3 downto 0) := "1000"; -- HCC Register Read TYP
  constant padding : std_logic_vector(3 downto 0) := "0000";

  constant k28_5 : std_logic_vector(7 downto 0) := x"bc"; -- idle
  constant k28_1 : std_logic_vector(7 downto 0) := x"3c"; -- start
  constant k28_6 : std_logic_vector(7 downto 0) := x"dc"; -- end

  type t_states is (Idle, Header, Write1, Write2, Write3, Trailer);

  signal state : t_states;

  constant word_len   : std_logic_vector(4 downto 0) := "10000"; -- 16b words
  signal   fifo_word  : std_logic_vector(15 downto 0);
  signal   fifo_last  : std_logic;
  signal   fifo_din   : std_logic_vector(21 downto 0);
  signal   fifo_wr_en : std_logic;

begin

  prc_sm_sync : process (clk) is
  begin

    if (rising_edge(clk)) then

      case state is

        when Idle =>
          fifo_wr_en <= '0';
          if (reg_read_i = '1') then
            reg_addr <= reg_addr_i;
            reg_data <= reg_data_i;
            state    <= Header;
          end if;
        when Header =>
          fifo_word  <= k28_5 & k28_1;
          fifo_last  <= '0';
          fifo_wr_en <= '1';
          state      <= Write1;
        when Write1 =>
          fifo_word  <= typ & reg_addr & reg_data(31 downto 28);
          fifo_last  <= '0';
          fifo_wr_en <= '1';
          state      <= Write2;
        when Write2 =>
          fifo_word  <= reg_data(27 downto 12);
          fifo_last  <= '0';
          fifo_wr_en <= '1';
          state      <= Write3;
        when Write3 =>
          fifo_word  <= reg_data(11 downto 0) & padding;
          fifo_last  <= '0';
          fifo_wr_en <= '1';
          state      <= Trailer;
        when Trailer =>
          fifo_word  <= k28_6 & k28_5;
          fifo_last  <= '1';
          fifo_wr_en <= '1';
          state      <= Idle;

      end case;

    end if;

  end process prc_sm_sync;

  fifo_din <= fifo_last & fifo_word & word_len;

  comp_reg_pkt_fifo : component reg_pkt_fifo
    port map (
      clk   => clk,
      srst  => rst,
      din   => fifo_din,
      wr_en => fifo_wr_en,
      rd_en => fifo_rd_en_i,
      dout  => fifo_data_o,
      full  => open,
      empty => fifo_empty_o,
      valid => fifo_valid_o
    );

end architecture behavioral;
