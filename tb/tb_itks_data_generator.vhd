----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/13/2021 11:24:43 AM
-- Design Name: 
-- Module Name: tb_itks_data_generator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
library hccemu;

entity tb_itks_data_generator is
--  Port ( );
end tb_itks_data_generator;

architecture Behavioral of tb_itks_data_generator is

    -- easy clock generator, specify FREQ in Hz
    procedure clk_gen(signal clk : out std_logic; constant FREQ : real) is
        constant PERIOD     : time := 1 sec / FREQ;
        constant HIGH_TIME  : time := PERIOD / 2;
        constant LOW_TIME   : time := PERIOD - HIGH_TIME;
    begin
        loop
            clk <= '1';
            wait for HIGH_TIME;
            clk <= '0';
            wait for LOW_TIME;
        end loop;
    end procedure;

    signal clk240       : std_logic;

    signal rst : std_logic;
    signal rd_en : std_logic;
    signal rd_ctl : std_logic;
    signal wr_en : std_logic;
    signal din : std_logic_vector(16 downto 0);
    signal wr_done : std_logic;
    signal full : std_logic;
    signal almost_full : std_logic;
    signal empty : std_logic;
    signal hits : std_logic_vector(16 downto 0);
    signal hits_valid : std_logic;

begin

    dgen : entity hccemu.itks_data_generator
    port map (
        clk240                  => clk240,
        itks_fifo_rst           => rst,
        itks_fifo_rd_en         => rd_en,
        itks_fifo_rd_ctl        => rd_ctl,
        itks_fifo_wr_en         => wr_en,
        itks_fifo_data          => din,
        itks_fifo_full          => full,
        itks_fifo_almost_full   => almost_full,
        itks_fifo_empty         => empty,
        hccstar_emu_hits        => hits,
        hccstar_emu_hits_valid  => hits_valid
    );

    clk_gen(clk240, 240.0E6);

    prc_main : process
    begin
        rst <= '1';
        wr_en <= '0';
        rd_en <= '0';
        rd_ctl <= '0';
        din <= (others => '0');
        wait for 100 ns;
        rst <= '0';
        wait for 100 ns;
        wr_en <= '1';
        din <= '0' & x"dead";
        wait for 100 ns;
        wr_en <= '0';
        wait for 100 ns;
        wr_en <= '1';
        din <= '1' & x"beef";
        wait for 100 ns;
        wr_en <= '0';
        wait;
    end process;

end Behavioral;
