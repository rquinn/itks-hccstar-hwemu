----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 05/13/2021 11:24:43 AM
-- Design Name:
-- Module Name: tb_top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.NUMERIC_STD.ALL;

library hccemu;

entity tb_top is
end entity tb_top;

architecture behavioral of tb_top is

  component itkstrips_lcb_clk is
    port (
      clk_in1 : in    std_logic;
      -- Clock out ports
      lcb_clk40 : out   std_logic;
      -- Status and control signals
      reset  : in    std_logic;
      locked : out   std_logic
    );
  end component;

  -- easy clock generator, specify FREQ in Hz
  procedure clk_gen(signal clk : out std_logic; constant FREQ : real) is
    constant period    : time := 1 sec / FREQ;
    constant high_time : time := period / 2;
    constant low_time  : time := period - high_time;
  begin
    loop
      clk <= '1';
      wait for high_time;
      clk <= '0';
      wait for low_time;
    end loop;
  end procedure;

  signal clk40     : std_logic;
  signal clk240    : std_logic;
  signal frame_clk : std_logic;

  signal rst          : std_logic := '0';
  signal rx_flag      : std_logic := '0';
  signal l0a          : std_logic := '0';
  signal l0a_tag      : std_logic_vector(6 downto 0) := (others => '0');
  signal bcr          : std_logic;
  signal reg_read     : std_logic;
  signal reg_addr     : std_logic_vector(7 downto 0);
  signal reg_data     : std_logic_vector(31 downto 0);
  signal l0a_240      : std_logic := '0';
  signal l0a_tag_240  : std_logic_vector(6 downto 0) := (others => '0');
  signal bcr_240      : std_logic;
  signal reg_read_240 : std_logic;
  signal reg_addr_240 : std_logic_vector(7 downto 0);
  signal reg_data_240 : std_logic_vector(31 downto 0);

  signal trig_sel     : std_logic := '0';
  signal chunk_length : std_logic_vector(11 downto 0) := (others => '0');

  signal itks_hits             : std_logic_vector(16 downto 0);
  signal itks_hits_valid       : std_logic;
  signal itks_fifo_rst         : std_logic;
  signal itks_rd_en            : std_logic;
  signal itks_rd_ctl           : std_logic := '0';
  signal itks_fifo_wr_en       : std_logic;
  signal itks_fifo_data        : std_logic_vector(16 downto 0);
  signal itks_hits_full        : std_logic;
  signal itks_hits_almost_full : std_logic;
  signal itks_hits_empty       : std_logic;

  signal lcb_4b         : std_logic_vector(3 downto 0) := (others => '0');
  signal lcb_clk_locked : std_logic;

  type state_t is (Reset, Idle, SendWriteCommand, Waiting1, SendReadCommand, Waiting2, SendTrigger, Final);

  signal state  : state_t;
  signal sm_rst : std_logic := '0';

  signal frame  : std_logic_vector(15 downto 0) := (others => '0');
  signal nframe : std_logic_vector(15 downto 0) := (others => '0');

  constant nrst  : integer := 2;
  constant nwait : integer := 4;

  constant idle_frame : std_logic_vector(15 downto 0) := x"7855";
  constant frame_lock : integer := 32;

  -- -- read command | hccid=0, regaddr=0
  -- constant RD_CMD_LEN    : integer := 4; -- command length in frames
  -- constant RD_CMD_BITS   : integer := RD_CMD_LEN * 16;
  -- constant RD_CMD        : std_logic_vector(RD_CMD_BITS-1 downto 0) := x"4753715959594759";
  -- -- write command | hccid=0, regaddr=0, data=0xffffffff
  -- constant WR_CMD_LEN    : integer := 9;
  -- constant WR_CMD_BITS   : integer := WR_CMD_LEN * 16;
  -- constant WR_CMD        : std_logic_vector(WR_CMD_BITS-1 downto 0) := x"475359595959594b71667166716671664759";

  -- -- read command | hccid=0, regaddr=0x92
  -- constant rd_cmd_len  : integer := 4; -- command length in frames
  -- constant rd_cmd_bits : integer := rd_cmd_len * 16;
  -- constant rd_cmd      : std_logic_vector(rd_cmd_bits - 1 downto 0) := x"4753717259e44759";
  -- -- write command | hccid=0, regaddr=0x99, data=0xdeadbeef
  -- constant wr_cmd_len  : integer := 9;
  -- constant wr_cmd_bits : integer := wr_cmd_len * 16;
  -- constant wr_cmd      : std_logic_vector(wr_cmd_bits - 1 downto 0) := x"4753597259e4598d71355936714d716c4759";

  -- read command | hccid=0, regaddr=0xa
  constant rd_cmd_len  : integer := 4;
  constant rd_cmd_bits : integer := rd_cmd_len * 16;
  constant rd_cmd      : std_logic_vector(rd_cmd_bits - 1 downto 0) := x"4753715959d44759";
  -- write command | hccid=0, regaddr=0xa, data=0xdeadbeef
  constant wr_cmd_len  : integer := 9;
  constant wr_cmd_bits : integer := wr_cmd_len * 16;
  constant wr_cmd      : std_logic_vector(wr_cmd_bits - 1 downto 0) := x"4753595959d4598d71355936714d716c4759";


  -- trigger command
  constant tr_cmd_len  : integer := 1;
  constant tr_cmd_bits : integer := tr_cmd_len * 16;
  constant tr_cmd      : std_logic_vector(tr_cmd_bits - 1 downto 0) := x"72d1";

  signal elinkdata_o     : std_logic_vector(17 downto 0);
  signal elinkdata_rdy_o : std_logic;

begin

  lcbdec : entity hccemu.itks_lcb_wrapper
    port map (
      clk40      => clk40,
      rst        => rst,
      rx_flag    => rx_flag,
      lcb_4b     => lcb_4b,
      l0a        => l0a,
      l0a_tag    => l0a_tag,
      bcr        => bcr,
      reg_read_o => reg_read,
      reg_addr_o => reg_addr,
      reg_data_o => reg_data
    );

  hccstaremu : entity hccemu.hccstaremu
    port map (
      clk                         => clk240,
      rst                         => rst,
      l0a_i                       => l0a_240,
      l0a_tag_i                   => l0a_tag_240,
      bcr_i                       => bcr_240,
      trig_sel                    => trig_sel,
      chunk_length_i              => chunk_length,
      itks_emu_hits               => itks_hits,
      itks_emu_hits_valid         => itks_hits_valid,
      itks_emu_hits_full          => itks_hits_full,
      itks_emu_hits_almost_full   => itks_hits_almost_full,
      itks_emu_hits_empty         => itks_hits_empty,
      itks_emu_rd_ctl             => itks_rd_ctl,
      itks_emu_rd_en              => itks_rd_en,
      elinkdata_o                 => elinkdata_o,
      elinkdata_rdy_o             => elinkdata_rdy_o,
      last_word                   => open,
      reg_read_i                  => reg_read_240,
      reg_addr_i                  => reg_addr_240,
      reg_data_i                  => reg_data_240
    );

  bram : entity hccemu.itks_data_generator
    port map (
      clk240                 => clk240,
      itks_fifo_rst          => itks_fifo_rst,
      itks_fifo_rd_en        => itks_rd_en,
      itks_fifo_rd_ctl       => itks_rd_ctl,
      itks_fifo_wr_en        => itks_fifo_wr_en,
      itks_fifo_data         => itks_fifo_data,
      itks_fifo_full         => itks_hits_full,
      itks_fifo_almost_full  => itks_hits_almost_full,
      itks_fifo_empty        => itks_hits_empty,
      hccstar_emu_hits       => itks_hits,
      hccstar_emu_hits_valid => itks_hits_valid
    );

  clk_lcb : component itkstrips_lcb_clk
    port map (
      clk_in1   => clk240,
      lcb_clk40 => clk40,
      reset     => '0',
      locked    => lcb_clk_locked
    );

  clk_gen(clk240, 240.0E6);

  prc_lcb_sync240 : process (clk240, rx_flag) is
  begin

    if (rising_edge(clk240)) then
      if (rx_flag = '1') then
        l0a_240      <= l0a;
        l0a_tag_240  <= l0a_tag;
        bcr_240      <= bcr;
        reg_read_240 <= reg_read;
        reg_addr_240 <= reg_addr;
        reg_data_240 <= reg_data;
      else
        l0a_240      <= '0';
        l0a_tag_240  <= (others => '0');
        bcr_240      <= '0';
        reg_read_240 <= '0';
        reg_addr_240 <= (others => '0');
        reg_data_240 <= (others => '0');
      end if;
    end if;

  end process prc_lcb_sync240;

  prc_rst : process is
  begin

    sm_rst <= '1';
    wait for 1000 ns;
    sm_rst <= '0';
    wait;

  end process prc_rst;

  prc_rx_flag : process (clk240) is

    variable cnt : integer := 0;

  begin

    if (rising_edge(clk240)) then
      if (cnt = 0) then
        rx_flag <= '1';
        cnt := cnt + 1;
      elsif (cnt = 5) then
        rx_flag <= '0';
        cnt := 0;
      else
        rx_flag <= '0';
        cnt := cnt + 1;
      end if;
    end if;

  end process prc_rx_flag;

  prc_load_bram : process is
  begin

    wait for 1000 ns;
    itks_fifo_rst   <= '1';
    itks_fifo_wr_en <= '0';
    -- itks_rd_en <= '0';
    itks_rd_ctl     <= '0';
    itks_fifo_data  <= (others => '0');
    wait for 100 ns;
    itks_fifo_rst   <= '0';
    wait for 100 ns;
    itks_fifo_data  <= '0' & x"aced";
    itks_fifo_wr_en <= '1';
    wait for 100 ns;
    itks_fifo_wr_en <= '0';
    wait for 100 ns;
    itks_fifo_data  <= '1' & x"cafe";
    itks_fifo_wr_en <= '1';
    wait for 100 ns;
    itks_fifo_wr_en <= '0';
    wait for 100 ns;
    itks_rd_ctl     <= '1';
    wait;

  end process prc_load_bram;

  prc_sync40 : process (clk40) is

    variable cnt : unsigned(1 downto 0) := "00";

  begin

    if (rising_edge(clk40)) then

      case cnt is

        when "00" =>
          lcb_4b    <= frame(15 downto 12);
          frame_clk <= '1';
        when "01" =>
          lcb_4b    <= frame(11 downto 8);
          frame_clk <= '1';
        when "10" =>
          lcb_4b    <= frame(7 downto 4);
          frame_clk <= '0';
        when "11" =>
          lcb_4b    <= frame(3 downto 0);
          frame_clk <= '0';
          frame     <= nframe;
        when others =>
          lcb_4b    <= "0000";
          frame_clk <= '0';

      end case;

      cnt := cnt + 1;
    end if;

  end process prc_sync40;

  prc_sm_syncfc : process (frame_clk) is

    variable idle_frame_cnt : integer := 0;
    variable rst_cnt        : integer := 0;
    variable wait_cnt       : integer := 0;
    variable cmd_frame_cnt  : integer := 0;
    variable cmd_idx        : integer;

  begin

    if (rising_edge(frame_clk)) then
      if (sm_rst = '1') then
        state  <= Reset;
        nframe <= idle_frame;
        idle_frame_cnt := 0;
      else

        case state is

          when Reset =>
            rst    <= '1';
            nframe <= idle_frame;
            rst_cnt := rst_cnt + 1;
            if (rst_cnt = nrst) then
              state <= Idle;
            end if;
          when Idle =>
            rst    <= '0';
            nframe <= idle_frame;
            idle_frame_cnt := idle_frame_cnt + 1;
            if (idle_frame_cnt = (frame_lock + 8)) then
              cmd_frame_cnt := 0;
              cmd_idx       := wr_cmd_bits - 1;
              state <= SendWriteCommand;
            end if;
          when SendReadCommand =>
            nframe <= rd_cmd(cmd_idx downto cmd_idx - 15);
            cmd_frame_cnt := cmd_frame_cnt + 1;
            cmd_idx       := cmd_idx - 16;
            if (cmd_frame_cnt = rd_cmd_len) then
              wait_cnt := 0;
              state <= Waiting2;
            end if;
          when Waiting1 =>
            nframe <= idle_frame;
            wait_cnt := wait_cnt + 1;
            if (wait_cnt = nwait) then
              cmd_frame_cnt := 0;
              cmd_idx       := rd_cmd_bits - 1;
              state <= SendReadCommand;
            end if;
          when SendWriteCommand =>
            nframe <= wr_cmd(cmd_idx downto cmd_idx - 15);
            cmd_frame_cnt := cmd_frame_cnt + 1;
            cmd_idx       := cmd_idx - 16;
            if (cmd_frame_cnt = wr_cmd_len) then
              wait_cnt := 0;
              state <= Waiting1;
            end if;
          when Waiting2 =>
            nframe <= idle_frame;
            wait_cnt := wait_cnt + 1;
            if (wait_cnt = nwait) then
              cmd_frame_cnt := 0;
              cmd_idx       := tr_cmd_bits - 1;
              state <= SendTrigger;
            end if;
          when SendTrigger =>
            nframe <= tr_cmd(cmd_idx downto cmd_idx - 15);
            cmd_frame_cnt := cmd_frame_cnt + 1;
            cmd_idx       := cmd_idx - 16;
            if (cmd_frame_cnt = tr_cmd_len) then
              state <= Final;
            end if;
          when Final =>
            nframe <= idle_frame;

        end case;

      end if;
    end if;

  end process prc_sm_syncfc;

end architecture behavioral;
